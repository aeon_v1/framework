import unittest
import validate as va

#testfiles directory
tf = "/home/masie11/cc/unittest_files"

class TestCNFValidate(unittest.TestCase):
    
    def test_is_cnf(self):
        print "Testing is_cnf():"
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_4_parts_p_line.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_first_p_line_after_comment_section.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_last_integer_0_clause_section.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_nclauses_integer_p_line.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_clause_section.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_cnf_p_line.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_only_integers_clause_section.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_nvariables_integer_p_line.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_nclauses.cnf"))
        self.assertFalse(va.is_cnf(tf+"/cnf/fail_nvariables.cnf"))
        self.assertFalse(va.is_cnf("fail_no_file"))
        self.assertTrue(va.is_cnf(tf+"/cnf/pass_validated.cnf"))

    def test_is_icnf(self):
        print "\nTesting is_icnf():"
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_2_parts_p_line.icnf"))
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_p_line_after_comment_section.icnf"))
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_last_integer_0_clause_section.icnf"))
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_inccnf_p_line.icnf"))
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_different_letter_than_a.icnf"))
        self.assertFalse(va.is_icnf(tf+"/icnf/fail_only_integers_clause_section_after_a.icnf"))
        self.assertFalse(va.is_icnf("fail_no_file"))
        self.assertTrue(va.is_icnf(tf+"/icnf/pass_validated.icnf"))

    def test_is_cubes(self):
        print "\nTesting is_cubes():"
        self.assertFalse(va.is_cubes(tf+"/cubes/fail_last_integer_0.cubes"))
        self.assertFalse(va.is_cubes(tf+"/cubes/fail_line_starts_with_a.cubes"))
        self.assertFalse(va.is_cubes(tf+"/cubes/fail_only_integers_after_a.cubes"))
        self.assertFalse(va.is_cubes("fail_no_file"))
        self.assertTrue(va.is_cubes(tf+"/cubes/pass_validated.cubes"))

    def test_is_drup(self):
        print "\nTesting is_drup():"
        self.assertFalse(va.is_drup(tf+"/drup/fail_last_integer_0.drup"))
        self.assertFalse(va.is_drup(tf+"/drup/fail_only_integers_except_d.drup"))
        self.assertFalse(va.is_drup("fail_no_file"))
        self.assertTrue(va.is_drup(tf+"/drup/pass_validated.drup"))

    def test_is_grit(self):
        print "\nTesting is_grit():"
        self.assertFalse(va.is_grit(tf+"/grit/fail_only_integers.grit"))
        self.assertFalse(va.is_grit(tf+"/grit/fail_last_integer_0.grit"))
        self.assertFalse(va.is_grit("fail_no_file"))
        self.assertTrue(va.is_grit(tf+"/grit/pass_validated.grit"))

    def test_is_lrat(self):
        print "\nTesting is_lrat():"
        self.assertFalse(va.is_lrat(tf+"/lrat/fail_last_integer_0.lrat"))
        self.assertFalse(va.is_lrat(tf+"/lrat/fail_only_integers_except_d_line.lrat"))
        self.assertFalse(va.is_lrat(tf+"/lrat/fail_incremental.lrat"))
        self.assertFalse(va.is_lrat("fail_no_file"))
        self.assertTrue(va.is_lrat(tf+"/lrat/pass_validated.lrat"))



suite = unittest.TestLoader().loadTestsFromTestCase(TestCNFValidate)
unittest.TextTestRunner(verbosity=0).run(suite)
#if __name__ == '__main__':
#   unittest.main()
