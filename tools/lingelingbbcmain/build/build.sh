#!/bin/sh
rm -rf druplig*
unzip ../archives/druplig*
mv druplig* druplig
cd druplig
./configure.sh
make libdruplig.a
cd ..
rm -rf lingeling*
tar xf ../archives/lingeling*
mv lingeling* lingeling
cd lingeling
./configure.sh
make lingeling
install -s lingeling ../../bin/
