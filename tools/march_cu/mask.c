#include <stdio.h>
#include <stdlib.h>

#define MAX 	1000000

int main (int argc, char** argv) {

  int i, tmp, var, max = 0;
  int in[MAX + 1];
  int lit1, lit2, lit3;
  int n, m;

  for (i = 1; i <= MAX; i++) in[i] = 0;

  FILE *mask, *cnf;

  mask = fopen (argv[1], "r");

  while (1) {
    tmp = fscanf (mask, " %i ", &var);
    if (tmp != 1) break;
    if (var > max) max = var;
    in[var] = 1; }

  fclose (mask);
  
  cnf = fopen (argv[2], "r");

  printf ("p cnf %i %i\n", max, max);

  tmp = fscanf (cnf, " p cnf %i %i ", &n, &m);

  while (1) {
    tmp = fscanf (cnf, " %i %i %i 0 ", &lit1, &lit2, &lit3); 
    if (tmp != 3) break;
    if (in[abs(lit1)] & in[abs(lit2)] & in[abs(lit3)])
      printf("%i %i %i 0\n", lit1, lit2, lit3);
  }

  fclose (cnf);
}
