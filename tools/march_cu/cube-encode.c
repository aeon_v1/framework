#include <stdio.h>
#include <stdlib.h>

#define MAX 	100000

int written_bytes;

FILE *input, *output;

static void die (const char * msg, ...) {
  fputs ("*** cube-encode error: ", stderr);
  fputc ('\n', stderr);
  exit (1);
}

static void write_char (unsigned char ch) {
  if (putc_unlocked ((int) ch, output) == EOF)
    die ("failed to write byte %ld", written_bytes + 1);
  written_bytes++; }

static void write_lit (int lit) {
  unsigned int nat = (abs(lit) * 2) + (lit < 0);
//  printf("%i ", lit);
//  if (lit == 0) printf("\n");
  for (; nat > 127; nat >>= 7)
    write_char (128 | (nat & 127));
  write_char (nat); }

void backtrack (int from, int to) {
  while (from > to) {
    from--;
    write_lit (0); } }

int main (int argc, char** argv) {

  int i;
  int *cube;
  cube = (int*) malloc (sizeof(int) * MAX);

  for (i = 0; i < MAX; i++) cube[i] = 0;

  written_bytes = 0;

  if (argc < 3) {
    printf("c wrong execution: ./cube-encode INPUT OUTPUT\n");
    exit(0); }

  input  = fopen (argv[1], "r");
  output = fopen (argv[2], "w");

  int last  = 0;
  int size  = 0;
  int match = 1;
  while (1) {
    int tmp, lit;

    if (size == 0) tmp = fscanf(input, " a %i ", &lit);
    else           tmp = fscanf(input,   " %i ", &lit);

    if (tmp == EOF) break;

    if (lit != 0) {
      if (lit != cube[size]) {
        if (match) backtrack (last, size);
        match =    0; }
      if (match == 0) {
        cube[size] = lit;
        write_lit (lit); }
      size++; }
    else {
      if (match) {
        backtrack (last, size - 1);
        write_lit (cube[size-1]); }
      last = size - 1;
      write_lit (0);
      size  = 0;
      match = 1; }
  }

  fclose (input);
  fclose (output);
}
