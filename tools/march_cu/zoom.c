#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char** argv) {
  int i, tmp, target, size = 0;

  FILE *input;

  input  = fopen (argv[1], "r");
  target = atoi  (argv[2]);

  double *values;

  values = (double *) malloc (sizeof(double) * target);

  double v, max = 0;
  while (1) {
    tmp = fscanf (input, " %lf ", &v);
    if (tmp != 1) break;
    if (v > max) max = v;
    if (size < target) values[size] = v;
    size++;
  };

  if (target <= size) {
    for (i = 0; i < size; i++)
      printf("1\n");
    exit (0); }

  double split = max * 0.5, diff = max * 0.25;

  int sum;
  do {
    sum = 0;
    for (i = 0; i < size; i++)
      sum += (int) ceil (values[i] / split);

    printf("%i\n", sum);
    if (sum  < target) split -= diff;
    if (sum  > target) split += diff;
    if (sum != target) diff  *=  0.5;
  }
  while (sum != target);

  printf("%.20f\n", split);

  for (i = 0; i < size; i++)
    printf("%i: %i\n", i + 1, (int) ceil(values[i] / split));


}
