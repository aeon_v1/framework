#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main (int argc, char** argv) {
  FILE *cubes;

  cubes = fopen (argv[1], "r");

  int tmp, lit;

  while (1) {
    tmp = fscanf (cubes, " a %i ", &lit);
    if (tmp == 0 || tmp == EOF) break;
    if (lit == 0) { printf ("0\n"); break; }
    do {
      printf("%i ", -lit);
      tmp = fscanf (cubes, " %i ", &lit);
      assert (tmp == 1);
    } while (lit);
    printf("0\n");
  }
  fclose (cubes);
}
