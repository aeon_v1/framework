#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MAX 	100000

static void printCube (int* cube, int size) {
  int i;
  printf("a ");
  for (i = 0; i < size; i++)
    printf("%i ", cube[i]);
  printf("0\n"); }

int main (int argc, char** argv) {
  if (argc < 2) {
    printf ("c wrong execution: ./cube-decode INPUT\n");
    exit (0); }

  FILE *input;
  input  = fopen (argv[1], "r");

  int i, *cube;
  cube = (int*) malloc (sizeof(int) * MAX);
  for (i = 0; i < MAX; i++) cube[i] = 0;

  int size = 0, last = 0;
  int nat = 0, lit = 0, shift = 0;
  while (1) {
    int tmp;
    char ch;

    tmp = fscanf(input, "%c", &ch);
    if (tmp == EOF) break;

    nat |= (((int) ch) & 127) << shift;
    shift += 7;

    if ((ch & 0x80) == 0) {
      last = lit;
      lit = (nat >> 1);
      if (nat & 1) lit *= -1;
      nat = 0;
      shift = 0;
      assert(size >= 0);
      if (lit != 0) cube[size++] = lit;
      else {
        if (last != 0) printCube (cube, size);
        size--; }
  } }

  fclose (input); }
