Require Export SetChecker.

Extraction Language Ocaml.

(***
Extract Inductive bool => Bool [ True False ].
***)
(*
Extract Inductive option => Maybe [ Just Nothing ].
Extract Inductive unit => "()" [ "()" ].
Extract Inductive list => "[ ]" [ "[]" "( : )" ].
Extract Inductive prod => "( , )" [ "( , )" ].
*)

(** NB: The "" above is a hack, but produce nicer code than "(,)" *)

(** Mapping sumbool to bool and sumor to option is not always nicer,
    but it helps when realizing stuff like [lt_eq_lt_dec] *)

(***
Extract Inductive sumbool => Bool [ True False ].
***)
(*
Extract Inductive sumor => Maybe [ Just Nothing ].
*)

(** Restore lazyness of andb, orb.
    NB: without these Extract Constant, andb/orb would be inlined
    by extraction in order to have lazyness, producing inelegant
    (if ... then ... else false) and (if ... then true else ...).
*)

(*
Extract Inlined Constant andb => "(&&)".
Extract Inlined Constant orb => "(||)".
*)

(***
Extract Inductive nat => Int [ "0" "succ" ]
 "(\ fO fS n -> if n==0 then (fO __) else fS (n-1))".
***)

(** Efficient (but uncertified) versions for usual [nat] functions *)

(*
Extract Constant plus => "(+)".
*)
(***
Extract Constant pred => "\ n -> max 0 (n-1)".
***)
(*
Extract Constant minus => "\ n m -> max 0 (n-m)".
Extract Constant mult => "( * )".
*)
(***
Extract Inlined Constant max => max.
***)
(*
Extract Inlined Constant min => min.
Extract Inlined Constant nat_beq => "(==)".
Extract Inlined Constant EqNat.beq_nat => "(==)".
Extract Inlined Constant EqNat.eq_nat_decide => "(==)".
*)

(***
Extract Inlined Constant Peano_dec.eq_nat_dec => "(==)".
***)

(*
Extract Constant Compare_dec.nat_compare =>
 "\ n m -> if n==m then EQ else if n<m then LT else GT".
Extract Inlined Constant Compare_dec.leb => "(<=)".
*)
(***
Extract Inlined Constant Compare_dec.le_lt_dec => "(<=)".
***)
(*
Extract Constant Compare_dec.lt_eq_lt_dec =>
 "\ n m -> if n>m then Nothing else Just (n<m)".

Extract Constant Even.even_odd_dec => "fun n -> n mod 2 = 0".
Extract Constant Div2.div2 => "fun n -> n/2".

Extract Inductive Euclid.diveucl => "(int * int)" [ "" ].
Extract Constant Euclid.eucl_dev => "fun n m -> (m/n, m mod n)".
Extract Constant Euclid.quotient => "fun n m -> m/n".
Extract Constant Euclid.modulo => "fun n m -> m mod n".
*)

(* LCF stuff
Extract Inlined Constant fst => fst.
Extract Inlined Constant snd => snd.
*)

Extract Constant LazyT "T" => "T Lazy.t".

Extraction lazy_list. 

Extraction "Checker" refute.
