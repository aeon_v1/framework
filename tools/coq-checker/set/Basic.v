Require Export List.
Require Export Arith.
Require Export Permutation.

(** This is stuff that complements the Coq standard library. *)

(** * Natural numbers *)

Lemma gt_neq : forall x y:nat, y < x -> x <> y.
intros; intro.
rewrite H0 in H; apply (lt_irrefl y); auto.
Qed.

Lemma lt_neq : forall x y:nat, x < y -> x <> y.
intros; intro.
rewrite H0 in H; apply (lt_irrefl y); auto.
Qed.

Lemma plus_minus_S_S : forall n m p, p + S n - S m = p + n - m.
intros.
induction m; simpl.
transitivity (S (p + n) - 1); auto.
transitivity (S (p + n) - S (S m)); auto.
Qed.

Lemma minus_S : forall n m, n - S m = n - m - 1.
induction n; auto with arith.
induction m; auto with arith.
simpl.
apply eq_add_S.
auto.
Qed.

(** ** Sign function *)

Definition sign (n:nat) :=
  match n with
  | O => 0
  | S _ => 1
  end.

Lemma sign_mon : forall i j, i <= j -> sign i <= sign j.
intros i j; elim i; elim j; simpl; auto.
intros; inversion H0.
Qed.

Lemma sign_lt : forall i j, sign i < sign j -> i < j.
intros i j; elim i; elim j; simpl; auto with arith.
intros; inversion H0.
intros; inversion H1; inversion H3.
Qed.

(** ** Comparisons *)

Lemma nat_compare_eq_rev : forall n, nat_compare n n = Eq.
intro; elim (nat_compare_eq_iff n n); auto.
Qed.

Lemma nat_compare_sym_lt : forall m n, nat_compare m n = Lt -> nat_compare n m = Gt.
double induction n m; simpl; auto.
intro; discriminate H.
Qed.

Lemma nat_compare_sym_gt : forall m n, nat_compare m n = Gt -> nat_compare n m = Lt.
double induction n m; simpl; auto.
intro; discriminate H.
Qed.

Lemma nat_compare_trans : forall m n o, nat_compare m n = Lt -> nat_compare n o = Lt ->
                                        nat_compare m o = Lt.
intros.
elim (nat_compare_lt m n); intros.
elim (nat_compare_lt m o); intros.
elim (nat_compare_lt n o); intros.
apply H3; transitivity n; auto.
Qed.

(** * Lists *)

Lemma nil_length : forall A, forall l:list A, length l = 0 -> l = nil.
intros A l; case l; simpl; auto.
intros; inversion H.
Qed.

Lemma is_nil_dec : forall A, forall l:list A, {l=nil}+{l<>nil}.
intros; case l; auto.
right; discriminate.
Qed.

Theorem list_length_induction : forall A, forall P:list A -> Prop,
        P nil ->
        (forall n, (forall l, length l = n -> P l) ->
                   (forall l, length l = (S n) -> P l)) ->
        forall l, P l.
intros; revert l.
assert (forall n l, length l = n -> P l).
induction n; dependent inversion l; intros; auto.
inversion H1.
apply H0 with n; auto.
intros; apply H1 with (length l); auto.
Qed.

Lemma all_in_dec : forall A B:Type, forall B_dec : (forall x y:B, {x=y}+{x<>y}),
                   forall f:A->B, forall l l',
                      {forall x, In x l -> In (f x) l'} +
                        {~(forall x, In x l -> In (f x) l')}.
intros.
induction l.
left; simpl; intros; inversion H.
inversion_clear IHl.
elim (In_dec B_dec (f a) l'); intro.
left; simpl; intros.
inversion_clear H0; auto.
rewrite <- H1; auto.
right; intro.
apply b; apply H0; left; auto.
right; intro.
apply H; intros; apply H0; right; auto.
Qed.

Lemma ext_map : forall (A B : Type)(f g:A->B), forall l, map f l = map g l ->
                forall a, In a l -> f a = g a.
induction l; intros; inversion H0.
simpl in H; rewrite <- H1; inversion H; auto.
inversion H; auto.
Qed.

Lemma map_ext_str : forall (A B : Type) (f g:A->B), forall l, (forall a, In a l -> f a = g a) -> map f l = map g l.
induction l; auto.
simpl; intros.
rewrite (H a); auto.
rewrite IHl; auto.
Qed.

Lemma nth_ok_length : forall A, forall l:list A, forall n x,
      n < length l -> nth_ok n l x = true.
induction l.
intros; inversion H.
intros.
destruct n; auto.
simpl; auto with arith.
Qed.

Lemma length_nth_ok : forall A, forall l:list A, forall n x,
      nth_ok n l x = true -> n < length l.
induction l.
destruct n; intros; inversion H.
destruct n.
simpl; auto with arith.
simpl; intros; apply lt_n_S; eauto.
Qed.

(** ** Changing values of list elements *)

Fixpoint change (n:nat) (A:Type) (l:list A) (x:A) : list A :=
    match n, l with
      | O, nil => nil
      | O, y :: l' => x :: l'
      | S m, nil => nil
      | S m, y :: l' => y :: change m A l' x
    end.

Lemma change_len : forall n A l x, length (change n A l x) = length l.
induction n; induction l; simpl; auto.
Qed.

Lemma change_In : forall n A l x y, In y (change n A l x) -> (x = y) \/ In y l.
induction n; induction l; simpl; auto; intros; inversion_clear H; auto.
elim (IHn _ _ _ _ H0); auto.
Qed.

Lemma change_nth_k : forall A x n a l, n < length l ->
                       nth n (change n A l x) a = x.
induction n; induction l; simpl; auto.
intro H; inversion H.
intro H; inversion H.
intro; apply IHn; auto with arith.
Qed.

Lemma change_nth_neq : forall A:Type, forall x:A, forall k n a l,
                       n < length l -> k <> n ->
                       nth k (change n A l x) a = nth k l a.
induction k.
induction l; intros; simpl; auto.
inversion H.
rewrite (S_pred n 0); simpl; auto with arith.

intros n a l; simpl.
elim l; simpl.
intro H; inversion H.
clear l; intros y l.
intros.
elim (le_lt_dec n 0); intro Hn.
inversion Hn; simpl; auto.
rewrite (S_pred _ _ Hn).
apply IHk.
rewrite (S_pred _ _ Hn) in H0; auto with arith.
rewrite (S_pred _ _ Hn) in H1; auto with arith.
Qed.

Arguments change: default implicits.

(** ** Useful criterium for list equality *)

Lemma list_eq : forall P Q, length P = length Q ->
               (forall i x, i < length P -> nth (A:=nat) i P x = nth i Q x) ->
                P = Q.
intro P; induction P; intro Q; case Q; simpl; intros; auto.
inversion H.
inversion H.
replace a with n.
replace P with l; auto.
symmetry; apply IHP.
auto with arith.
intros; apply (H0 (S i)); auto with arith.
symmetry; apply (H0 0); auto with arith.
Qed.

Theorem nth_In_Prop : forall A, forall P:A -> Prop, forall l, forall k:nat,
                      k < length l -> (forall x, In x l -> P x) ->
                      forall a, P (nth k l a).
intros.
apply H0; auto.
apply nth_In; auto.
Qed.

(** ** Miscellaneous about In *)

Lemma In_char : forall A, (forall x y:A, {x=y} + {x<>y}) -> forall l:list A,
                          forall x y, In x l -> {i | i < length l & nth i l y = x}.
induction l.
simpl; intros; inversion H.
intros.
elim (X a x).
exists 0; simpl; auto with arith.
intros.
elim (IHl x y).
intros j Hj; exists (S j); simpl; auto with arith.
elim H; auto.
intro; elim b; auto.
Qed.

Lemma In_suff : forall A, (forall x y:A, {x=y} + {x<>y}) -> forall l:list A,
                          forall x y i, i < length l -> nth i l y = x -> In x l.
induction l.
intros; inversion H.
intros x y i; case i; simpl; auto.
intros; right.
apply IHl with y n; auto with arith.
Qed.

Lemma map_in : forall A B, forall f:A->B, forall l y,
                           In y (map f l) -> exists x, f x = y /\ In x l.
intros; elim (in_map_iff f l y); auto.
Qed.

Arguments map_in : default implicits.

(** ** Miscellaneous about remove *)

Section Remove.

Variable A:Type.
Hypothesis A_dec : forall x y:A, {x=y} + {x <> y}.

Lemma remove_app : forall l l' x,
                   remove A_dec x (l++l') = remove A_dec x l ++ remove A_dec x l'.
induction l; simpl; auto.
intros.
elim (A_dec x a); simpl; auto.
intro; rewrite IHl; auto.
Qed.

Lemma remove_not_in : forall l x, ~In x l -> remove A_dec x l = l.
induction l; simpl; auto.
intros.
elim (A_dec x a).
intro; exfalso; apply H; auto.
intro; rewrite IHl; auto.
Qed.

Lemma in_remove : forall l x y, In x l -> x <> y -> In x (remove A_dec y l).
induction l; simpl; intros; auto.
elim A_dec; simpl; intro; inversion_clear H; auto.
exfalso; apply H0; rewrite a0; auto.
Qed.

Lemma remove_out : forall l x y, ~(In x l) -> ~(In x (remove A_dec y l)).
induction l; simpl; intros; auto.
elim A_dec; simpl; intros.
apply IHl.
intro; apply H; auto.
intro; inversion_clear H0; auto.
generalize H1; apply IHl; auto.
Qed.

Lemma remove_In_neq : forall l x y, In x (remove A_dec y l) -> In x l.
induction l; simpl; auto.
intros x y; elim A_dec; intros.
right; apply IHl with y; auto.
inversion_clear H; auto.
right; apply IHl with y; auto.
Qed.

Lemma remove_length : forall l x, length (remove A_dec x l) <= length l.
induction l; simpl; auto with arith; intros.
elim A_dec; simpl; auto with arith.
Qed.

Lemma remove_length_In : forall l x, In x l ->
                                     length (remove A_dec x l) < length l.
induction l; intros.
inversion H.
unfold remove; elim A_dec; simpl; intros; fold (remove A_dec x l).
eapply le_lt_trans.
apply remove_length.
auto.
apply lt_n_S; apply IHl.
inversion_clear H; auto.
elim b; auto.
Qed.

(** Remove and count *)

Lemma count_length : forall l x, count_occ A_dec l x <= length l.
induction l; auto with arith.
intros; simpl; elim A_dec; auto with arith.
Qed.

Lemma count_remove : forall l x y, x <> y -> count_occ A_dec l x =
                                             count_occ A_dec (remove A_dec y l) x.
induction l; auto with arith.
intros; simpl; elim A_dec; simpl; elim A_dec; auto with arith.
intros; elim H; transitivity a; auto.
simpl; intros; elim A_dec; simpl; auto with arith.
intro H0; elim H0; auto.
simpl; intros; elim A_dec; simpl; auto with arith.
intro; elim b0; auto.
Qed.

Lemma remove_count_length : forall l x, length (remove A_dec x l) =
                                        length l - count_occ A_dec l x.
induction l; simpl; auto; intros.
elim A_dec; simpl; elim A_dec; intros; auto.
elim b; auto.
elim b; auto.
rewrite IHl.
rewrite minus_Sn_m; auto with arith.
apply count_length.
Qed.

(** More on count *)

Lemma count_occ_zero : forall l x, ~In x l -> count_occ A_dec l x = 0.
induction l; simpl; auto.
intros.
elim A_dec; simpl; auto.
intro; elim H; auto.
Qed.

Lemma zero_count : forall l x, count_occ A_dec l x = 0 -> ~In x l.
intros; intro.
apply (lt_irrefl 0).
pattern O at 2; rewrite <- H.
apply count_occ_In; auto.
Qed.

(** ** Miscellaneous about NoDup *)

Lemma Permutation_NoDup : forall A, forall P Q: list A, Permutation P Q ->
                                                        NoDup P -> NoDup Q.
intros.
induction H; auto.
inversion_clear H0; apply NoDup_cons; auto.
intro; apply H1; apply Permutation_in with l'; auto.
apply Permutation_sym; auto.
inversion_clear H0; inversion_clear H1.
apply NoDup_cons.
intro; inversion_clear H1; auto.
apply H; left; auto.
apply NoDup_cons; auto.
intro; apply H; right; auto.
Qed.

Lemma NoDup_dec : forall l:list A, {NoDup l} + {~NoDup l}.
induction l; simpl; intros.
left; apply NoDup_nil.
elim (In_dec A_dec a l); intro.
right; intro.
inversion_clear H.
apply H0; auto.
elim IHl; intro.
left; apply NoDup_cons; auto.
right; intro; apply b0.
inversion_clear H; auto.
Qed.

Lemma NoDup_remove : forall l a, NoDup l -> NoDup (remove A_dec a l).
induction l; simpl; auto.
intros; elim A_dec; intros; auto.
inversion_clear H; auto.
inversion_clear H.
apply NoDup_cons; auto.
apply remove_out; auto.
Qed.

Lemma NoDup_append : forall l, forall a:A, NoDup (a::l) -> NoDup (l ++ a :: nil).
induction l; simpl; auto.
intros b Hb.
inversion_clear Hb.
inversion_clear H0.
apply NoDup_cons.
intro.
elim (in_app_or _ _ _ H0); auto.
intro; apply H.
left; inversion_clear H3; auto.
elim H4.
apply IHl; apply NoDup_cons; auto.
intro; apply H; right; auto.
Qed.

Lemma NoDup_char_1 : forall l:list A,
                    (forall i j x, i < length l -> j < length l ->
                                   i<>j -> nth i l x <> nth j l x) -> NoDup l.
induction l.
intros; apply NoDup_nil.
intros; apply NoDup_cons.
intro.
elim (In_char _ A_dec _ _ a H0).
simpl; intros.
elim (H 0 (S x) a); simpl; auto with arith.
apply IHl; auto.
intros.
change (nth (S i) (a::l) x <> nth (S j) (a::l) x).
apply H; simpl; auto with arith.
Qed.

Lemma NoDup_char_2 : forall l:list A, NoDup l ->
                     forall i j x, i < length l -> j < length l ->
                                   i<>j -> nth i l x <> nth j l x.
induction l.
intros; inversion H0.
intros H i j.
inversion H; case i; case j; simpl; auto with arith;
intros; intro; apply H2; apply In_suff with x0 n; auto with arith.
Qed.

Lemma NoDup_char_3 : forall l:list A, NoDup l ->
                     forall i j x, i < length l -> j < length l ->
                                   nth i l x = nth j l x -> i=j.
intros.
elim (eq_nat_dec i j); auto.
intro; exfalso.
generalize H2; apply NoDup_char_2; auto.
Qed.

(** NoDup and count *)

Lemma NoDup_count : forall l x, NoDup l -> In x l -> count_occ A_dec l x = 1.
induction l; simpl; auto.
intros; elim H0.
intros; elim A_dec; simpl; intros.
replace (count_occ A_dec l x) with 0; auto.
rewrite count_occ_zero; auto.
rewrite <- a0; inversion H; auto.
inversion H.
apply IHl; auto.
inversion_clear H0; auto.
elim b; auto.
Qed.

Lemma count_NoDup : forall l, (forall x, count_occ A_dec l x <= 1) -> NoDup l.
induction l.
intros; apply NoDup_nil.
intros.
generalize (H a).
rewrite count_occ_cons_eq; auto; intros.
assert (count_occ A_dec l a = 0).
inversion H0; auto.
inversion H2; auto.
clear H0.
apply NoDup_cons.
apply zero_count; auto.
apply IHl.
intros; elim (A_dec a x); intros.
rewrite <- a0; rewrite H1; auto.
rewrite <- (count_occ_cons_neq A_dec l b); auto.
Qed.

Lemma NoDup_count_length_le : forall l: list A, NoDup l ->
                              forall l', (forall x, In x l -> In x l') ->
                                          length l <= length l'.
induction l; simpl; auto with arith.
intros.
inversion_clear H.
apply le_lt_trans with (length (remove A_dec a l')).
apply IHl; auto.
intros; elim (A_dec x a); intros.
elim H1; rewrite <- a0; auto.
apply in_remove; auto.
apply remove_length_In; auto.
Qed.

Lemma NoDup_count_length_lt : forall l: list A, NoDup l ->
                              forall l', (forall x, In x l -> In x l') ->
                              forall y, count_occ A_dec l' y > 1 ->
                                        length l < length l'.
intros.
apply le_lt_trans with (length (y::(remove A_dec y l'))).
apply NoDup_count_length_le; auto.
intro x; elim (A_dec x y); intros.
left; auto.
right; apply in_remove; auto.
simpl.
rewrite remove_count_length.
apply lt_le_trans with (S (length l' - 1)).
apply lt_n_S.
unfold lt; simpl.
rewrite minus_Sn_m.
2: apply count_length.
replace (length l'-1) with (S(length l')-2).
2: auto with arith.
apply minus_le_compat_l; auto.
rewrite <- pred_of_minus.
rewrite <- S_pred with (length l') 0; auto.
apply lt_le_trans with (count_occ A_dec l' y); auto with arith.
apply count_length.
Qed.

Lemma NoDup_count_occ : forall l x, NoDup l -> count_occ A_dec l x <= 1.
intros.
elim (In_dec A_dec x l); intro.
rewrite NoDup_count; auto.
rewrite count_occ_zero; auto.
Qed.

Lemma NoDup_lemma : forall l l':list A, length l = length l' -> NoDup l ->
                               (forall x, In x l -> In x l') -> NoDup l'.
intros.
apply count_NoDup; intros.
apply not_gt; intro.
apply (lt_irrefl (length l')).
apply le_lt_trans with (length l).
rewrite H; auto.
apply NoDup_count_length_lt with x; auto.
Qed.

Lemma NoDup_app : forall l l':list A, NoDup l -> NoDup l' ->
                 (forall x, In x l -> ~In x l') -> NoDup (l++l').
induction l; simpl; auto.
intros.
inversion_clear H.
apply NoDup_cons.
intro; elim (in_app_or _ _ _ H); auto.
apply H1; auto.
apply IHl; auto.
Qed.

End Remove.

(** ** Find-and-remove
    Takes a list and two elements, checks whether they are both in the list, and removes the second.
    Not used anymore.
 *)

Section FindAndRemove.

Variable A:Type.
Hypothesis A_dec : forall x y:A, {x = y} + {x <> y}.

Fixpoint remove_fst (x:A) (l:list A) : list A :=
  match l with
  | nil => nil
  | y :: l' => match (A_dec x y) with
               | left _ => l'
               | right _ => y :: remove_fst x l'
               end
  end.

Lemma remove_fst_NoDup : forall l, NoDup l -> forall x, remove_fst x l = remove A_dec x l.
induction l; auto.
intros; inversion_clear H.
simpl; elim A_dec; intros.
symmetry; apply remove_not_in.
rewrite a0; auto.
rewrite IHl; auto.
Qed.

Lemma remove_fst_not_In : forall l x, ~In x l -> remove_fst x l = remove A_dec x l.
induction l; auto.
simpl; intros; rewrite IHl.
elim A_dec; auto.
intro; elim H; auto.
intro; elim H; auto.
Qed.

Lemma remove_fst_length_In : forall l x, In x l ->
                             length (remove_fst x l) < length l.
induction l; intros.
inversion H.
unfold remove_fst; elim A_dec; simpl; intros; auto.
fold (remove_fst x l).
apply lt_n_S; apply IHl.
inversion_clear H; auto.
elim b; auto.
Qed.

Fixpoint find_and_remove (a b:bool) (l:list A) (x y:A)
  : bool * bool * (list A) :=
  match (a,b,l) with
  | (true,true,_) => (true,true,l)
  | (_,_,nil) => (a,b,nil)
  | (false,true,z::l') => match (A_dec x z) with
      | left _ => (true,true,z::l')
      | right _ => let (t1,t2) := find_and_remove a b l' x y
                   in (t1,z::t2)
      end
  | (true,false,z::l') => match (A_dec y z) with
          | left _ => (true,true,l')
          | right _ => let (t1,t2) := find_and_remove a b l' x y
                       in (t1,z::t2)
          end
  | (false,false,z::l') => match (A_dec x z) with
      | left _ => let (t1,t2) := find_and_remove true b l' x y
                  in (t1,z::t2)
      | right _ => match (A_dec y z) with
          | left _ => find_and_remove a true l' x y
          | right _ => let (t1,t2) := find_and_remove a b l' x y
                       in (t1,z::t2)
          end
      end
  end.

Lemma find_and_remove_true_true : forall l x y, find_and_remove true true l x y = (true,true,l).
induction l; auto.
Qed.

Lemma find_and_remove_1_true : forall l b x y, fst (fst (find_and_remove true b l x y)) = true.
induction l; simpl; auto.
induction b; auto.
intros b x y; elim b; auto.
elim A_dec; simpl; auto.
intros.
pattern true at 2; rewrite <- IHl with false x y.
elim (find_and_remove true false l x y); simpl; auto.
Qed.

Lemma find_and_remove_1 : forall l a b x y,
      fst (fst (find_and_remove a b l x y)) = false -> ~In x l.
induction l; simpl; auto.
rename a into z; intros a b x y; repeat (elim A_dec); simpl; auto.
destruct a; destruct b; simpl; intros; try (inversion H; fail).
generalize (find_and_remove_1_true l false x y); revert H.
elim (find_and_remove true false l x y); simpl; intros.
rewrite H0 in H; inversion H.
destruct a; destruct b; simpl; intros; try (inversion H; fail).
intro; inversion_clear H0; auto.
revert H1; apply (IHl false true x y).
revert H; elim (find_and_remove false true l x y); auto.
intro; inversion_clear H0; auto.
revert H1; apply (IHl false true x y).
revert H; elim (find_and_remove false true l x y); auto.

generalize (find_and_remove_1_true l b x y).
destruct a; destruct b; simpl; try (intros; inversion H0; fail).
elim (find_and_remove true false l x y); simpl; intros.
rewrite H0 in H; inversion H.
elim (find_and_remove true false l x y); simpl; intros.
rewrite H0 in H; inversion H.

induction a.
generalize (find_and_remove_1_true l b x y).
destruct b; simpl; try (intros; inversion H0; fail).
elim (find_and_remove true false l x y); simpl; intros.
rewrite H0 in H; inversion H.

generalize (IHl false b x y).
destruct b; simpl.
elim (find_and_remove false true l x y); simpl; intros.
intro; inversion_clear H1; auto.
apply H; auto.
elim (find_and_remove false false l x y); simpl; intros.
intro; inversion_clear H1; auto.
apply H; auto.
Qed.

Lemma find_and_remove_1a : forall l b x y,
      fst (fst (find_and_remove false b l x y)) = true -> In x l.
induction l; simpl; auto.
destruct b; simpl; intros; discriminate H.
intros b x y; elim A_dec; simpl; auto.
induction b; simpl; auto.
right; apply IHl with true y; auto.
revert H; elim (find_and_remove false true l x y); auto.
elim A_dec.
right; apply IHl with true y; auto.
right; apply IHl with false y; auto.
revert H; elim (find_and_remove false false l x y); simpl; auto.
Qed.

Lemma find_and_remove_2_true : forall l a x y, snd (fst (find_and_remove a true l x y)) = true.
induction l; simpl.
intros; case a; auto.
rename a into z; intros a x y; elim a; simpl; auto.
elim A_dec; simpl; auto.
intros.
pattern true at 2; rewrite <- IHl with false x y.
elim (find_and_remove false true l x y); simpl; auto.
Qed.

Lemma find_and_remove_2 : forall l a b x y, x <> y -> 
      snd (fst (find_and_remove a b l x y)) = false -> ~In y l.
induction l; simpl; auto.
rename a into z; intros a b x y.
induction a; simpl; auto.
induction b; simpl; auto.
intros; inversion H0.
elim A_dec; simpl; auto.
intros; inversion H0.
intros; intro; inversion_clear H1; auto.
revert H2; apply IHl with true false x; auto.
revert H0; elim (find_and_remove true false l x y); auto.
induction b; elim A_dec; simpl; auto.
intros; inversion H0.
generalize (find_and_remove_2_true l false x y).
elim (find_and_remove false true l x y); simpl; auto.
intros; rewrite H1 in H; inversion H.
intros; intro; inversion_clear H1.
apply H; rewrite a; auto.
revert H2; apply IHl with true false x; auto.
revert H0; elim (find_and_remove true false l x y); auto.
elim A_dec; simpl; auto.
generalize (find_and_remove_2_true l false x y).
elim (find_and_remove false true l x y); simpl; auto.
intros; rewrite H1 in H; inversion H.
intros; intro; inversion_clear H1; auto.
apply (IHl false false x y); auto.
revert H0; elim (find_and_remove false false l x y); auto.
Qed.

Lemma find_and_remove_2a : forall l a x y,
      snd (fst (find_and_remove a false l x y)) = true -> In y l.
induction l; simpl; auto.
intros; induction a; try discriminate H.
rename a into z; intros a x y; elim A_dec; simpl; auto.
induction a; auto.
right; apply IHl with true x; auto.
revert H; elim (find_and_remove true false l x y); auto.
elim A_dec; simpl; auto.
right; apply IHl with true x; auto.
revert H; elim (find_and_remove true false l x y); auto.
right; apply IHl with false x; auto.
revert H; elim (find_and_remove false false l x y); auto.
Qed.

Lemma find_and_remove_3a : forall l a x y,
      snd (find_and_remove a true l x y) = l.
induction l; simpl; auto.
induction a; auto.
rename a into z; intros a x y.
induction a; auto.
elim A_dec; simpl; auto.
pattern l at 2; rewrite <- (IHl false x y).
elim (find_and_remove false true l x y); simpl; auto.
Qed.

Lemma find_and_remove_3b : forall l a x y, x <> y ->
      snd (find_and_remove a false l x y) = remove_fst y l.
induction l; simpl; auto.
induction a; auto.
rename a into z; intros a x y.
induction a; auto.
elim A_dec; simpl; auto.
intros; rewrite <- (IHl true x y); auto.
elim (find_and_remove true false l x y); simpl; auto.

repeat elim A_dec; simpl; auto.
intros; elim H; transitivity z; auto.
intros; rewrite <- (IHl true x y); auto.
elim (find_and_remove true false l x y); simpl; auto.
intros; apply find_and_remove_3a.

intros; rewrite <- (IHl false x y); auto.
elim (find_and_remove false false l x y); simpl; auto.
Qed.

Lemma find_and_remove_not_in : forall l a b x y, x <> y -> ~In y l ->
      snd (find_and_remove a b l x y) = l.
destruct b; intros.
rewrite find_and_remove_3a; auto.
rewrite find_and_remove_3b; auto.
rewrite remove_fst_not_In; auto.
apply remove_not_in; auto.
Qed.

Lemma find_and_remove_3c : forall l a x y, x <> y ->
  NoDup l ->
  snd (find_and_remove a false l x y) = remove A_dec y l.
intros; rewrite find_and_remove_3b; auto.
rewrite remove_fst_NoDup; auto.
Qed.

Lemma find_and_remove_le : forall l l' a a' b b' x y,
      find_and_remove a b l x y = (a',b',l') -> length l' <= length l.
induction l; simpl.
destruct a; destruct b; intros; inversion H; auto with arith.

rename a into z; intros l' a a' b b' x y.
destruct a; simpl; auto.
destruct b; simpl; auto.
intro; inversion H; auto with arith.

elim A_dec; simpl; auto.
intros; inversion H; auto with arith.
set (WW := find_and_remove true false l x y).
generalize (IHl (snd WW) true a' false b' x y).
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.

destruct b; elim A_dec; simpl; auto.

intros; inversion H; auto with arith.

set (WW := find_and_remove false true l x y).
generalize (IHl (snd WW) false a' true b' x y).
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.
set (WW := find_and_remove true false l x y).
generalize (IHl (snd WW) true a' false b' x y).
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.

elim A_dec; simpl.
intros.
assert (snd (find_and_remove false true l x y) = l').
rewrite H; auto.
rewrite find_and_remove_3a in H0.
rewrite H0; auto with arith.
set (WW := find_and_remove false false l x y).
generalize (IHl (snd WW) false a' false b' x y).
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.
Qed.

Lemma find_and_remove_smaller : forall l l' x y a a',
      find_and_remove a false l x y = (a',true,l') -> length l' < length l.
induction l; simpl; auto.
destruct a; intros; inversion H.

rename a into z; intros l' x y a a'.
destruct a; simpl; auto.

elim A_dec; simpl; auto.
intros; inversion H; auto with arith.
set (WW := find_and_remove true false l x y).
generalize (IHl (snd WW) x y true a').
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.

elim A_dec; simpl; auto.

set (WW := find_and_remove true false l x y).
generalize (IHl (snd WW) x y true a').
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.

elim A_dec; simpl.
intros.
assert (snd (find_and_remove false true l x y) = l').
rewrite H; auto.
rewrite find_and_remove_3a in H0.
rewrite H0; auto with arith.
set (WW := find_and_remove false false l x y).
generalize (IHl (snd WW) x y false a').
fold WW; clear IHl; elim WW.
clear WW; simpl; intros.
inversion H0.
simpl; apply le_n_S.
apply H; rewrite H2; auto.
Qed.

End FindAndRemove.

Section Split.

Variable A:Type.
Variable A_dec : forall x y : A, {x = y} + {x <> y}.

Fixpoint split (x:A) (l:list A) : list A * list A :=
  match l with
  | nil => (nil,nil)
  | y :: l' => match A_dec x y with
               | left _ => (nil,l')
               | right _ => let (w,w') := (split x l')
                            in (y :: w, w')
               end
  end.

Lemma split_char_in : forall x l, In x l -> forall w w', split x l = (w,w') ->
                      w ++ w' = remove_fst A A_dec x l.
induction l; simpl; intros.
inversion H0; auto.
revert H0; elim A_dec; intros.
inversion H0; auto.
inversion_clear H.
elim b; auto.
revert H0.
set (W := split x l).
assert (W = split x l); auto; clearbody W; induction W.
intro; inversion H0.
simpl; rewrite (IHl H1 a0 w'); auto.
rewrite <- H4; auto.
Qed.

Lemma split_char_out : forall x l, ~In x l -> split x l = (l,nil).
induction l; auto.
simpl; intros.
elim A_dec; simpl; intros.
elim H; auto.
rewrite IHl; auto.
Qed.

Lemma split_length_1 : forall x l, In x l ->
                       forall w w', split x l = (w,w') -> length w < length l.
induction l.
intros; inversion H.
simpl; elim A_dec; intros.
inversion H0; auto with arith.
inversion_clear H.
elim b; auto.
revert H0.
set (W := split x l).
assert (W = split x l); auto; clearbody W; induction W.
intro; inversion H0.
simpl; apply lt_n_S.
apply (IHl H1 a0 w'); auto.
rewrite <- H4; auto.
Qed.

Lemma split_length_2 : forall x l, In x l ->
                       forall w w', split x l = (w,w') -> length w' < length l.
induction l.
intros; inversion H.
simpl; elim A_dec; intros.
inversion H0; auto with arith.
inversion_clear H.
elim b; auto.
revert H0.
set (W := split x l).
assert (W = split x l); auto; clearbody W; induction W.
intro; inversion H0.
simpl; apply lt_S.
apply (IHl H1 a0 w'); auto.
rewrite <- H4; auto.
Qed.

Lemma In_split : forall x l y, x <> y ->
                 forall w w', split x l = (w,w') -> In y l -> In y (w++w').
induction l.
intros; inversion H1.
simpl; elim A_dec; intros.
inversion_clear H1.
elim H; rewrite a0; auto.
inversion H0.
simpl; rewrite <- H4; auto.
inversion_clear H1.
revert H0.
set (W := split x l).
assert (W = split x l); auto; clearbody W; induction W.
intro; inversion H1.
simpl; left; auto.
revert H0.
set (W := split x l).
assert (W = split x l); auto; clearbody W; induction W.
intro; inversion H1.
right; apply IHl; auto.
rewrite <- H5; auto.
Qed.

End Split.

Arguments split : default implicits.

Section RemoveAll.

Variable A:Type.
Variable A_dec : forall x y : A, {x = y} + {x <> y}.

Fixpoint remove_all (l w:list A) :=
  match l with
  | nil => w
  | x :: l' => match w with
            | nil => nil
            | y::w' => match (A_dec x y) with
                       | left _ => remove_all l' w'
                       | right _ => y :: remove_all l w'
                       end
            end
  end.

Lemma remove_all_length_le : forall l w, length (remove_all l w) <= length w.
induction l; induction w; auto.
simpl; elim A_dec; intros; auto with arith.
simpl; auto with arith.
Qed.

Lemma remove_all_length_lt : forall x l w, In x w -> length (remove_all (x::l) w) < length w.
induction l; induction w; intros.
inversion H.
simpl; elim A_dec.
intros; apply le_n_S; apply remove_all_length_le.
inversion_clear H.
intros; elim b; auto.
intros; apply lt_n_S; auto.
inversion H.
simpl; elim A_dec; intros.
apply le_n_S; apply remove_all_length_le.
apply le_n_S.
change (S (length (remove_all (x::a::l) w)) <= length w).
apply IHw.
inversion_clear H; auto.
elim b; auto.
Qed.

Lemma remove_all_in : forall l w x, In x (remove_all l w) -> In x w.
induction l; induction w; auto.
simpl; elim A_dec; intros; auto.
inversion_clear H; auto.
Qed.

Lemma remove_all_nil : forall l, remove_all l nil = nil.
induction l; auto.
Qed.

Lemma remove_all_NoDup : forall l w, NoDup w -> NoDup (remove_all l w).
induction l; induction w; auto.
intro; inversion_clear H.
simpl; elim A_dec; auto.
intros; apply NoDup_cons; auto.
intro; apply H0.
apply remove_all_in with (a::l); auto.
Qed.

Lemma remove_all_not_in : forall l w x, In x w -> ~In x (remove_all l w) -> In x l.
induction l; induction w; simpl; auto.
intros; inversion H.
intros x Hx; elim A_dec.
intros; inversion_clear Hx.
left; transitivity a0; auto.
right; apply IHl with w; auto.
simpl; intros.
inversion_clear Hx.
elim H; auto.
apply IHw; auto.
Qed.

End RemoveAll.

Arguments remove_all : default implicits.
