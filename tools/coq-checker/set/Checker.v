Require Export Clause.
Require Export Recdef.

Definition true_C : Clause := (pos 1::neg 1::nil).

Lemma true_C_true : forall v:Valuation, C_satisfies v true_C.
simpl.
intro; elim (v 1); auto.
Qed.

Lemma satisfies_forall : forall v c, satisfies v c ->
  forall c', In c' c -> C_satisfies v c'.
induction c; intros; inversion_clear H0.
inversion_clear H; rewrite <- H1; auto.

apply IHc; auto.
inversion_clear H; auto.
Qed.

Lemma forall_satisfies : forall v c, (forall c', In c' c -> C_satisfies v c') ->
  satisfies v c.
induction c; simpl; auto.
Qed.

Lemma CNF_empty : forall c, entails c nil -> unsat c.
red; intros; intro.
apply C_unsat_empty with v.
apply H; auto.
Qed.

Definition ICNF := list (nat*Clause).

Definition empty_ICNF : ICNF := nil.

Fixpoint ICNF_to_CNF (c:ICNF) : CNF :=
  match c with
  | nil => nil
  | c'::c => let (_,l) := c' in l :: (ICNF_to_CNF c)
  end.

Coercion ICNF_to_CNF : ICNF >-> CNF.

Fixpoint get_ICNF (c:ICNF) (i:nat) : Clause :=
  match c with
  | nil => true_C
  | c'::cs => let (j,cl) := c' in if (eq_nat_dec i j) then cl else (get_ICNF cs i)
  end.

Lemma get_ICNF_in_or_default : forall i c,
  {In (get_ICNF c i) (ICNF_to_CNF c)} + {get_ICNF c i = true_C}.
induction c; auto.
simpl; elim a; simpl; auto.
intros j l; elim eq_nat_dec; simpl; auto.
inversion_clear IHc; auto.
Qed.

Fixpoint del_ICNF (i:nat) (c:ICNF) : ICNF :=
  match c with
  | nil => nil
  | c'::cs => let (j,cl) := c' in if (eq_nat_dec i j) then cs else c'::(del_ICNF i cs)
  end.

Definition add_ICNF (i:nat) (cl:Clause) (c:ICNF) := ((i,cl) :: c) : ICNF.

Definition Set_Clause := Clause.

Definition Clause_to_Set_Clause : Clause -> Set_Clause := id (A := Clause).

Definition Set_empty : Set_Clause := (nil (A := Literal)).

Lemma Set_clause_eq_nil_cons : forall c: Set_Clause,
  {l : Literal & {c' : Set_Clause | c = l :: c'}} + {c = Set_empty}.
intro c; case c; auto.
clear c; intros l c; left.
exists l; exists c; auto.
Qed.

Definition insert : Literal -> Set_Clause -> Set_Clause := (cons (A := Literal)).

Fixpoint propagate (cs: ICNF) (c: Set_Clause) (is:list nat) : bool := 
  match is with
  | nil => false
  | (i::is) => match Set_clause_eq_nil_cons (remove_all literal_eq_dec c (Clause_to_Set_Clause (get_ICNF cs i))) with
    | inright _ => true
    | inleft H => let (l,Hl) := H in let (c',_) := Hl in
                  match Set_clause_eq_nil_cons c' with
                  | inright _ => propagate cs (insert (negate l) c) is
                  | inleft _ => false
  end end end.

Lemma propagate_lemma_1 : forall cs c c', In c cs ->
  entails (c::cs) c' -> entails cs c'.
intros.
red; intros.
apply H0; auto.
split; auto.
apply satisfies_forall with cs; auto.
Qed.

Lemma propagate_lemma_2 : forall cs c', entails (true_C::cs) c' -> entails cs c'.
intros.
red; intros.
apply H; split; auto.
apply true_C_true.
Qed.

Lemma propagate_sound : forall is cs c, propagate cs c is = true -> entails cs c.
induction is; simpl; intros.
inversion H.
revert H.
rename a into i.
elim Set_clause_eq_nil_cons; intros.
induction a; induction p.
revert H; elim Set_clause_eq_nil_cons; intros.
inversion H.
rename x into l.
rewrite b in p; clear b x0.
elim (get_ICNF_in_or_default i cs); intro.
apply propagate_lemma_1 with (get_ICNF cs i); auto; apply propagate_singleton with l; auto.
apply propagate_lemma_2; apply propagate_singleton with l; auto.
rewrite b in p; auto.

elim (get_ICNF_in_or_default i cs); intro.
apply propagate_lemma_1 with (get_ICNF cs i); auto; apply propagate_empty; auto.
apply propagate_lemma_2; apply propagate_empty; auto.
rewrite b0 in b; auto.
Qed.

Inductive Action : Type :=
  | D : list nat -> Action
  | O : nat -> Clause -> Action
  | R : nat -> Clause -> list nat -> Action.

Definition Oracle := list Action.

Fixpoint Oracle_size (O:Oracle) : nat :=
  match O with
  | nil => 0
  | (D is)::O' => length is + 1 + Oracle_size O'
  | _::O' => 1 + Oracle_size O'
  end.

Definition Answer := bool.

Function refute_work (w:ICNF) (c:CNF) (O:Oracle) {measure Oracle_size O} : Answer :=
  match O with
  | nil => false
  | (D nil)::O' => refute_work w c O'
  | (D (i::is))::O' => refute_work (del_ICNF i w) c ((D is)::O')
  | (O i cl)::O' => if (In_dec clause_eq_dec cl c) then (refute_work (add_ICNF i cl w) c O') else false
  | (R i nil is)::O' => propagate w Set_empty is
  | (R i cl is)::O' => andb (propagate w (Clause_to_Set_Clause cl) is) (refute_work (add_ICNF i cl w) c O')
  end.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
Defined.

Definition refute (c:CNF) (O:Oracle) : Answer :=
  refute_work empty_ICNF c O.


(** move me!!! **)

Lemma unsat_subset : forall c c', (forall cl, In cl c -> In cl c') -> unsat c -> unsat c'.
intros; intro; intro.
apply H0 with v.
apply forall_satisfies; intros; apply satisfies_forall with c'; auto.
Qed.

Lemma del_ICNF_in : forall i c cl, In cl ((del_ICNF i c):CNF) -> In cl (c:CNF).
induction c; auto.
intros; simpl.
revert H; simpl.
induction a; elim eq_nat_dec; simpl; auto.
intros.
inversion_clear H; auto.
Qed.

Lemma add_ICNF_unsat : forall i cl, forall c:ICNF,
  unsat ((add_ICNF i cl c):ICNF) -> entails c cl -> unsat c.
intros; intro; intro.
apply H with v.
split; auto.
Qed.

Lemma refute_work_correct : forall w c O, refute_work w c O = true -> unsat (app c (w : CNF)).
intros w c O; functional induction (refute_work w c O); intros; auto.
(* 1/6 *)
inversion H.
(* 2/6 *)
apply unsat_subset with (c ++ ((del_ICNF i w) : CNF)); auto.
intros.
apply in_or_app; elim (in_app_or _ _ _ H0); auto.
right.
eapply del_ICNF_in; eauto.
(* 3/6 *)
intro; intro; apply IHa with v; auto.
apply forall_satisfies; intros.
simpl in H1.
elim (in_app_or _ _ _ H1); intros; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
inversion_clear H2; auto.
rewrite <- H3.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
(* 4/6 *)
inversion H.
(* 5/6 *)
apply unsat_subset with (w:CNF).
intros; apply in_or_app; auto.
apply CNF_empty.
apply propagate_sound with is; auto.
(* 6/6 *)
elim (andb_true_eq _ _ (eq_sym H)); clear H; intros.
intro; intro; apply IHa with v; auto.
apply forall_satisfies; intros.
simpl in H2.
elim (in_app_or _ _ _ H2); intros; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
inversion_clear H3; auto.
rewrite <- H4.
generalize (propagate_sound _ _ _ (eq_sym H)); intro.
apply H3.
apply forall_satisfies; intros.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
Qed.

Theorem refute_correct : forall c O, refute c O = true -> unsat c.
intros c O H; replace c with (c++nil).
apply (refute_work_correct nil c O); auto.
apply app_nil_r.
Qed.
