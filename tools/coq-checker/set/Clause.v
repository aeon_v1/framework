Require Export Basic.
Require Export Even.
Require Export Bool.

Definition Valuation := nat -> bool.

Section RemoveAll.

Variable A:Type.
Variable A_dec : forall x y : A, {x = y} + {x <> y}.

Fixpoint remove_all (l w:list A) :=
  match l with
  | nil => w
  | x :: l' => remove_all l' (remove A_dec x w)
  end.

Lemma remove_all_orig : forall l w x,
  In x w -> In x l \/ In x (remove_all l w).
induction l; simpl; auto.
intros.
elim (A_dec a x); auto.
intro; elim IHl with (remove A_dec a w) x; auto.
apply in_remove; auto.
Qed.

End RemoveAll.

Arguments remove_all : default implicits.

Section Literal.

Inductive Literal : Type :=
  | pos : nat -> Literal
  | neg : nat -> Literal.

Definition negate (l:Literal) : Literal :=
  match l with
  | pos n => neg n
  | neg n => pos n
  end.

Lemma literal_eq_dec : forall l l':Literal, {l = l'} + {l <> l'}.
induction l; induction l'; intros; try (right; discriminate); 
elim eq_nat_dec with n n0; auto;
right; intro H; inversion H; auto.
Qed.

Fixpoint L_satisfies (v:Valuation) (l:Literal) : Prop :=
  match l with
  | pos x => if (v x) then True else False
  | neg x => if (v x) then False else True
  end.

Lemma L_satisfies_neg : forall v l, L_satisfies v l <-> ~ L_satisfies v (negate l).
intros; induction l; simpl; split; case (v n); auto.
Qed.

End Literal.

Section Clauses.

Definition Clause := list Literal.

Fixpoint C_satisfies (v:Valuation) (c:Clause) : Prop :=
  match c with
  | nil => False
  | l :: c' => (L_satisfies v l) \/ (C_satisfies v c')
  end.

Lemma C_satisfies_exists : forall v c, C_satisfies v c ->
  exists l, In l c /\ L_satisfies v l.
induction c; intros.
inversion H.
inversion_clear H.
exists a; split; auto.
left; auto.

elim IHc; auto.
intros; exists x.
inversion_clear H; split; auto.
right; auto.
Qed.

Lemma exists_C_satisfies : forall v c l, In l c -> L_satisfies v l ->
  C_satisfies v c.
induction c; auto.
intros.
inversion_clear H.
rewrite H1; left; auto.
right; apply IHc with l; auto.
Qed.

Definition myclause := ((1,true) :: (2,false) :: (3,true) :: nil).
Definition myvaluation (n:nat) := if (even_odd_dec n) then false else true.

(*
Check (C_satisfies myvaluation myclause).
Eval simpl in (C_satisfies myvaluation myclause).
*)

Definition C_unsat (c:Clause) : Prop := forall v:Valuation, ~(C_satisfies v c).

Lemma C_unsat_empty : C_unsat nil.
red; auto.
Qed.

Lemma C_sat_clause : forall c:Clause, c <> nil -> ~(C_unsat c).
intro c; case c; intros.
elim H; auto.
intro Hu; red in Hu.
induction l.
apply Hu with (fun _ => true); simpl; auto.
apply Hu with (fun _ => false); simpl; auto.
Qed.

Lemma clause_eq_dec : forall c c':Clause, {c = c'} + {c <> c'}.
induction c, c'; auto.
right; discriminate.
right; discriminate.
elim (literal_eq_dec a l); intro Hs;
elim (IHc c'); intro Hc;
try (rewrite Hs; rewrite Hc; auto);
right; intro; inversion H;
try apply Hc; try apply Hs; auto.
Qed.

Lemma clause_eq_nil_cons : forall c: Clause, {c = nil} + {exists l c', c = l :: c'} .
intro c; case c; auto.
clear c; intros l c; right.
exists l; exists c; auto.
Qed.

End Clauses.

Section CNF.

Definition CNF := list Clause.

Fixpoint satisfies (v:Valuation) (c:CNF) : Prop :=
  match c with
  | nil => True
  | cl :: c' => (C_satisfies v cl) /\ (satisfies v c')
  end.

Definition unsat (c:CNF) : Prop := forall v:Valuation, ~(satisfies v c).

(*
Definition c1 := ((1,true) :: (2,false) :: nil).
Definition c2 := ((1,false) :: (2,true) :: nil).
Definition c3 := ((1,false) :: nil).
Definition c4 := ((2,true) :: (3,false) :: nil).
Definition cnf1 := (c1::c2::c3::c4::nil).

Eval simpl in (satisfies myvaluation cnf1).
*)

Lemma CNF_eq_dec : forall c c':CNF, {c = c'} + {c <> c'}.
induction c, c'; auto; try (right; discriminate).
elim (clause_eq_dec a c0); intro H1;
elim (IHc c'); intro H2;
try (rewrite H1; rewrite H2; auto);
right; intro H; inversion H;
try (apply H1); try (apply H2); auto.
Qed.

Lemma satisfies_remove : forall c:CNF, forall c':Clause, forall v,
  satisfies v c -> satisfies v (remove clause_eq_dec c' c).
induction c; simpl; intros; auto.
elim clause_eq_dec; intro Hc; inversion_clear H; auto.
split; auto.
Qed.

Lemma unsat_remove : forall c:CNF, forall c':Clause,
  unsat (remove clause_eq_dec c' c) -> unsat c.
red; intros; intro.
apply H with v; apply satisfies_remove; auto.
Qed.

End CNF.

Definition list_subset (A:Type) (l1 l2:list A) := forall x, In x l1 -> In x l2.

Definition entails (c:CNF) (c':Clause) : Prop :=
  forall v:Valuation, satisfies v c -> C_satisfies v c'.

Section Unit_Propagation.

Lemma propagate_singleton : forall cs : CNF, forall c c' : Clause, forall l,
  remove_all literal_eq_dec c' c = (l :: nil) ->
  entails cs ((negate l) :: c') -> entails (c::cs) c'.
intros.
red; intros.
inversion_clear H1.
elim (C_satisfies_exists _ _ H2).
intros l' Hl; inversion_clear Hl.
elim (remove_all_orig _ literal_eq_dec c' _ _ H1); intros.
apply exists_C_satisfies with l'; auto.
rewrite H in H5; clear H; inversion_clear H5.
red in H0.
generalize (H0 v H3); clear H0.
simpl; rewrite H.
intro H0; inversion_clear H0; auto.
elim (L_satisfies_neg v l'); intros.
elim H0; auto.
inversion H.
Qed.

Lemma propagate_empty : forall cs : CNF, forall c c' : Clause,
  remove_all literal_eq_dec c' c = nil -> entails (c::cs) c'.
red; intros.
inversion_clear H0.
elim (C_satisfies_exists _ _ H1).
intros l Hl; inversion_clear Hl.
apply exists_C_satisfies with l; auto.
elim (remove_all_orig _ literal_eq_dec c' _ _ H0); auto.
rewrite H; intro Hbot; elim Hbot.
Qed.

End Unit_Propagation.

