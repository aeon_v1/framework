Require Export Checker.

Extraction Language Haskell.

Extract Inductive bool => Bool [ True False ].

Extract Inductive sumbool => Bool [ True False ].

Extract Inductive nat => Int [ "0" "succ" ]
 "(\ fO fS n -> if n==0 then (fO __) else fS (n-1))".

Extract Inlined Constant Peano_dec.eq_nat_dec => "(==)".

Extract Constant ICNF => "IntMap.IntMap Clause".

Extract Constant add_ICNF => "IntMap.insert".
Extract Constant del_ICNF => "IntMap.delete".
Extract Constant get_ICNF => "(IntMap.!)".
Extract Inlined Constant empty_ICNF => "IntMap.empty".

Extract Constant CNF => "Set.Set Clause".
Extract Inlined Constant In_dec => "(\_ -> Set.member)".

Extract Inlined Constant Set_Clause => "(Set.Set Literal)".
Extract Inlined Constant Set_empty => "Set.empty".
Extract Inlined Constant remove_all => "(\_ x y -> Set.difference y x)".

Extract Constant Set_clause_eq_nil_cons => "\x ->
  if Set.null x then Inright
                else Inleft (ExistT (Set.findMin x) (Set.deleteMin x))".

Extract Constant Clause_to_Set_Clause => "\x ->
  case x of { Nil -> Set.empty;
              Cons y s -> Set.insert y (clause_to_Set_Clause s)}".

Extract Constant insert => "Set.insert".

Extraction "Checker" refute.
