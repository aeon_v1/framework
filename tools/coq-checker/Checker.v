Require Export CNF.
Require Export Recdef.
Require Import Allmaps.

Section ICNF.

Definition ICNF := Map Clause.

Definition empty_ICNF : ICNF := M0 _.

Fixpoint ICNF_to_CNF (c:ICNF) : CNF :=
  match c with
  | M0 => nil
  | M1 _ cl => cl :: nil
  | M2 c' c'' => (ICNF_to_CNF c') ++ (ICNF_to_CNF c'')
  end.

Coercion ICNF_to_CNF : ICNF >-> CNF.

Lemma ICNF_get_in : forall c (cl:Clause) i,
  MapGet _ c i = Some cl -> In cl (ICNF_to_CNF c).
induction c; simpl; intros.
inversion H.
revert H; elim BinNat.N.eqb; simpl; intro; inversion H; auto.
revert H; induction i; auto.
intro; apply in_or_app; left; revert H; apply IHc1.
induction p.
intro; apply in_or_app; right; revert H; apply IHc2.
intro; apply in_or_app; left; revert H; apply IHc1.
intro; apply in_or_app; right; revert H; apply IHc2.
Qed.

Lemma in_ICNF_get : forall c (cl:Clause),
  In cl (ICNF_to_CNF c) -> exists i, MapGet _ c i = Some cl.
induction c; simpl; intros.
inversion H.
inversion_clear H.
exists a; rewrite H0; simpl.
elim (BinNat.N.eqb_eq a a); intros.
rewrite H1; simpl; auto.
inversion H0.
elim (in_app_or _ _ _ H); intro.
elim (IHc1 _ H0); intros j Hj.

induction j.
exists BinNums.N0; auto.
exists (BinNums.Npos (BinNums.xO p)); auto.

elim (IHc2 _ H0); intros j Hj.
induction j.
exists (BinNums.Npos BinNums.xH); auto.
exists (BinNums.Npos (BinNums.xI p)); auto.
Qed.

Definition get_ICNF (c:ICNF) i : Clause :=
  match (MapGet _ c i) with
  | None => true_C
  | Some cl => cl
  end.

(*
Fixpoint get_ICNF (c:ICNF) (i:nat) : Clause :=
  match c with
  | nil => true_C
  | c'::cs => let (j,cl) := c' in if (eq_nat_dec i j) then cl else (get_ICNF cs i)
  end.
*)

Lemma get_ICNF_in_or_default : forall c i,
  {In (get_ICNF c i) (ICNF_to_CNF c)} + {get_ICNF c i = true_C}.
induction c; intro i; auto.
unfold get_ICNF; simpl; elim BinNat.N.eqb; auto.

unfold get_ICNF; induction i.
simpl.
elim (IHc1 BinNums.N0); auto.
left; apply in_or_app; auto.

induction p; intros; simpl.
elim (IHc2 (BinNums.Npos p)); auto.
left; apply in_or_app; right; auto.

elim (IHc1 (BinNums.Npos p)); auto.
left; apply in_or_app; left; auto.

elim (IHc2 (BinNums.N0)); auto.
left; apply in_or_app; right; auto.
Qed.

Definition del_ICNF i (c:ICNF) : ICNF :=
  MapRemove _ c i.

(*
Fixpoint del_ICNF (i:nat) (c:ICNF) : ICNF :=
  match c with
  | nil => nil
  | c'::cs => let (j,cl) := c' in if (eq_nat_dec i j) then cs else c'::(del_ICNF i cs)
  end.
*)

Lemma ICNF_get_del : forall c (cl:Clause) i j,
  MapGet _ (MapRemove _ c j) i = Some cl -> MapGet _ c i = Some cl.
induction c; intros cl i j; auto.

simpl.
set (Haj := BinNat.N.eqb a j); assert (Haj = BinNat.N.eqb a j); auto; clearbody Haj.
set (Hai := BinNat.N.eqb a i); assert (Hai = BinNat.N.eqb a i); auto; clearbody Hai.
revert H H0.
elim Haj; elim Hai; simpl; auto; intros.
inversion H1.
rewrite <- H0 in H1; auto.
rewrite <- H0 in H1; auto.

simpl.
elim BinNat.N.odd; rewrite makeM2_M2;
simpl; induction i; auto.
induction p; auto.
apply IHc2.
apply IHc2.
apply IHc1.
induction p; auto.
apply IHc1.
Qed.

Lemma MapRemove_in : forall c cl j,
  In cl (ICNF_to_CNF (MapRemove _ c j)) -> In cl (ICNF_to_CNF c).
intros.
elim (in_ICNF_get _ _ H); intros i Hi.
apply ICNF_get_in with i.
apply ICNF_get_del with j; auto.
Qed.

Lemma del_ICNF_in : forall (i:ad) (c:ICNF) (cl:Clause),
  In cl ((del_ICNF i c):CNF) -> In cl (c:CNF).
induction c; auto.
intros; simpl.
revert H; simpl.
induction a; elim BinNat.N.eqb; simpl; auto.
intros.
apply MapRemove_in with i; auto.
Qed.

Definition add_ICNF i (cl:Clause) (c:ICNF) :=
  MapPut _ c i cl.

(*
Definition add_ICNF (i:nat) (cl:Clause) (c:ICNF) := ((i,cl) :: c) : ICNF.
*)

Lemma MapPut1_in : forall p i j ci cj cl,
  In cl (ICNF_to_CNF (MapPut1 _ i ci j cj p)) -> cl = ci \/ cl = cj.
induction p; simpl; auto; intros i j ci cj cl;
elim BinNat.N.odd; simpl; auto.
intro H; inversion_clear H; auto; inversion_clear H0; auto; inversion H.
intro H; inversion_clear H; auto; inversion_clear H0; auto; inversion H.
apply IHp.
rewrite app_nil_r; apply IHp.
intro H; inversion_clear H; auto; inversion_clear H0; auto; inversion H.
intro H; inversion_clear H; auto; inversion_clear H0; auto; inversion H.
Qed.

Lemma MapPut_in : forall c cl cl' j,
  In cl (ICNF_to_CNF (MapPut _ c j cl')) -> cl = cl' \/ In cl (ICNF_to_CNF c).
induction c; simpl; intros.
inversion_clear H; auto.
revert H; elim BinNat.N.lxor; simpl; intros.
inversion_clear H; auto.
elim (MapPut1_in _ _ _ _ _ _ H); auto.

revert H; induction j; auto.
simpl; intro.
elim (in_app_or _ _ _ H); intros.
elim (IHc1 _ _ _ H0); intros; auto.
right; apply in_or_app; auto.
right; apply in_or_app; auto.

case p; simpl; intros; auto.
elim (in_app_or _ _ _ H); clear H; intros.
right; apply in_or_app; auto.
elim (IHc2 _ _ _ H); intros; auto.
right; apply in_or_app; auto.
elim (in_app_or _ _ _ H); clear H; intros.
elim (IHc1 _ _ _ H); intros; auto.
right; apply in_or_app; auto.
right; apply in_or_app; auto.
elim (in_app_or _ _ _ H); clear H; intros.
right; apply in_or_app; auto.
elim (IHc2 _ _ _ H); intros; auto.
right; apply in_or_app; auto.
Qed.

Lemma add_ICNF_unsat : forall i cl, forall c:ICNF,
  unsat ((add_ICNF i cl c):ICNF) -> entails c cl -> unsat c.
intros; intro; intro.
apply H with v.
unfold add_ICNF.
apply forall_satisfies; intros.
elim (MapPut_in _ _ _ _ H2); intros.
rewrite H3; auto.
apply satisfies_forall with (ICNF_to_CNF c); auto.
Qed.

End ICNF.

Section Propagation.

Fixpoint propagate (cs: ICNF) (c: Set_Clause) (is:list ad) : bool := 
  match is with
  | nil => false
  | (i::is) => match Set_clause_eq_nil_cons (remove_all literal_eq_dec c (Clause_to_Set_Clause (get_ICNF cs i))) with
    | inright _ => true
    | inleft H => let (l,Hl) := H in let (c',_) := Hl in
                  match Set_clause_eq_nil_cons c' with
                  | inright _ => propagate cs (insert (negate l) c) is
                  | inleft _ => false
  end end end.


Lemma propagate_sound : forall is cs c, propagate cs c is = true -> entails cs c.
induction is; simpl; intros.
inversion H.
revert H.
rename a into i.
elim Set_clause_eq_nil_cons; intros.
induction a; induction p.
revert H; elim Set_clause_eq_nil_cons; intros.
inversion H.
rename x into l.
rewrite b in p; clear b x0.
elim (get_ICNF_in_or_default cs i); intro.
apply propagate_entails with (get_ICNF cs i); auto; apply propagate_singleton with l; auto.
apply propagate_true; apply propagate_singleton with l; auto.
rewrite b in p; auto.

elim (get_ICNF_in_or_default cs i); intro.
apply propagate_entails with (get_ICNF cs i); auto; apply propagate_empty; auto.
apply propagate_true; apply propagate_empty; auto.
rewrite b0 in b; auto.
Qed.

End Propagation.

Section Refutation.

Inductive Action : Type :=
  | D : list ad -> Action
  | O : ad -> Clause -> Action
  | R : ad -> Clause -> list ad -> Action.

Definition Oracle := list Action.

Fixpoint Oracle_size (O:Oracle) : nat :=
  match O with
  | nil => 0
  | (D is)::O' => length is + 1 + Oracle_size O'
  | _::O' => 1 + Oracle_size O'
  end.

Definition Answer := bool.

Function refute_work (w:ICNF) (c:CNF) (O:Oracle) {measure Oracle_size O} : Answer :=
  match O with
  | nil => false
  | (D nil)::O' => refute_work w c O'
  | (D (i::is))::O' => refute_work (del_ICNF i w) c ((D is)::O')
  | (O i cl)::O' => if (In_dec clause_eq_dec cl c) then (refute_work (add_ICNF i cl w) c O') else false
  | (R i nil is)::O' => propagate w Set_empty is
  | (R i cl is)::O' => andb (propagate w (Clause_to_Set_Clause cl) is) (refute_work (add_ICNF i cl w) c O')
  end.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
simpl; intros; auto with arith.
Defined.

Definition refute (c:CNF) (O:Oracle) : Answer :=
  refute_work empty_ICNF c O.

Lemma refute_work_correct : forall w c O, refute_work w c O = true -> unsat (app c (w : CNF)).
intros w c O; functional induction (refute_work w c O); intros; auto.
(* 1/6 *)
inversion H.
(* 2/6 *)
apply unsat_subset with (c ++ ((del_ICNF i w) : CNF)); auto.
intros.
apply in_or_app; elim (in_app_or _ _ _ H0); auto.
right.
eapply del_ICNF_in; eauto.
(* 3/6 *)
intro; intro; apply IHa with v; auto.
apply forall_satisfies; intros.
simpl in H1.
elim (in_app_or _ _ _ H1); intros; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
elim (MapPut_in _ _ _ _ H2); intros.
rewrite H3.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
(* 4/6 *)
inversion H.
(* 5/6 *)
apply unsat_subset with (w:CNF).
intros; apply in_or_app; auto.
apply CNF_empty.
apply propagate_sound with is; auto.
(* 6/6 *)
elim (andb_true_eq _ _ (eq_sym H)); clear H; intros.
intro; intro; apply IHa with v; auto.
apply forall_satisfies; intros.
simpl in H2.
elim (in_app_or _ _ _ H2); intros; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
elim (MapPut_in _ _ _ _ H3); intros.
rewrite H4.
generalize (propagate_sound _ _ _ (eq_sym H)); intro.
apply H5.
apply forall_satisfies; intros.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
apply satisfies_forall with (c ++ (w:CNF)); auto.
apply in_or_app; auto.
Qed.

Theorem refute_correct : forall c O, refute c O = true -> unsat c.
intros c O H; replace c with (c++nil).
apply (refute_work_correct empty_ICNF c O); auto.
apply app_nil_r.
Qed.

End Refutation.
