(*{-# OPTIONS_GHC -cpp -XForeignFunctionInterface -XUnliftedFFITypes -XImplicitParams -XScopedTypeVariables -XUnboxedTuples -XTypeSynonymInstances -XStandaloneDeriving -XDeriveDataTypeable -XDeriveFunctor -XDeriveFoldable -XDeriveTraversable -XDeriveGeneric -XFlexibleContexts -XFlexibleInstances -XConstrainedClassMethods -XMultiParamTypeClasses -XFunctionalDependencies -XMagicHash -XPolymorphicComponents -XExistentialQuantification -XUnicodeSyntax -XPostfixOperators -XPatternGuards -XLiberalTypeSynonyms -XRankNTypes -XTypeOperators -XExplicitNamespaces -XRecursiveDo -XParallelListComp -XEmptyDataDecls -XKindSignatures -XGeneralizedNewtypeDeriving #-}
import qualified Checker
import Prelude
import System.Environment
import System.IO
import Data.List
import qualified Data.Set as Set
import Debug.Trace
import Control.DeepSeq
import Control.Exception

--deriving instance Show Checker.Bool 
--deriving instance Show Checker.Nat
deriving instance Show Checker.Action
deriving instance Show a => Show (Checker.List a)
deriving instance (Show a, Show b) => Show (Checker.Prod a b)
*)

type action = D of int list | O of int * int list | R of int * int list * int list

let rec nat_to_positive = function
| 1 -> Checker.XH
| n -> let m = nat_to_positive (n / 2) in if n mod 2 = 0 then Checker.XO m else Checker.XI m

let nat_to_N = function
| 0 -> Checker.N0
| n -> Checker.Npos (nat_to_positive n)

let rec list_to_List = function
| [] -> Checker.Nil
| x :: l -> Checker.Cons (x,list_to_List l)

let literal_to_Literal = function
| x -> if x > 0 then Checker.Pos x else Checker.Neg (-x)

let (<<) f g x = f(g(x));;

let clause_to_Clause = list_to_List << List.map literal_to_Literal

let action_to_Action = function
| D is -> Checker.D (list_to_List (List.map nat_to_N is))
| O (i,c) -> Checker.O (nat_to_N i,clause_to_Clause c)
| R (i,c,is) -> Checker.R (nat_to_N i,clause_to_Clause c,list_to_List (List.map nat_to_N is))

let test = Checker.refute

let rec lines = function
| chan -> try
    let line = input_line chan in let rest = lines chan in
      if line = "" then rest else line :: rest
  with End_of_file ->
    close_in chan;
    []

let words = List.filter ((<>) "") << Str.split (Str.regexp " ")

exception Should_never_happen of string 

let rec drop_zero = function
| [0] -> []
| x::xs -> x::drop_zero xs
| [] -> raise (Should_never_happen "clause not terminated with 0")

let rec extract_clauses = function
| [] -> []
| line::lines -> let rest = extract_clauses lines in match (Str.first_chars line 1) with
  | "c" -> rest
  | "p" -> rest
  | _ -> line::rest

let of_list l = List.fold_right Checker.SetClause.add l Checker.SetClause.empty

let parse_cnf = of_list << List.map clause_to_Clause << List.map (List.sort compare) << List.map drop_zero << (List.filter ((<>) [])) << (List.map (List.map int_of_string << words)) << extract_clauses << lines

let read_cnf = function
| name -> let chan = open_in name in
  parse_cnf chan

let rec split_zero = function
| 0::xs -> [],drop_zero xs
| x::xs -> let ys,zs = split_zero xs in x::ys,zs
| [] -> raise (Should_never_happen "action not terminated with 0")

let list_to_action = function
| 0::xs -> D (drop_zero xs)
| id::xs -> let ys,zs = split_zero xs in let sorted_ys = List.sort compare ys in
    if zs = [] then O (id,sorted_ys) else R (id,sorted_ys,List.rev zs)
| [] -> raise (Should_never_happen "empty action")

let stream_lines chan = Stream.from (fun _ ->
  try Some (input_line chan) with End_of_file -> close_in chan; None)

let stream_map f stream = let rec next i =
  try Some (f (Stream.next stream))
  with Stream.Failure -> None in
  Stream.from next
  
let stream_filter p stream = let rec next i =
  try let value = Stream.next stream in if p value then Some value else next i
  with Stream.Failure -> None in
  Stream.from next

let stream_option stream = let rec next i =
  try let value = Stream.next stream in Some (Checker.Some value)
  with Stream.Failure -> Some Checker.None in
  Stream.from next

(*let parse_grup = list_to_List << (List.map action_to_Action) << (List.map list_to_action) << (List.filter ((<>) [])) << (List.map (List.map int_of_string << words)) << lines*)
let parse_grup = stream_option << (stream_map action_to_Action) << (stream_map list_to_action) << (stream_filter ((<>) [])) << (stream_map (List.map int_of_string << words)) << stream_lines

let read_grup = function
| name -> let chan = open_in name in
  parse_grup chan

let () =
  let cnfFile = Sys.argv.(1) in
  let cnf = read_cnf cnfFile in
  let grupFile = Sys.argv.(2) in
  let grup = read_grup grupFile in
  if test cnf grup then print_endline "True" else print_endline "False"

