Require Export Checker.

Extraction Language Ocaml.

Extract Inductive bool => bool [ "true" "false" ].

Extract Inductive sumbool => bool [ "true" "false" ].

Extract Inductive nat => int [ "0" "Pervasives.succ" ]
 "(fun fO fS n -> if n=0 then fO () else fS (n-1))".

Extract Inlined Constant Peano_dec.eq_nat_dec => "(=)".

Extract Constant CNF => "SetClause.t".
Extract Inlined Constant In_dec => "(fun _ -> SetClause.mem)".

Extract Inlined Constant Set_Clause => "SetLiteral.t".
Extract Inlined Constant Set_empty => "SetLiteral.empty".
Extract Inlined Constant remove_all => "(fun _ x y -> SetLiteral.diff y x)".

Extract Constant Set_clause_eq_nil_cons => "fun x ->
  if SetLiteral.is_empty x then Inright
                else Inleft (ExistT (SetLiteral.min_elt x,SetLiteral.remove (SetLiteral.min_elt x) x))".

Extract Constant Clause_to_Set_Clause => "fun x ->
  match x with
   | Nil -> SetLiteral.empty
   | Cons (y, s) -> SetLiteral.add y (clause_to_Set_Clause s)".

Extract Constant insert => "SetLiteral.add".

Extract Constant Oracle => "action option Stream.t"

Extract Constant get_oracle => "fun x ->
  match (Stream.next x) with
   | None -> None
   | Some a -> Some (Pair (a,x))".

Extraction "Checker" refute.
