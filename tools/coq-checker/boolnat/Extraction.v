Require Export Checker.

Extraction Language Haskell.

Extract Inductive bool => Bool [ True False ].

Extract Inductive sumbool => Bool [ True False ].

Extract Inductive nat => Int [ "0" "succ" ]
 "(\ fO fS n -> if n==0 then (fO __) else fS (n-1))".

Extract Inlined Constant Peano_dec.eq_nat_dec => "(==)".

Extraction "Checker" refute.
