Require Export Basic.
Require Export Even.
Require Export Bool.

Definition Valuation := nat -> bool.

Section RemoveAll.

Variable A:Type.
Variable A_dec : forall x y : A, {x = y} + {x <> y}.

Fixpoint remove_all (l w:list A) :=
  match l with
  | nil => w
  | x :: l' => remove_all l' (remove A_dec x w)
  end.

Lemma remove_all_orig : forall l w x,
  In x w -> In x l \/ In x (remove_all l w).
induction l; simpl; auto.
intros.
elim (A_dec a x); auto.
intro; elim IHl with (remove A_dec a w) x; auto.
apply in_remove; auto.
Qed.

(*
Lemma remove_all_length_le : forall l w, length (remove_all l w) <= length w.
induction l; simpl; auto.
intros; apply IHl.
simpl; elim A_dec; intros; auto with arith.
simpl; auto with arith.
Qed.

Lemma remove_all_length_lt : forall x l w, In x w -> length (remove_all (x::l) w) < length w.
induction l; induction w; intros.
inversion H.
simpl; elim A_dec.
intros; apply le_n_S; apply remove_all_length_le.
inversion_clear H.
intros; elim b; auto.
intros; apply lt_n_S; auto.
inversion H.
simpl; elim A_dec; intros.
apply le_n_S; apply remove_all_length_le.
apply le_n_S.
change (S (length (remove_all (x::a::l) w)) <= length w).
apply IHw.
inversion_clear H; auto.
elim b; auto.
Qed.

Lemma remove_all_in : forall l w x, In x (remove_all l w) -> In x w.
induction l; induction w; auto.
simpl; elim A_dec; intros; auto.
inversion_clear H; auto.
Qed.

Lemma remove_all_nil : forall l, remove_all l nil = nil.
induction l; auto.
Qed.

Lemma remove_all_NoDup : forall l w, NoDup w -> NoDup (remove_all l w).
induction l; induction w; auto.
intro; inversion_clear H.
simpl; elim A_dec; auto.
intros; apply NoDup_cons; auto.
intro; apply H0.
apply remove_all_in with (a::l); auto.
Qed.

Lemma remove_all_not_in : forall l w x, In x w -> ~In x (remove_all l w) -> In x l.
induction l; induction w; simpl; auto.
intros; inversion H.
intros x Hx; elim A_dec.
intros; inversion_clear Hx.
left; transitivity a0; auto.
right; apply IHl with w; auto.
simpl; intros.
inversion_clear Hx.
elim H; auto.
apply IHw; auto.
Qed.
*)

End RemoveAll.

Arguments remove_all : default implicits.

Section Clauses.

Definition Clause := list (nat*bool).

Fixpoint C_satisfies (v:Valuation) (c:Clause) : Prop :=
  match c with
  | nil => False
  | (x,true) :: c' => if (v x) then True else (C_satisfies v c')
  | (x,false) :: c' => if (v x) then (C_satisfies v c') else True
  end.

Lemma C_satisfies_exists : forall v c, C_satisfies v c ->
  exists x s, In (x,s) c /\ (v x) = s.
induction c; intros.
inversion H.
simpl in H.
revert H; destruct a; case b;
set (z := v n); assert (z = v n); auto; clearbody z;
revert H; elim z; intros.

exists n; exists true; split; auto.
left; auto.

elim IHc; auto.
intros; exists x.
elim H1; intros s Hs; exists s; inversion_clear Hs; split; auto.
right; auto.

elim IHc; auto.
intros; exists x.
elim H1; intros s Hs; exists s; inversion_clear Hs; split; auto.
right; auto.

exists n; exists false; split; auto.
left; auto.
Qed.

Lemma exists_C_satisfies : forall v c x s, In (x,s) c -> v x = s ->
  C_satisfies v c.
induction c; auto.
intros.
inversion_clear H.
rewrite H1; simpl.
rewrite H0; elim s; simpl; auto.
simpl.
elim a; intros; elim b; simpl; elim (v a0); auto.
apply IHc with x s; auto.
apply IHc with x s; auto.
Qed.

Definition myclause := ((1,true) :: (2,false) :: (3,true) :: nil).
Definition myvaluation (n:nat) := if (even_odd_dec n) then false else true.

(*
Check (C_satisfies myvaluation myclause).
Eval simpl in (C_satisfies myvaluation myclause).
*)

Definition C_unsat (c:Clause) : Prop := forall v:Valuation, ~(C_satisfies v c).

Lemma C_unsat_empty : C_unsat nil.
red; auto.
Qed.

Lemma C_sat_clause : forall c:Clause, c <> nil -> ~(C_unsat c).
intro c; case c; intros.
elim H; auto.
elim p; intros.
rename a into x; rename b into s; clear p H.
intro Hu; red in Hu.
apply Hu with (fun _ => s).
simpl.
case s; auto.
Qed.

Lemma literal_eq_dec : forall x y : nat*bool, {x = y} + {x <> y}.
destruct x, y.
rename n into x; rename n0 into y; rename b into sx; rename b0 into sy.
elim (eq_nat_dec x y); intro Hxy;
elim (bool_dec sx sy); intro Hs;
try (rewrite Hxy; rewrite Hs; auto);
right; intro; inversion H;
try apply Hs; try apply Hx; auto.
Qed.

Lemma clause_eq_dec : forall c c':Clause, {c = c'} + {c <> c'}.
induction c, c'; auto.
right; discriminate.
right; discriminate.
elim (literal_eq_dec a p); intro Hs;
elim (IHc c'); intro Hc;
try (rewrite Hs; rewrite Hc; auto);
right; intro; inversion H;
try apply Hc; try apply Hs; auto.
Qed.

End Clauses.

Section CNF.

Definition CNF := list Clause.

Fixpoint satisfies (v:Valuation) (c:CNF) : Prop :=
  match c with
  | nil => True
  | cl :: c' => (C_satisfies v cl) /\ (satisfies v c')
  end.

Definition unsat (c:CNF) : Prop := forall v:Valuation, ~(satisfies v c).

(*
Definition c1 := ((1,true) :: (2,false) :: nil).
Definition c2 := ((1,false) :: (2,true) :: nil).
Definition c3 := ((1,false) :: nil).
Definition c4 := ((2,true) :: (3,false) :: nil).
Definition cnf1 := (c1::c2::c3::c4::nil).

Eval simpl in (satisfies myvaluation cnf1).
*)

Lemma CNF_eq_dec : forall c c':CNF, {c = c'} + {c <> c'}.
induction c, c'; auto; try (right; discriminate).
elim (clause_eq_dec a c0); intro H1;
elim (IHc c'); intro H2;
try (rewrite H1; rewrite H2; auto);
right; intro H; inversion H;
try (apply H1); try (apply H2); auto.
Qed.

Lemma satisfies_remove : forall c:CNF, forall c':Clause, forall v,
  satisfies v c -> satisfies v (remove clause_eq_dec c' c).
induction c; simpl; intros; auto.
elim clause_eq_dec; intro Hc; inversion_clear H; auto.
split; auto.
Qed.

Lemma unsat_remove : forall c:CNF, forall c':Clause,
  unsat (remove clause_eq_dec c' c) -> unsat c.
red; intros; intro.
apply H with v; apply satisfies_remove; auto.
Qed.

End CNF.

Definition list_subset (A:Type) (l1 l2:list A) := forall x, In x l1 -> In x l2.

Definition entails (c:CNF) (c':Clause) : Prop :=
  forall v:Valuation, satisfies v c -> C_satisfies v c'.

Section Unit_Propagation.

Lemma propagate_singleton : forall cs : CNF, forall c c' : Clause, forall x s,
  remove_all literal_eq_dec c' c = ((x,s) :: nil) ->
  entails cs ((x,negb s) :: c') -> entails (c::cs) c'.
intros.
red; intros.
inversion_clear H1.
elim (C_satisfies_exists _ _ H2).
intros y Hy; elim Hy; clear Hy; intros sy Hy; inversion_clear Hy.
elim (remove_all_orig _ literal_eq_dec c' _ _ H1); intros.
apply exists_C_satisfies with y sy; auto.
rewrite H in H5; clear H; inversion_clear H5.
inversion H; clear H.
rewrite <- H6 in H4; rewrite <- H6 in H1; clear H6 y.
rewrite <- H7 in H1; rewrite <- H7 in H4; clear H7 sy.
red in H0.
generalize (H0 v H3); clear H0.
simpl; rewrite H4; elim s; simpl; auto.
inversion H.
Qed.

Lemma propagate_empty : forall cs : CNF, forall c c' : Clause,
  remove_all literal_eq_dec c' c = nil -> entails (c::cs) c'.
red; intros.
inversion_clear H0.
elim (C_satisfies_exists _ _ H1).
intros x Hx; elim Hx; clear Hx; intros s Hs; inversion_clear Hs.
apply exists_C_satisfies with x s; auto.
elim (remove_all_orig _ literal_eq_dec c' _ _ H0); auto.
rewrite H; intro Hbot; elim Hbot.
Qed.

End Unit_Propagation.

