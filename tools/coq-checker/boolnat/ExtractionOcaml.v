Require Export Checker.

Extraction Language Ocaml.

Extract Inductive bool => bool [ "true" "false" ].

Extract Inductive sumbool => bool [ "true" "false" ].

Extract Inductive nat => int [ "0" "succ" ]
 "(fun fO fS n -> if n=0 then fO () else fS (n-1))".

Extract Inlined Constant Peano_dec.eq_nat_dec => "(=)".

Extraction "Checker" refute.
