type bool =
| True
| False

val negb : bool -> bool

type 'a option =
| Some of 'a
| None

type ('a, 'b) sum =
| Inl of 'a
| Inr of 'b

type ('a, 'b) prod =
| Pair of 'a * 'b

val snd : ('a1, 'a2) prod -> 'a2

type 'a list =
| Nil
| Cons of 'a * 'a list

val app : 'a1 list -> 'a1 list -> 'a1 list

type comparison =
| Eq
| Lt
| Gt

type 'a sig0 =
  'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

type sumbool =
| Left
| Right

type 'a sumor =
| Inleft of 'a
| Inright

val in_dec : ('a1 -> 'a1 -> sumbool) -> 'a1 -> 'a1 list -> sumbool

val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list

type n =
| N0
| Npos of int

module Pos :
 sig
  val succ : int -> int

  val add : int -> int -> int

  val add_carry : int -> int -> int

  val mul : int -> int -> int

  val compare_cont : comparison -> int -> int -> comparison

  val eqb : int -> int -> bool

  val coq_Nsucc_double : n -> n

  val coq_Ndouble : n -> n

  val coq_lxor : int -> int -> n

  val eq_dec : int -> int -> sumbool
 end

module N :
 sig
  val succ_double : n -> n

  val double : n -> n

  val add : n -> n -> n

  val mul : n -> n -> n

  val eqb : n -> n -> bool

  val div2 : n -> n

  val even : n -> bool

  val odd : n -> bool

  val coq_lxor : n -> n -> n

  val eq_dec : n -> n -> sumbool
 end

type 't binaryTree =
| Nought
| Node of 't * 't binaryTree * 't binaryTree

val bT_add :
  ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree

val bT_split : 'a1 binaryTree -> 'a1 -> ('a1, 'a1 binaryTree) prod

val bT_in_dec : ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> sumbool

val bT_remove :
  ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree

val bT_diff :
  ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
  binaryTree

val bT_add_all :
  ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
  binaryTree

type literal =
| Pos of int
| Neg of int

val negate : literal -> literal

val literal_eq_dec : literal -> literal -> sumbool

val literal_compare : literal -> literal -> comparison

type clause = literal list

val clause_compare : clause -> clause -> comparison

type setClause = literal binaryTree

val sC_add : literal -> literal binaryTree -> literal binaryTree

val sC_diff : literal binaryTree -> literal binaryTree -> literal binaryTree

val clause_to_SetClause : clause -> setClause

val setClause_to_Clause : setClause -> clause

val true_SC : setClause

val setClause_eq_dec : setClause -> setClause -> sumbool

val setClause_eq_nil_cons :
  setClause -> (literal, (setClause, setClause) sigT) sigT sumor

type ad = n

type 'a map0 =
| M0
| M1 of ad * 'a
| M2 of 'a map0 * 'a map0

val mapGet : 'a1 map0 -> ad -> 'a1 option

val mapPut1 : ad -> 'a1 -> ad -> 'a1 -> int -> 'a1 map0

val mapPut : 'a1 map0 -> ad -> 'a1 -> 'a1 map0

val makeM2 : 'a1 map0 -> 'a1 map0 -> 'a1 map0

val mapRemove : 'a1 map0 -> ad -> 'a1 map0

type cNF = clause binaryTree

type iCNF = setClause map0

val iCNF_to_CNF : iCNF -> cNF

val get_ICNF : iCNF -> ad -> setClause

val del_ICNF : ad -> iCNF -> iCNF

val add_ICNF : ad -> setClause -> iCNF -> setClause map0

val propagate : iCNF -> setClause -> ad list -> bool

val get_list_from :
  n -> (n, (n list, (literal, n list) prod) sum) prod list -> (n list,
  (literal, n list) prod) sum

val sC_has_literal : literal -> literal binaryTree -> bool

val c_has_literal : literal -> literal list -> bool

val rAT_check_run :
  iCNF -> (n, setClause) prod list -> literal -> setClause -> (n, (n list,
  (literal, n list) prod) sum) prod list -> bool

val double_all : (n, setClause) prod list -> (n, setClause) prod list

val double_all_add_one : (n, setClause) prod list -> (n, setClause) prod list

val iCNF_to_list : iCNF -> (n, setClause) prod list

val rAT_check :
  iCNF -> literal -> clause -> (n, (n list, (literal, n list) prod) sum) prod
  list -> bool

type action =
| D of ad list
| R of ad * clause * ad list
| A of ad * literal * clause
   * (ad, (ad list, (literal, ad list) prod) sum) prod list

type 'a lazyT = 'a Lazy.t

type 'a lazy_list =
| Lnil
| Lcons of 'a * 'a lazy_list lazyT

type oracle = action lazy_list lazyT

type answer = (bool, iCNF) prod

val empty_SC : setClause

val refute_work : iCNF -> oracle -> answer

val make_ICNF : (ad, clause) prod list -> iCNF

val iCNF_eq_empty_dec : iCNF -> sumbool

val refute : (ad, clause) prod list -> oracle -> bool

val clause_equivalent_dec : clause -> clause -> sumbool

val iCNF_has_equiv : clause -> cNF -> sumbool

val iCNF_all_in_dec : clause list -> cNF -> sumbool

val entail :
  (ad, clause) prod list -> (ad, clause) prod list -> oracle -> bool
