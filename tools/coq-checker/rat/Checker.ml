type bool =
| True
| False

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type 'a option =
| Some of 'a
| None

type ('a, 'b) sum =
| Inl of 'a
| Inr of 'b

type ('a, 'b) prod =
| Pair of 'a * 'b

(** val snd : ('a1, 'a2) prod -> 'a2 **)

let snd = function
| Pair (_, y) -> y

type 'a list =
| Nil
| Cons of 'a * 'a list

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | Nil -> m
  | Cons (a, l1) -> Cons (a, (app l1 m))

type comparison =
| Eq
| Lt
| Gt

type 'a sig0 =
  'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

type sumbool =
| Left
| Right

type 'a sumor =
| Inleft of 'a
| Inright

(** val in_dec : ('a1 -> 'a1 -> sumbool) -> 'a1 -> 'a1 list -> sumbool **)

let rec in_dec h a = function
| Nil -> Right
| Cons (y, l0) ->
  let s = h y a in
  (match s with
   | Left -> Left
   | Right -> in_dec h a l0)

(** val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list **)

let rec map f = function
| Nil -> Nil
| Cons (a, t) -> Cons ((f a), (map f t))

type n =
| N0
| Npos of int

module Pos =
 struct
  (** val succ : int -> int **)

  let rec succ x =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p -> (fun x -> 2*x)
      (succ p))
      (fun p -> (fun x -> 2*x+1)
      p)
      (fun _ -> (fun x -> 2*x)
      1)
      x

  (** val add : int -> int -> int **)

  let rec add x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun _ -> (fun x -> 2*x)
        (succ p))
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun q -> (fun x -> 2*x)
        (add p q))
        (fun _ -> (fun x -> 2*x+1)
        p)
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (succ q))
        (fun q -> (fun x -> 2*x+1)
        q)
        (fun _ -> (fun x -> 2*x)
        1)
        y)
      x

  (** val add_carry : int -> int -> int **)

  and add_carry x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (add_carry p q))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun _ -> (fun x -> 2*x+1)
        (succ p))
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun _ -> (fun x -> 2*x)
        (succ p))
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (succ q))
        (fun q -> (fun x -> 2*x)
        (succ q))
        (fun _ -> (fun x -> 2*x+1)
        1)
        y)
      x

  (** val mul : int -> int -> int **)

  let rec mul x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      add y ((fun x -> 2*x) (mul p y)))
      (fun p -> (fun x -> 2*x)
      (mul p y))
      (fun _ ->
      y)
      x

  (** val compare_cont : comparison -> int -> int -> comparison **)

  let rec compare_cont r x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        compare_cont r p q)
        (fun q ->
        compare_cont Gt p q)
        (fun _ ->
        Gt)
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        compare_cont Lt p q)
        (fun q ->
        compare_cont r p q)
        (fun _ ->
        Gt)
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Lt)
        (fun _ ->
        Lt)
        (fun _ ->
        r)
        y)
      x

  (** val eqb : int -> int -> bool **)

  let rec eqb p q =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        eqb p0 q0)
        (fun _ ->
        False)
        (fun _ ->
        False)
        q)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        False)
        (fun q0 ->
        eqb p0 q0)
        (fun _ ->
        False)
        q)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        False)
        (fun _ ->
        False)
        (fun _ ->
        True)
        q)
      p

  (** val coq_Nsucc_double : n -> n **)

  let coq_Nsucc_double = function
  | N0 -> Npos 1
  | Npos p -> Npos ((fun x -> 2*x+1) p)

  (** val coq_Ndouble : n -> n **)

  let coq_Ndouble = function
  | N0 -> N0
  | Npos p -> Npos ((fun x -> 2*x) p)

  (** val coq_lxor : int -> int -> n **)

  let rec coq_lxor p q =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        coq_Ndouble (coq_lxor p0 q0))
        (fun q0 ->
        coq_Nsucc_double (coq_lxor p0 q0))
        (fun _ -> Npos ((fun x -> 2*x)
        p0))
        q)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        coq_Nsucc_double (coq_lxor p0 q0))
        (fun q0 ->
        coq_Ndouble (coq_lxor p0 q0))
        (fun _ -> Npos ((fun x -> 2*x+1)
        p0))
        q)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 -> Npos ((fun x -> 2*x)
        q0))
        (fun q0 -> Npos ((fun x -> 2*x+1)
        q0))
        (fun _ ->
        N0)
        q)
      p

  (** val eq_dec : int -> int -> sumbool **)

  let rec eq_dec p y0 =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun p1 ->
        eq_dec p0 p1)
        (fun _ ->
        Right)
        (fun _ ->
        Right)
        y0)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Right)
        (fun p1 ->
        eq_dec p0 p1)
        (fun _ ->
        Right)
        y0)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Right)
        (fun _ ->
        Right)
        (fun _ ->
        Left)
        y0)
      p
 end

module N =
 struct
  (** val succ_double : n -> n **)

  let succ_double = function
  | N0 -> Npos 1
  | Npos p -> Npos ((fun x -> 2*x+1) p)

  (** val double : n -> n **)

  let double = function
  | N0 -> N0
  | Npos p -> Npos ((fun x -> 2*x) p)

  (** val add : n -> n -> n **)

  let add n0 m =
    match n0 with
    | N0 -> m
    | Npos p ->
      (match m with
       | N0 -> n0
       | Npos q -> Npos (Pos.add p q))

  (** val mul : n -> n -> n **)

  let mul n0 m =
    match n0 with
    | N0 -> N0
    | Npos p ->
      (match m with
       | N0 -> N0
       | Npos q -> Npos (Pos.mul p q))

  (** val eqb : n -> n -> bool **)

  let rec eqb n0 m =
    match n0 with
    | N0 ->
      (match m with
       | N0 -> True
       | Npos _ -> False)
    | Npos p ->
      (match m with
       | N0 -> False
       | Npos q -> Pos.eqb p q)

  (** val div2 : n -> n **)

  let div2 = function
  | N0 -> N0
  | Npos p0 ->
    ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
       (fun p -> Npos
       p)
       (fun p -> Npos
       p)
       (fun _ ->
       N0)
       p0)

  (** val even : n -> bool **)

  let even = function
  | N0 -> True
  | Npos p ->
    ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
       (fun _ ->
       False)
       (fun _ ->
       True)
       (fun _ ->
       False)
       p)

  (** val odd : n -> bool **)

  let odd n0 =
    negb (even n0)

  (** val coq_lxor : n -> n -> n **)

  let coq_lxor n0 m =
    match n0 with
    | N0 -> m
    | Npos p ->
      (match m with
       | N0 -> n0
       | Npos q -> Pos.coq_lxor p q)

  (** val eq_dec : n -> n -> sumbool **)

  let eq_dec n0 m =
    match n0 with
    | N0 ->
      (match m with
       | N0 -> Left
       | Npos _ -> Right)
    | Npos x ->
      (match m with
       | N0 -> Right
       | Npos p0 -> Pos.eq_dec x p0)
 end

type 't binaryTree =
| Nought
| Node of 't * 't binaryTree * 't binaryTree

(** val bT_add :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree **)

let rec bT_add t_compare t tree = match tree with
| Nought -> Node (t, Nought, Nought)
| Node (t', l, r) ->
  (match t_compare t t' with
   | Eq -> tree
   | Lt -> Node (t', (bT_add t_compare t l), r)
   | Gt -> Node (t', l, (bT_add t_compare t r)))

(** val bT_split : 'a1 binaryTree -> 'a1 -> ('a1, 'a1 binaryTree) prod **)

let rec bT_split tree default =
  match tree with
  | Nought -> Pair (default, Nought)
  | Node (t, l, r) ->
    (match l with
     | Nought -> Pair (t, r)
     | Node (_, _, _) ->
       let Pair (t', l') = bT_split l default in Pair (t', (Node (t, l', r))))

(** val bT_in_dec :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> sumbool **)

let rec bT_in_dec t_compare t = function
| Nought -> Right
| Node (t0, b, b0) ->
  let z = t_compare t t0 in
  (match z with
   | Eq -> Left
   | Lt -> bT_in_dec t_compare t b
   | Gt -> bT_in_dec t_compare t b0)

(** val bT_remove :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree **)

let rec bT_remove t_compare t = function
| Nought -> Nought
| Node (t', l, r) ->
  (match t_compare t t' with
   | Eq ->
     (match r with
      | Nought -> l
      | Node (_, _, _) ->
        let Pair (min, r') = bT_split r t in Node (min, l, r'))
   | Lt -> Node (t', (bT_remove t_compare t l), r)
   | Gt -> Node (t', l, (bT_remove t_compare t r)))

(** val bT_diff :
    ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
    binaryTree **)

let rec bT_diff t_compare tree = function
| Nought -> tree
| Node (t', l, r) ->
  bT_remove t_compare t' (bT_diff t_compare (bT_diff t_compare tree l) r)

(** val bT_add_all :
    ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
    binaryTree **)

let rec bT_add_all t_compare tree tree' =
  match tree with
  | Nought -> tree'
  | Node (t, l, r) ->
    bT_add_all t_compare r
      (bT_add_all t_compare l (bT_add t_compare t tree'))

type literal =
| Pos of int
| Neg of int

(** val negate : literal -> literal **)

let negate = function
| Pos n0 -> Neg n0
| Neg n0 -> Pos n0

(** val literal_eq_dec : literal -> literal -> sumbool **)

let literal_eq_dec l l' =
  match l with
  | Pos x ->
    (match l' with
     | Pos x0 -> Pos.eq_dec x x0
     | Neg _ -> Right)
  | Neg x ->
    (match l' with
     | Pos _ -> Right
     | Neg x0 -> Pos.eq_dec x x0)

(** val literal_compare : literal -> literal -> comparison **)

let literal_compare l l' =
  match l with
  | Pos n0 ->
    (match l' with
     | Pos n' ->
       (fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) n0 n'
     | Neg _ -> Gt)
  | Neg n0 ->
    (match l' with
     | Pos _ -> Lt
     | Neg n' ->
       (fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) n0 n')

type clause = literal list

(** val clause_compare : clause -> clause -> comparison **)

let rec clause_compare cl cl' =
  match cl with
  | Nil ->
    (match cl' with
     | Nil -> Eq
     | Cons (_, _) -> Lt)
  | Cons (l, c) ->
    (match cl' with
     | Nil -> Gt
     | Cons (l', c') ->
       (match literal_compare l l' with
        | Eq -> clause_compare c c'
        | x -> x))

type setClause = literal binaryTree

(** val sC_add : literal -> literal binaryTree -> literal binaryTree **)

let sC_add =
  bT_add literal_compare

(** val sC_diff :
    literal binaryTree -> literal binaryTree -> literal binaryTree **)

let sC_diff =
  bT_diff literal_compare

(** val clause_to_SetClause : clause -> setClause **)

let rec clause_to_SetClause = function
| Nil -> Nought
| Cons (l, cl') -> sC_add l (clause_to_SetClause cl')

(** val setClause_to_Clause : setClause -> clause **)

let rec setClause_to_Clause = function
| Nought -> Nil
| Node (l, cl', cl'') ->
  Cons (l, (app (setClause_to_Clause cl') (setClause_to_Clause cl'')))

(** val true_SC : setClause **)

let true_SC =
  Node ((Pos 1), (Node ((Neg 1), Nought, Nought)), Nought)

(** val setClause_eq_dec : setClause -> setClause -> sumbool **)

let rec setClause_eq_dec b c' =
  match b with
  | Nought ->
    (match c' with
     | Nought -> Left
     | Node (_, _, _) -> Right)
  | Node (t, b0, b1) ->
    (match c' with
     | Nought -> Right
     | Node (l, c'1, c'2) ->
       (match literal_eq_dec t l with
        | Left ->
          (match setClause_eq_dec b0 c'1 with
           | Left -> setClause_eq_dec b1 c'2
           | Right -> Right)
        | Right -> Right))

(** val setClause_eq_nil_cons :
    setClause -> (literal, (setClause, setClause) sigT) sigT sumor **)

let setClause_eq_nil_cons = function
| Nought -> Inright
| Node (l, c0, b) -> Inleft (ExistT (l, (ExistT (c0, b))))

type ad = n

type 'a map0 =
| M0
| M1 of ad * 'a
| M2 of 'a map0 * 'a map0

(** val mapGet : 'a1 map0 -> ad -> 'a1 option **)

let rec mapGet m a =
  match m with
  | M0 -> None
  | M1 (x, y) ->
    (match N.eqb x a with
     | True -> Some y
     | False -> None)
  | M2 (m1, m2) ->
    (match a with
     | N0 -> mapGet m1 N0
     | Npos p0 ->
       ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
          (fun p ->
          mapGet m2 (Npos p))
          (fun p ->
          mapGet m1 (Npos p))
          (fun _ ->
          mapGet m2 N0)
          p0))

(** val mapPut1 : ad -> 'a1 -> ad -> 'a1 -> int -> 'a1 map0 **)

let rec mapPut1 a y a' y' p =
  (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
    (fun _ ->
    match N.odd a with
    | True -> M2 ((M1 ((N.div2 a'), y')), (M1 ((N.div2 a), y)))
    | False -> M2 ((M1 ((N.div2 a), y)), (M1 ((N.div2 a'), y'))))
    (fun p' ->
    let m = mapPut1 (N.div2 a) y (N.div2 a') y' p' in
    (match N.odd a with
     | True -> M2 (M0, m)
     | False -> M2 (m, M0)))
    (fun _ ->
    match N.odd a with
    | True -> M2 ((M1 ((N.div2 a'), y')), (M1 ((N.div2 a), y)))
    | False -> M2 ((M1 ((N.div2 a), y)), (M1 ((N.div2 a'), y'))))
    p

(** val mapPut : 'a1 map0 -> ad -> 'a1 -> 'a1 map0 **)

let rec mapPut m x x0 =
  match m with
  | M0 -> M1 (x, x0)
  | M1 (a, y) ->
    (match N.coq_lxor a x with
     | N0 -> M1 (x, x0)
     | Npos p -> mapPut1 a y x x0 p)
  | M2 (m1, m2) ->
    (match x with
     | N0 -> M2 ((mapPut m1 N0 x0), m2)
     | Npos p0 ->
       ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
          (fun p -> M2 (m1,
          (mapPut m2 (Npos p) x0)))
          (fun p -> M2 ((mapPut m1 (Npos p) x0),
          m2))
          (fun _ -> M2 (m1,
          (mapPut m2 N0 x0)))
          p0))

(** val makeM2 : 'a1 map0 -> 'a1 map0 -> 'a1 map0 **)

let makeM2 m m' =
  match m with
  | M0 ->
    (match m' with
     | M0 -> M0
     | M1 (a, y) -> M1 ((N.succ_double a), y)
     | M2 (_, _) -> M2 (m, m'))
  | M1 (a, y) ->
    (match m' with
     | M0 -> M1 ((N.double a), y)
     | _ -> M2 (m, m'))
  | M2 (_, _) -> M2 (m, m')

(** val mapRemove : 'a1 map0 -> ad -> 'a1 map0 **)

let rec mapRemove m x =
  match m with
  | M0 -> M0
  | M1 (a, _) ->
    (match N.eqb a x with
     | True -> M0
     | False -> m)
  | M2 (m1, m2) ->
    (match N.odd x with
     | True -> makeM2 m1 (mapRemove m2 (N.div2 x))
     | False -> makeM2 (mapRemove m1 (N.div2 x)) m2)

type cNF = clause binaryTree

type iCNF = setClause map0

(** val iCNF_to_CNF : iCNF -> cNF **)

let rec iCNF_to_CNF = function
| M0 -> Nought
| M1 (_, cl) -> Node ((setClause_to_Clause cl), Nought, Nought)
| M2 (c', c'') ->
  bT_add_all clause_compare (iCNF_to_CNF c') (iCNF_to_CNF c'')

(** val get_ICNF : iCNF -> ad -> setClause **)

let get_ICNF c i =
  match mapGet c i with
  | Some cl -> cl
  | None -> true_SC

(** val del_ICNF : ad -> iCNF -> iCNF **)

let del_ICNF i c =
  mapRemove c i

(** val add_ICNF : ad -> setClause -> iCNF -> setClause map0 **)

let add_ICNF i cl c =
  mapPut c i cl

(** val propagate : iCNF -> setClause -> ad list -> bool **)

let rec propagate cs c = function
| Nil -> False
| Cons (i, is0) ->
  (match setClause_eq_nil_cons (sC_diff (get_ICNF cs i) c) with
   | Inleft h ->
     let ExistT (l, hl) = h in
     let ExistT (c', hc) = hl in
     (match setClause_eq_nil_cons c' with
      | Inleft _ -> False
      | Inright ->
        (match setClause_eq_nil_cons hc with
         | Inleft _ -> False
         | Inright -> propagate cs (sC_add (negate l) c) is0))
   | Inright -> True)

(** val get_list_from :
    n -> (n, (n list, (literal, n list) prod) sum) prod list -> (n list,
    (literal, n list) prod) sum **)

let rec get_list_from i = function
| Nil -> Inl Nil
| Cons (p, l') ->
  let Pair (j, lj) = p in
  (match N.eq_dec i j with
   | Left -> lj
   | Right -> get_list_from i l')

(** val sC_has_literal : literal -> literal binaryTree -> bool **)

let sC_has_literal l cl =
  match bT_in_dec literal_compare l cl with
  | Left -> True
  | Right -> False

(** val c_has_literal : literal -> literal list -> bool **)

let c_has_literal l cl =
  match in_dec literal_eq_dec l cl with
  | Left -> True
  | Right -> False

(** val rAT_check_run :
    iCNF -> (n, setClause) prod list -> literal -> setClause -> (n, (n list,
    (literal, n list) prod) sum) prod list -> bool **)

let rec rAT_check_run c c' pivot cl l =
  match c' with
  | Nil -> True
  | Cons (p, newC) ->
    let Pair (i, s) = p in
    (match bT_in_dec literal_compare (negate pivot) s with
     | Left ->
       let lIST = get_list_from i l in
       (match lIST with
        | Inl is ->
          (match propagate c
                   (bT_add_all literal_compare
                     (bT_remove literal_compare (negate pivot) s)
                     (bT_add literal_compare pivot cl)) is with
           | True -> rAT_check_run c newC pivot cl l
           | False -> False)
        | Inr p0 ->
          let Pair (lit, is) = p0 in
          (match literal_eq_dec pivot lit with
           | Left -> False
           | Right ->
             (match match sC_has_literal (negate lit) s with
                    | True ->
                      (match c_has_literal lit (setClause_to_Clause cl) with
                       | True -> True
                       | False ->
                         propagate c
                           (bT_add literal_compare (negate lit)
                             (bT_add literal_compare pivot cl)) is)
                    | False -> False with
              | True -> rAT_check_run c newC pivot cl l
              | False -> False)))
     | Right -> rAT_check_run c newC pivot cl l)

(** val double_all : (n, setClause) prod list -> (n, setClause) prod list **)

let rec double_all = function
| Nil -> Nil
| Cons (p, l') ->
  let Pair (i, x) = p in
  Cons ((Pair ((N.mul (Npos ((fun x -> 2*x) 1)) i), x)), (double_all l'))

(** val double_all_add_one :
    (n, setClause) prod list -> (n, setClause) prod list **)

let rec double_all_add_one = function
| Nil -> Nil
| Cons (p, l') ->
  let Pair (i, x) = p in
  Cons ((Pair ((N.add (N.mul (Npos ((fun x -> 2*x) 1)) i) (Npos 1)), x)),
  (double_all_add_one l'))

(** val iCNF_to_list : iCNF -> (n, setClause) prod list **)

let rec iCNF_to_list = function
| M0 -> Nil
| M1 (i, cl) -> Cons ((Pair (i, cl)), Nil)
| M2 (c', c'') ->
  app (double_all (iCNF_to_list c')) (double_all_add_one (iCNF_to_list c''))

(** val rAT_check :
    iCNF -> literal -> clause -> (n, (n list, (literal, n list) prod) sum)
    prod list -> bool **)

let rAT_check c pivot cl l =
  rAT_check_run c (iCNF_to_list c) pivot (clause_to_SetClause cl) l

type action =
| D of ad list
| R of ad * clause * ad list
| A of ad * literal * clause
   * (ad, (ad list, (literal, ad list) prod) sum) prod list

type 'a lazyT = 'a Lazy.t

type 'a lazy_list =
| Lnil
| Lcons of 'a * 'a lazy_list lazyT

type oracle = action lazy_list lazyT

type answer = (bool, iCNF) prod

(** val empty_SC : setClause **)

let empty_SC =
  clause_to_SetClause Nil

(** val refute_work : iCNF -> oracle -> answer **)

let rec refute_work w o =
  match Lazy.force o with
  | Lnil -> Pair (True, w)
  | Lcons (a, o') ->
    (match a with
     | D l ->
       (match l with
        | Nil -> refute_work w o'
        | Cons (i, is) ->
          refute_work (del_ICNF i w) (Lazy.from_val (Lcons ((D is), o'))))
     | R (i, cl, is) ->
       (match cl with
        | Nil -> Pair ((propagate w Nought is), (M1 (N0, empty_SC)))
        | Cons (l, l0) ->
          let Pair (b, f) =
            refute_work (add_ICNF i (clause_to_SetClause (Cons (l, l0))) w)
              o'
          in
          Pair
          ((match propagate w (clause_to_SetClause (Cons (l, l0))) is with
            | True -> b
            | False -> False), f))
     | A (i, p, cl, l) ->
       let Pair (b, f) =
         refute_work (add_ICNF i (clause_to_SetClause (Cons (p, cl))) w) o'
       in
       Pair
       ((match rAT_check w p cl l with
         | True -> b
         | False -> False), f))

(** val make_ICNF : (ad, clause) prod list -> iCNF **)

let rec make_ICNF = function
| Nil -> M0
| Cons (p, l') ->
  let Pair (i, cl) = p in add_ICNF i (clause_to_SetClause cl) (make_ICNF l')

(** val iCNF_eq_empty_dec : iCNF -> sumbool **)

let iCNF_eq_empty_dec = function
| M1 (a, a0) ->
  (match N.eq_dec a N0 with
   | Left -> setClause_eq_dec a0 Nought
   | Right -> Right)
| _ -> Right

(** val refute : (ad, clause) prod list -> oracle -> bool **)

let refute c o =
  let Pair (b, f) = refute_work (make_ICNF c) o in
  (match b with
   | True ->
     (match iCNF_eq_empty_dec f with
      | Left -> True
      | Right -> False)
   | False -> False)

(** val clause_equivalent_dec : clause -> clause -> sumbool **)

let clause_equivalent_dec c c' =
  let c0 = clause_to_SetClause c in
  let c'0 = clause_to_SetClause c' in
  let cC' = bT_diff literal_compare c0 c'0 in
  let c'C = bT_diff literal_compare c'0 c0 in
  (match cC' with
   | Nought ->
     (match c'C with
      | Nought -> Left
      | Node (_, _, _) -> Right)
   | Node (_, _, _) -> Right)

(** val iCNF_has_equiv : clause -> cNF -> sumbool **)

let rec iCNF_has_equiv cl = function
| Nought -> Right
| Node (t, b, b0) ->
  (match clause_equivalent_dec cl t with
   | Left -> Left
   | Right ->
     (match iCNF_has_equiv cl b with
      | Left -> Left
      | Right -> iCNF_has_equiv cl b0))

(** val iCNF_all_in_dec : clause list -> cNF -> sumbool **)

let rec iCNF_all_in_dec c c' =
  match c with
  | Nil -> Left
  | Cons (y, l) ->
    (match iCNF_has_equiv y c' with
     | Left -> iCNF_all_in_dec l c'
     | Right -> Right)

(** val entail :
    (ad, clause) prod list -> (ad, clause) prod list -> oracle -> bool **)

let entail c c' o =
  let Pair (b, f) = refute_work (make_ICNF c) o in
  (match b with
   | True ->
     (match iCNF_all_in_dec (map snd c') (iCNF_to_CNF f) with
      | Left -> True
      | Right -> False)
   | False -> False)
