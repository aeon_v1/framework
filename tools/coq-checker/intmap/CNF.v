Require Export Clause.

Section CNF.

Definition CNF := list Clause.

Lemma CNF_eq_dec : forall c c':CNF, {c = c'} + {c <> c'}.
induction c, c'; auto; try (right; discriminate).
elim (clause_eq_dec a c0); intro H1;
elim (IHc c'); intro H2;
try (rewrite H1; rewrite H2; auto);
right; intro H; inversion H;
try (apply H1); try (apply H2); auto.
Qed.

Fixpoint satisfies (v:Valuation) (c:CNF) : Prop :=
  match c with
  | nil => True
  | cl :: c' => (C_satisfies v cl) /\ (satisfies v c')
  end.

Lemma satisfies_forall : forall v c, satisfies v c ->
  forall c', In c' c -> C_satisfies v c'.
induction c; intros; inversion_clear H0.
inversion_clear H; rewrite <- H1; auto.

apply IHc; auto.
inversion_clear H; auto.
Qed.

Lemma forall_satisfies : forall v c, (forall c', In c' c -> C_satisfies v c') ->
  satisfies v c.
induction c; simpl; auto.
Qed.

Lemma satisfies_remove : forall c:CNF, forall c':Clause, forall v,
  satisfies v c -> satisfies v (remove clause_eq_dec c' c).
induction c; simpl; intros; auto.
elim clause_eq_dec; intro Hc; inversion_clear H; auto.
split; auto.
Qed.

Definition entails (c:CNF) (c':Clause) : Prop :=
  forall v:Valuation, satisfies v c -> C_satisfies v c'.

Definition unsat (c:CNF) : Prop := forall v:Valuation, ~(satisfies v c).

Lemma unsat_remove : forall c:CNF, forall c':Clause,
  unsat (remove clause_eq_dec c' c) -> unsat c.
red; intros; intro.
apply H with v; apply satisfies_remove; auto.
Qed.

Lemma unsat_subset : forall (c c':CNF),
  (forall cl, In cl c -> In cl c') -> unsat c -> unsat c'.
intros; intro; intro.
apply H0 with v.
apply forall_satisfies; intros; apply satisfies_forall with c'; auto.
Qed.

Lemma CNF_empty : forall c, entails c nil -> unsat c.
red; intros; intro.
apply C_unsat_empty with v.
apply H; auto.
Qed.

(*
Definition c1 := ((1,true) :: (2,false) :: nil).
Definition c2 := ((1,false) :: (2,true) :: nil).
Definition c3 := ((1,false) :: nil).
Definition c4 := ((2,true) :: (3,false) :: nil).
Definition cnf1 := (c1::c2::c3::c4::nil).

Eval simpl in (satisfies myvaluation cnf1).
*)

End CNF.

Section Unit_Propagation.

Lemma propagate_singleton : forall (cs:CNF) (c c':Clause), forall l,
  remove_all literal_eq_dec c' c = (l :: nil) ->
  entails cs ((negate l) :: c') -> entails (c::cs) c'.
intros.
red; intros.
inversion_clear H1.
elim (C_satisfies_exists _ _ H2).
intros l' Hl; inversion_clear Hl.
elim (remove_all_orig _ literal_eq_dec c' _ _ H1); intros.
apply exists_C_satisfies with l'; auto.
rewrite H in H5; clear H; inversion_clear H5.
red in H0.
generalize (H0 v H3); clear H0.
simpl; rewrite H.
intro H0; inversion_clear H0; auto.
elim (L_satisfies_neg v l'); intros.
elim H0; auto.
inversion H.
Qed.

Lemma propagate_empty : forall (cs:CNF) (c c':Clause),
  remove_all literal_eq_dec c' c = nil -> entails (c::cs) c'.
red; intros.
inversion_clear H0.
elim (C_satisfies_exists _ _ H1).
intros l Hl; inversion_clear Hl.
apply exists_C_satisfies with l; auto.
elim (remove_all_orig _ literal_eq_dec c' _ _ H0); auto.
rewrite H; intro Hbot; elim Hbot.
Qed.

Lemma propagate_entails : forall (cs:CNF) (c c':Clause), In c cs ->
  entails (c::cs) c' -> entails cs c'.
intros.
red; intros.
apply H0; auto.
split; auto.
apply satisfies_forall with cs; auto.
Qed.

Lemma propagate_true : forall (cs:CNF) (c:Clause),
  entails (true_C::cs) c -> entails cs c.
intros.
red; intros.
apply H; split; auto.
apply true_C_true.
Qed.

End Unit_Propagation.