Require Export Basic.
Require Export Even.
Require Export Bool.

Definition Valuation := nat -> bool.

Section Literal.

Inductive Literal : Type :=
  | pos : nat -> Literal
  | neg : nat -> Literal.

Definition negate (l:Literal) : Literal :=
  match l with
  | pos n => neg n
  | neg n => pos n
  end.

Lemma literal_eq_dec : forall l l':Literal, {l = l'} + {l <> l'}.
induction l; induction l'; intros; try (right; discriminate); 
elim eq_nat_dec with n n0; auto;
right; intro H; inversion H; auto.
Qed.

Fixpoint L_satisfies (v:Valuation) (l:Literal) : Prop :=
  match l with
  | pos x => if (v x) then True else False
  | neg x => if (v x) then False else True
  end.

Lemma L_satisfies_neg : forall v l, L_satisfies v l <-> ~ L_satisfies v (negate l).
intros; induction l; simpl; split; case (v n); auto.
Qed.

Section Compare.

Definition Literal_compare (l l':Literal) : comparison :=
  match l, l' with 
  | pos n, pos n' => nat_compare n n'
  | neg n, neg n' => nat_compare n n'
  | neg n, pos n' => Lt
  | pos n, neg n' => Gt
  end.

Lemma eq_Lit_compare : forall l l', Literal_compare l l' = Eq -> l = l'.
induction l; induction l'; intros; inversion H; auto.
rewrite nat_compare_eq with n n0; auto.
rewrite nat_compare_eq with n n0; auto.
Qed.

Lemma Lit_compare_eq : forall l, Literal_compare l l = Eq.
induction l; apply nat_compare_eq_rev.
Qed.

Lemma Lit_compare_trans : forall l l' l'', Literal_compare l l' = Lt -> Literal_compare l' l'' = Lt ->
                                           Literal_compare l l'' = Lt.
induction l; induction l'; induction l''; simpl; auto; intros.
apply nat_compare_trans with n0; auto.
inversion H.
inversion H0.
apply nat_compare_trans with n0; auto.
Qed.

Lemma Lit_compare_sym_Gt : forall l l', Literal_compare l l' = Gt -> Literal_compare l' l = Lt.
induction l; induction l'; simpl; auto; apply nat_compare_sym_gt.
Qed.

End Compare.

End Literal.

Section Clauses.

Definition Clause := list Literal.

Fixpoint C_satisfies (v:Valuation) (c:Clause) : Prop :=
  match c with
  | nil => False
  | l :: c' => (L_satisfies v l) \/ (C_satisfies v c')
  end.

Lemma C_satisfies_exists : forall v c, C_satisfies v c ->
  exists l, In l c /\ L_satisfies v l.
induction c; intros.
inversion H.
inversion_clear H.
exists a; split; auto.
left; auto.

elim IHc; auto.
intros; exists x.
inversion_clear H; split; auto.
right; auto.
Qed.

Lemma exists_C_satisfies : forall v c l, In l c -> L_satisfies v l ->
  C_satisfies v c.
induction c; auto.
intros.
inversion_clear H.
rewrite H1; left; auto.
right; apply IHc with l; auto.
Qed.

Definition myclause := ((1,true) :: (2,false) :: (3,true) :: nil).
Definition myvaluation (n:nat) := if (even_odd_dec n) then false else true.

(*
Check (C_satisfies myvaluation myclause).
Eval simpl in (C_satisfies myvaluation myclause).
*)

Definition C_unsat (c:Clause) : Prop := forall v:Valuation, ~(C_satisfies v c).

Lemma C_unsat_empty : C_unsat nil.
red; auto.
Qed.

Definition true_C : Clause := (pos 1::neg 1::nil).

Lemma true_C_true : forall v:Valuation, C_satisfies v true_C.
simpl.
intro; elim (v 1); auto.
Qed.

Lemma C_sat_clause : forall c:Clause, c <> nil -> ~(C_unsat c).
intro c; case c; intros.
elim H; auto.
intro Hu; red in Hu.
induction l.
apply Hu with (fun _ => true); simpl; auto.
apply Hu with (fun _ => false); simpl; auto.
Qed.

Lemma clause_eq_dec : forall c c':Clause, {c = c'} + {c <> c'}.
induction c, c'; auto.
right; discriminate.
right; discriminate.
elim (literal_eq_dec a l); intro Hs;
elim (IHc c'); intro Hc;
try (rewrite Hs; rewrite Hc; auto);
right; intro; inversion H;
try apply Hc; try apply Hs; auto.
Qed.

Lemma clause_eq_nil_cons : forall c: Clause, {c = nil} + {exists l c', c = l :: c'} .
intro c; case c; auto.
clear c; intros l c; right.
exists l; exists c; auto.
Qed.

Section Compare.

Fixpoint Clause_compare (cl cl':Clause) : comparison :=
  match cl, cl' with 
  | nil, nil => Eq
  | nil, _::_ => Lt
  | _::_, nil => Gt
  | l::c, l'::c' => match (Literal_compare l l') with
                    | Lt => Lt
                    | Gt => Gt
                    | Eq => Clause_compare c c'
  end end.

Lemma eq_Clause_compare : forall cl cl', Clause_compare cl cl' = Eq -> cl = cl'.
induction cl; induction cl'; intros; try (inversion H; auto; fail).
simpl in H.
revert H; set (w := Literal_compare a a0); assert (w = Literal_compare a a0); auto; clearbody w.
revert H; elim w; intros.
rewrite eq_Lit_compare with a a0; auto; rewrite IHcl with cl'; auto.
inversion H0.
inversion H0.
Qed.

Lemma Clause_compare_eq : forall cl, Clause_compare cl cl = Eq.
induction cl; simpl; auto; rewrite Lit_compare_eq; auto.
Qed.

Lemma Clause_compare_trans : forall cl cl' cl'', Clause_compare cl cl' = Lt -> Clause_compare cl' cl'' = Lt ->
                                           Clause_compare cl cl'' = Lt.
induction cl; induction cl'; induction cl''; simpl; auto; intros.
inversion H0.
inversion H.
revert H H0.
set (w0 := Literal_compare a a0); assert (w0 = Literal_compare a a0); auto; clearbody w0; revert H.
set (w1 := Literal_compare a a1); assert (w1 = Literal_compare a a1); auto; clearbody w1; revert H.
set (w2 := Literal_compare a0 a1); assert (w2 = Literal_compare a0 a1); auto; clearbody w2; revert H.
elim w0; elim w1; elim w2; auto; intros; try (inversion H2; fail); try (inversion H3; fail);
symmetry in H; symmetry in H0; symmetry in H1.
(* 1/8 *)
apply IHcl with cl'; auto.
(* 2/8 *)
replace a0 with a1 in H.
rewrite Lit_compare_eq in H; inversion H.
transitivity a; [symmetry | idtac]; apply eq_Lit_compare; auto.
(* 3/8 *)
replace a with a1 in H0.
rewrite Lit_compare_eq in H0; inversion H0.
transitivity a0; symmetry; apply eq_Lit_compare; auto.
(* 4/8 *)
rewrite (eq_Lit_compare _ _ H1) in H0.
rewrite <- H; auto.
(* 5/8 *)
rewrite (eq_Lit_compare _ _ H) in H1; rewrite (eq_Lit_compare _ _ H0) in H1.
rewrite Lit_compare_eq in H1; inversion H1.
(* 6/8 *)
rewrite (Lit_compare_trans a a0 a1) in H0; auto.
inversion H0.
(* 7/8 *)
rewrite (eq_Lit_compare _ _ H) in H1; rewrite H1 in H0; inversion H0.
(* 8/8 *)
rewrite (Lit_compare_trans a a0 a1) in H0; auto.
Qed.

Lemma Clause_compare_sym_Gt : forall cl cl', Clause_compare cl cl' = Gt -> Clause_compare cl' cl = Lt.
induction cl; induction cl'; simpl; intros; auto.
inversion H.
revert H; clear IHcl'.
set (w := Literal_compare a a0); assert (w = Literal_compare a a0); auto; clearbody w; revert H.
case w; intros.
rewrite (eq_Lit_compare _ _ (eq_sym H)); rewrite Lit_compare_eq; auto.
inversion H0.
rewrite (Lit_compare_sym_Gt _ _ (eq_sym H)); auto.
Qed.

End Compare.

End Clauses.

Section Set_Clause.

(* to allow extraction as a set *)

Definition Set_Clause := Clause.

Definition Clause_to_Set_Clause : Clause -> Set_Clause := id (A := Clause).

Definition Set_empty : Set_Clause := (nil (A := Literal)).

Lemma Set_clause_eq_nil_cons : forall c: Set_Clause,
  {l : Literal & {c' : Set_Clause | c = l :: c'}} + {c = Set_empty}.
intro c; case c; auto.
clear c; intros l c; left.
exists l; exists c; auto.
Qed.

Definition insert : Literal -> Set_Clause -> Set_Clause := (cons (A := Literal)).

End Set_Clause.
