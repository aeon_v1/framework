type bool =
| True
| False

val negb : bool -> bool

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

type 'a list =
| Nil
| Cons of 'a * 'a list

type comparison =
| Eq
| Lt
| Gt

val compOpp : comparison -> comparison

type 'a sig0 =
  'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

type sumbool =
| Left
| Right

type 'a sumor =
| Inleft of 'a
| Inright

type n =
| N0
| Npos of int

type z =
| Z0
| Zpos of int
| Zneg of int

module Pos :
 sig
  val succ : int -> int

  val add : int -> int -> int

  val add_carry : int -> int -> int

  val pred_double : int -> int

  val compare_cont : comparison -> int -> int -> comparison

  val eqb : int -> int -> bool

  val coq_Nsucc_double : n -> n

  val coq_Ndouble : n -> n

  val coq_lxor : int -> int -> n

  val eq_dec : int -> int -> sumbool
 end

module N :
 sig
  val succ_double : n -> n

  val double : n -> n

  val eqb : n -> n -> bool

  val div2 : n -> n

  val even : n -> bool

  val odd : n -> bool

  val coq_lxor : n -> n -> n
 end

module Z :
 sig
  val double : z -> z

  val succ_double : z -> z

  val pred_double : z -> z

  val pos_sub : int -> int -> z

  val add : z -> z -> z

  val opp : z -> z

  val sub : z -> z -> z

  val compare : z -> z -> comparison

  val max : z -> z -> z

  val eq_dec : z -> z -> sumbool
 end

val compare_Lt_dec : comparison -> sumbool

val compare_Gt_dec : comparison -> sumbool

type ad = n

type 'a map =
| M0
| M1 of ad * 'a
| M2 of 'a map * 'a map

val mapGet : 'a1 map -> ad -> 'a1 option

val mapPut1 : ad -> 'a1 -> ad -> 'a1 -> int -> 'a1 map

val mapPut : 'a1 map -> ad -> 'a1 -> 'a1 map

val makeM2 : 'a1 map -> 'a1 map -> 'a1 map

val mapRemove : 'a1 map -> ad -> 'a1 map

type 't binaryTree =
| Nought
| Node of 't * 't binaryTree * 't binaryTree

val height : 'a1 binaryTree -> z

val bT_rot_L : 'a1 binaryTree -> 'a1 binaryTree

val bT_rot_R : 'a1 binaryTree -> 'a1 binaryTree

val bT_rot_LL : 'a1 binaryTree -> 'a1 binaryTree

val bT_rot_RR : 'a1 binaryTree -> 'a1 binaryTree

val bT_balance : 'a1 binaryTree -> 'a1 binaryTree

val bT_add :
  ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree

val bT_split : 'a1 binaryTree -> 'a1 -> ('a1, 'a1 binaryTree) prod

val bT_in_dec : ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> sumbool

val bT_remove :
  ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree

val bT_diff :
  ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
  binaryTree

type literal =
| Pos of int
| Neg of int

val negate : literal -> literal

val literal_compare : literal -> literal -> comparison

type clause = literal list

val clause_compare : clause -> clause -> comparison

type setClause = literal binaryTree

val sC_add : literal -> literal binaryTree -> literal binaryTree

val sC_diff : literal binaryTree -> literal binaryTree -> literal binaryTree

val clause_to_SetClause : clause -> setClause

val true_SC : setClause

val setClause_eq_nil_cons :
  setClause -> (literal, (setClause, setClause) sigT) sigT sumor

type cNF = clause binaryTree

val cNF_add : clause -> clause binaryTree -> clause binaryTree

type iCNF = setClause map

val empty_ICNF : iCNF

val get_ICNF : iCNF -> ad -> setClause

val del_ICNF : ad -> iCNF -> iCNF

val add_ICNF : ad -> setClause -> iCNF -> setClause map

val propagate : iCNF -> setClause -> ad list -> bool

type action =
| D of ad list
| O of ad * clause
| R of ad * clause * ad list

type 'a lazyT = 'a Lazy.t

type 'a lazy_list =
| Lnil
| Lcons of 'a * 'a lazy_list lazyT

type oracle = action lazy_list lazyT

type answer = bool

val refute_work : iCNF -> cNF -> oracle -> answer

val make_CNF : clause list -> cNF

val refute : clause list -> oracle -> answer
