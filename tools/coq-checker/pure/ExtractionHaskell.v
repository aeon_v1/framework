Require Export SetChecker.

Extraction Language Haskell.

Extract Inductive positive => Int [ "(\x -> 2*x+1)" "(\x -> 2*x)" "1" ]
 "(\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))".

Extract Inlined Constant Pos.compare => "(\x y -> if x == y then Eq else (if x < y then Lt else Gt))".

Extraction "Checker" refute.
