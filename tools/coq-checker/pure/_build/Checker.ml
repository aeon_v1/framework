type bool =
| True
| False

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

type 'a list =
| Nil
| Cons of 'a * 'a list

type comparison =
| Eq
| Lt
| Gt

(** val compOpp : comparison -> comparison **)

let compOpp = function
| Eq -> Eq
| Lt -> Gt
| Gt -> Lt

type 'a sig0 =
  'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

type sumbool =
| Left
| Right

type 'a sumor =
| Inleft of 'a
| Inright

type n =
| N0
| Npos of int

type z =
| Z0
| Zpos of int
| Zneg of int

module Pos =
 struct
  (** val succ : int -> int **)

  let rec succ x =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p -> (fun x -> 2*x)
      (succ p))
      (fun p -> (fun x -> 2*x+1)
      p)
      (fun _ -> (fun x -> 2*x)
      1)
      x

  (** val add : int -> int -> int **)

  let rec add x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun _ -> (fun x -> 2*x)
        (succ p))
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun q -> (fun x -> 2*x)
        (add p q))
        (fun _ -> (fun x -> 2*x+1)
        p)
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (succ q))
        (fun q -> (fun x -> 2*x+1)
        q)
        (fun _ -> (fun x -> 2*x)
        1)
        y)
      x

  (** val add_carry : int -> int -> int **)

  and add_carry x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (add_carry p q))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun _ -> (fun x -> 2*x+1)
        (succ p))
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x)
        (add_carry p q))
        (fun q -> (fun x -> 2*x+1)
        (add p q))
        (fun _ -> (fun x -> 2*x)
        (succ p))
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> (fun x -> 2*x+1)
        (succ q))
        (fun q -> (fun x -> 2*x)
        (succ q))
        (fun _ -> (fun x -> 2*x+1)
        1)
        y)
      x

  (** val pred_double : int -> int **)

  let rec pred_double x =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p -> (fun x -> 2*x+1) ((fun x -> 2*x)
      p))
      (fun p -> (fun x -> 2*x+1)
      (pred_double p))
      (fun _ ->
      1)
      x

  (** val compare_cont : comparison -> int -> int -> comparison **)

  let rec compare_cont r x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        compare_cont r p q)
        (fun q ->
        compare_cont Gt p q)
        (fun _ ->
        Gt)
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        compare_cont Lt p q)
        (fun q ->
        compare_cont r p q)
        (fun _ ->
        Gt)
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Lt)
        (fun _ ->
        Lt)
        (fun _ ->
        r)
        y)
      x

  (** val eqb : int -> int -> bool **)

  let rec eqb p q =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        eqb p0 q0)
        (fun _ ->
        False)
        (fun _ ->
        False)
        q)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        False)
        (fun q0 ->
        eqb p0 q0)
        (fun _ ->
        False)
        q)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        False)
        (fun _ ->
        False)
        (fun _ ->
        True)
        q)
      p

  (** val coq_Nsucc_double : n -> n **)

  let coq_Nsucc_double = function
  | N0 -> Npos 1
  | Npos p -> Npos ((fun x -> 2*x+1) p)

  (** val coq_Ndouble : n -> n **)

  let coq_Ndouble = function
  | N0 -> N0
  | Npos p -> Npos ((fun x -> 2*x) p)

  (** val coq_lxor : int -> int -> n **)

  let rec coq_lxor p q =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        coq_Ndouble (coq_lxor p0 q0))
        (fun q0 ->
        coq_Nsucc_double (coq_lxor p0 q0))
        (fun _ -> Npos ((fun x -> 2*x)
        p0))
        q)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 ->
        coq_Nsucc_double (coq_lxor p0 q0))
        (fun q0 ->
        coq_Ndouble (coq_lxor p0 q0))
        (fun _ -> Npos ((fun x -> 2*x+1)
        p0))
        q)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q0 -> Npos ((fun x -> 2*x)
        q0))
        (fun q0 -> Npos ((fun x -> 2*x+1)
        q0))
        (fun _ ->
        N0)
        q)
      p

  (** val eq_dec : int -> int -> sumbool **)

  let rec eq_dec p y0 =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun p1 ->
        eq_dec p0 p1)
        (fun _ ->
        Right)
        (fun _ ->
        Right)
        y0)
      (fun p0 ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Right)
        (fun p1 ->
        eq_dec p0 p1)
        (fun _ ->
        Right)
        y0)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun _ ->
        Right)
        (fun _ ->
        Right)
        (fun _ ->
        Left)
        y0)
      p
 end

module N =
 struct
  (** val succ_double : n -> n **)

  let succ_double = function
  | N0 -> Npos 1
  | Npos p -> Npos ((fun x -> 2*x+1) p)

  (** val double : n -> n **)

  let double = function
  | N0 -> N0
  | Npos p -> Npos ((fun x -> 2*x) p)

  (** val eqb : n -> n -> bool **)

  let rec eqb n0 m =
    match n0 with
    | N0 ->
      (match m with
       | N0 -> True
       | Npos _ -> False)
    | Npos p ->
      (match m with
       | N0 -> False
       | Npos q -> Pos.eqb p q)

  (** val div2 : n -> n **)

  let div2 = function
  | N0 -> N0
  | Npos p0 ->
    ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
       (fun p -> Npos
       p)
       (fun p -> Npos
       p)
       (fun _ ->
       N0)
       p0)

  (** val even : n -> bool **)

  let even = function
  | N0 -> True
  | Npos p ->
    ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
       (fun _ ->
       False)
       (fun _ ->
       True)
       (fun _ ->
       False)
       p)

  (** val odd : n -> bool **)

  let odd n0 =
    negb (even n0)

  (** val coq_lxor : n -> n -> n **)

  let coq_lxor n0 m =
    match n0 with
    | N0 -> m
    | Npos p ->
      (match m with
       | N0 -> n0
       | Npos q -> Pos.coq_lxor p q)
 end

module Z =
 struct
  (** val double : z -> z **)

  let double = function
  | Z0 -> Z0
  | Zpos p -> Zpos ((fun x -> 2*x) p)
  | Zneg p -> Zneg ((fun x -> 2*x) p)

  (** val succ_double : z -> z **)

  let succ_double = function
  | Z0 -> Zpos 1
  | Zpos p -> Zpos ((fun x -> 2*x+1) p)
  | Zneg p -> Zneg (Pos.pred_double p)

  (** val pred_double : z -> z **)

  let pred_double = function
  | Z0 -> Zneg 1
  | Zpos p -> Zpos (Pos.pred_double p)
  | Zneg p -> Zneg ((fun x -> 2*x+1) p)

  (** val pos_sub : int -> int -> z **)

  let rec pos_sub x y =
    (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        double (pos_sub p q))
        (fun q ->
        succ_double (pos_sub p q))
        (fun _ -> Zpos ((fun x -> 2*x)
        p))
        y)
      (fun p ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q ->
        pred_double (pos_sub p q))
        (fun q ->
        double (pos_sub p q))
        (fun _ -> Zpos
        (Pos.pred_double p))
        y)
      (fun _ ->
      (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
        (fun q -> Zneg ((fun x -> 2*x)
        q))
        (fun q -> Zneg
        (Pos.pred_double q))
        (fun _ ->
        Z0)
        y)
      x

  (** val add : z -> z -> z **)

  let add x y =
    match x with
    | Z0 -> y
    | Zpos x' ->
      (match y with
       | Z0 -> x
       | Zpos y' -> Zpos (Pos.add x' y')
       | Zneg y' -> pos_sub x' y')
    | Zneg x' ->
      (match y with
       | Z0 -> x
       | Zpos y' -> pos_sub y' x'
       | Zneg y' -> Zneg (Pos.add x' y'))

  (** val opp : z -> z **)

  let opp = function
  | Z0 -> Z0
  | Zpos x0 -> Zneg x0
  | Zneg x0 -> Zpos x0

  (** val sub : z -> z -> z **)

  let sub m n0 =
    add m (opp n0)

  (** val compare : z -> z -> comparison **)

  let compare x y =
    match x with
    | Z0 ->
      (match y with
       | Z0 -> Eq
       | Zpos _ -> Lt
       | Zneg _ -> Gt)
    | Zpos x' ->
      (match y with
       | Zpos y' ->
         (fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) x' y'
       | _ -> Gt)
    | Zneg x' ->
      (match y with
       | Zneg y' ->
         compOpp
           ((fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) x'
             y')
       | _ -> Lt)

  (** val max : z -> z -> z **)

  let max n0 m =
    match compare n0 m with
    | Lt -> m
    | _ -> n0

  (** val eq_dec : z -> z -> sumbool **)

  let eq_dec x y =
    match x with
    | Z0 ->
      (match y with
       | Z0 -> Left
       | _ -> Right)
    | Zpos x0 ->
      (match y with
       | Zpos p0 -> Pos.eq_dec x0 p0
       | _ -> Right)
    | Zneg x0 ->
      (match y with
       | Zneg p0 -> Pos.eq_dec x0 p0
       | _ -> Right)
 end

(** val compare_Lt_dec : comparison -> sumbool **)

let compare_Lt_dec = function
| Lt -> Left
| _ -> Right

(** val compare_Gt_dec : comparison -> sumbool **)

let compare_Gt_dec = function
| Gt -> Left
| _ -> Right

type ad = n

type 'a map =
| M0
| M1 of ad * 'a
| M2 of 'a map * 'a map

(** val mapGet : 'a1 map -> ad -> 'a1 option **)

let rec mapGet m a =
  match m with
  | M0 -> None
  | M1 (x, y) ->
    (match N.eqb x a with
     | True -> Some y
     | False -> None)
  | M2 (m1, m2) ->
    (match a with
     | N0 -> mapGet m1 N0
     | Npos p0 ->
       ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
          (fun p ->
          mapGet m2 (Npos p))
          (fun p ->
          mapGet m1 (Npos p))
          (fun _ ->
          mapGet m2 N0)
          p0))

(** val mapPut1 : ad -> 'a1 -> ad -> 'a1 -> int -> 'a1 map **)

let rec mapPut1 a y a' y' p =
  (fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
    (fun _ ->
    match N.odd a with
    | True -> M2 ((M1 ((N.div2 a'), y')), (M1 ((N.div2 a), y)))
    | False -> M2 ((M1 ((N.div2 a), y)), (M1 ((N.div2 a'), y'))))
    (fun p' ->
    let m = mapPut1 (N.div2 a) y (N.div2 a') y' p' in
    (match N.odd a with
     | True -> M2 (M0, m)
     | False -> M2 (m, M0)))
    (fun _ ->
    match N.odd a with
    | True -> M2 ((M1 ((N.div2 a'), y')), (M1 ((N.div2 a), y)))
    | False -> M2 ((M1 ((N.div2 a), y)), (M1 ((N.div2 a'), y'))))
    p

(** val mapPut : 'a1 map -> ad -> 'a1 -> 'a1 map **)

let rec mapPut m x x0 =
  match m with
  | M0 -> M1 (x, x0)
  | M1 (a, y) ->
    (match N.coq_lxor a x with
     | N0 -> M1 (x, x0)
     | Npos p -> mapPut1 a y x x0 p)
  | M2 (m1, m2) ->
    (match x with
     | N0 -> M2 ((mapPut m1 N0 x0), m2)
     | Npos p0 ->
       ((fun fI fO fH n -> if n=1 then fH () else if n mod 2 = 0 then fO (n/2) else fI (n/2))
          (fun p -> M2 (m1,
          (mapPut m2 (Npos p) x0)))
          (fun p -> M2 ((mapPut m1 (Npos p) x0),
          m2))
          (fun _ -> M2 (m1,
          (mapPut m2 N0 x0)))
          p0))

(** val makeM2 : 'a1 map -> 'a1 map -> 'a1 map **)

let makeM2 m m' =
  match m with
  | M0 ->
    (match m' with
     | M0 -> M0
     | M1 (a, y) -> M1 ((N.succ_double a), y)
     | M2 (_, _) -> M2 (m, m'))
  | M1 (a, y) ->
    (match m' with
     | M0 -> M1 ((N.double a), y)
     | _ -> M2 (m, m'))
  | M2 (_, _) -> M2 (m, m')

(** val mapRemove : 'a1 map -> ad -> 'a1 map **)

let rec mapRemove m x =
  match m with
  | M0 -> M0
  | M1 (a, _) ->
    (match N.eqb a x with
     | True -> M0
     | False -> m)
  | M2 (m1, m2) ->
    (match N.odd x with
     | True -> makeM2 m1 (mapRemove m2 (N.div2 x))
     | False -> makeM2 (mapRemove m1 (N.div2 x)) m2)

type 't binaryTree =
| Nought
| Node of 't * 't binaryTree * 't binaryTree

(** val height : 'a1 binaryTree -> z **)

let rec height = function
| Nought -> Z0
| Node (_, l, r) -> Z.add (Zpos 1) (Z.max (height l) (height r))

(** val bT_rot_L : 'a1 binaryTree -> 'a1 binaryTree **)

let bT_rot_L = function
| Nought -> Nought
| Node (n0, l, b) ->
  (match b with
   | Nought -> Node (n0, l, Nought)
   | Node (n', rL, rR) -> Node (n', (Node (n0, l, rL)), rR))

(** val bT_rot_R : 'a1 binaryTree -> 'a1 binaryTree **)

let bT_rot_R = function
| Nought -> Nought
| Node (n0, b, r) ->
  (match b with
   | Nought -> Node (n0, Nought, r)
   | Node (n', lL, lR) -> Node (n', lL, (Node (n0, lR, r))))

(** val bT_rot_LL : 'a1 binaryTree -> 'a1 binaryTree **)

let bT_rot_LL = function
| Nought -> Nought
| Node (n0, l, r) -> bT_rot_L (Node (n0, l, (bT_rot_R r)))

(** val bT_rot_RR : 'a1 binaryTree -> 'a1 binaryTree **)

let bT_rot_RR = function
| Nought -> Nought
| Node (n0, l, r) -> bT_rot_R (Node (n0, (bT_rot_L l), r))

(** val bT_balance : 'a1 binaryTree -> 'a1 binaryTree **)

let bT_balance tree = match tree with
| Nought -> Nought
| Node (n0, l, r) ->
  (match Z.eq_dec (Z.sub (height l) (height r)) (Zpos ((fun x -> 2*x) 1)) with
   | Left ->
     (match l with
      | Nought -> Node (n0, l, r)
      | Node (_, lL, lR) ->
        (match compare_Gt_dec (Z.compare (height lL) (height lR)) with
         | Left -> bT_rot_R tree
         | Right -> bT_rot_RR tree))
   | Right ->
     (match Z.eq_dec (Z.sub (height r) (height l)) (Zpos ((fun x -> 2*x) 1)) with
      | Left ->
        (match r with
         | Nought -> Node (n0, l, r)
         | Node (_, rL, rR) ->
           (match compare_Lt_dec (Z.compare (height rL) (height rR)) with
            | Left -> bT_rot_L tree
            | Right -> bT_rot_LL tree))
      | Right -> tree))

(** val bT_add :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree **)

let rec bT_add t_compare t tree = match tree with
| Nought -> Node (t, Nought, Nought)
| Node (t', l, r) ->
  (match t_compare t t' with
   | Eq -> tree
   | Lt -> bT_balance (Node (t', (bT_add t_compare t l), r))
   | Gt -> bT_balance (Node (t', l, (bT_add t_compare t r))))

(** val bT_split : 'a1 binaryTree -> 'a1 -> ('a1, 'a1 binaryTree) prod **)

let rec bT_split tree default =
  match tree with
  | Nought -> Pair (default, Nought)
  | Node (t, l, r) ->
    (match l with
     | Nought -> Pair (t, r)
     | Node (_, _, _) ->
       let Pair (t', l') = bT_split l default in Pair (t', (Node (t, l', r))))

(** val bT_in_dec :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> sumbool **)

let rec bT_in_dec t_compare t = function
| Nought -> Right
| Node (t0, b, b0) ->
  let z0 = t_compare t t0 in
  (match z0 with
   | Eq -> Left
   | Lt -> bT_in_dec t_compare t b
   | Gt -> bT_in_dec t_compare t b0)

(** val bT_remove :
    ('a1 -> 'a1 -> comparison) -> 'a1 -> 'a1 binaryTree -> 'a1 binaryTree **)

let rec bT_remove t_compare t = function
| Nought -> Nought
| Node (t', l, r) ->
  (match t_compare t t' with
   | Eq ->
     (match r with
      | Nought -> l
      | Node (_, _, _) ->
        let Pair (min, r') = bT_split r t in Node (min, l, r'))
   | Lt -> Node (t', (bT_remove t_compare t l), r)
   | Gt -> Node (t', l, (bT_remove t_compare t r)))

(** val bT_diff :
    ('a1 -> 'a1 -> comparison) -> 'a1 binaryTree -> 'a1 binaryTree -> 'a1
    binaryTree **)

let rec bT_diff t_compare tree = function
| Nought -> tree
| Node (t', l, r) ->
  bT_remove t_compare t' (bT_diff t_compare (bT_diff t_compare tree l) r)

type literal =
| Pos of int
| Neg of int

(** val negate : literal -> literal **)

let negate = function
| Pos n0 -> Neg n0
| Neg n0 -> Pos n0

(** val literal_compare : literal -> literal -> comparison **)

let literal_compare l l' =
  match l with
  | Pos n0 ->
    (match l' with
     | Pos n' ->
       (fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) n0 n'
     | Neg _ -> Gt)
  | Neg n0 ->
    (match l' with
     | Pos _ -> Lt
     | Neg n' ->
       (fun x y -> if x = y then Eq else (if x < y then Lt else Gt)) n0 n')

type clause = literal list

(** val clause_compare : clause -> clause -> comparison **)

let rec clause_compare cl cl' =
  match cl with
  | Nil ->
    (match cl' with
     | Nil -> Eq
     | Cons (_, _) -> Lt)
  | Cons (l, c) ->
    (match cl' with
     | Nil -> Gt
     | Cons (l', c') ->
       (match literal_compare l l' with
        | Eq -> clause_compare c c'
        | x -> x))

type setClause = literal binaryTree

(** val sC_add : literal -> literal binaryTree -> literal binaryTree **)

let sC_add =
  bT_add literal_compare

(** val sC_diff :
    literal binaryTree -> literal binaryTree -> literal binaryTree **)

let sC_diff =
  bT_diff literal_compare

(** val clause_to_SetClause : clause -> setClause **)

let rec clause_to_SetClause = function
| Nil -> Nought
| Cons (l, cl') -> sC_add l (clause_to_SetClause cl')

(** val true_SC : setClause **)

let true_SC =
  Node ((Pos 1), (Node ((Neg 1), Nought, Nought)), Nought)

(** val setClause_eq_nil_cons :
    setClause -> (literal, (setClause, setClause) sigT) sigT sumor **)

let setClause_eq_nil_cons = function
| Nought -> Inright
| Node (l, c0, b) -> Inleft (ExistT (l, (ExistT (c0, b))))

type cNF = clause binaryTree

(** val cNF_add : clause -> clause binaryTree -> clause binaryTree **)

let cNF_add =
  bT_add clause_compare

type iCNF = setClause map

(** val empty_ICNF : iCNF **)

let empty_ICNF =
  M0

(** val get_ICNF : iCNF -> ad -> setClause **)

let get_ICNF c i =
  match mapGet c i with
  | Some cl -> cl
  | None -> true_SC

(** val del_ICNF : ad -> iCNF -> iCNF **)

let del_ICNF i c =
  mapRemove c i

(** val add_ICNF : ad -> setClause -> iCNF -> setClause map **)

let add_ICNF i cl c =
  mapPut c i cl

(** val propagate : iCNF -> setClause -> ad list -> bool **)

let rec propagate cs c = function
| Nil -> False
| Cons (i, is0) ->
  (match setClause_eq_nil_cons (sC_diff (get_ICNF cs i) c) with
   | Inleft h ->
     let ExistT (l, hl) = h in
     let ExistT (c', hc) = hl in
     (match setClause_eq_nil_cons c' with
      | Inleft _ -> False
      | Inright ->
        (match setClause_eq_nil_cons hc with
         | Inleft _ -> False
         | Inright -> propagate cs (sC_add (negate l) c) is0))
   | Inright -> True)

type action =
| D of ad list
| O of ad * clause
| R of ad * clause * ad list

type 'a lazyT = 'a Lazy.t

type 'a lazy_list =
| Lnil
| Lcons of 'a * 'a lazy_list lazyT

type oracle = action lazy_list lazyT

type answer = bool

(** val refute_work : iCNF -> cNF -> oracle -> answer **)

let rec refute_work w c o0 =
  match Lazy.force o0 with
  | Lnil -> False
  | Lcons (a, o') ->
    (match a with
     | D l ->
       (match l with
        | Nil -> refute_work w c o'
        | Cons (i, is) ->
          refute_work (del_ICNF i w) c (Lazy.from_val (Lcons ((D is), o'))))
     | O (i, cl) ->
       (match bT_in_dec clause_compare cl c with
        | Left -> refute_work (add_ICNF i (clause_to_SetClause cl) w) c o'
        | Right -> False)
     | R (i, cl, is) ->
       (match cl with
        | Nil -> propagate w Nought is
        | Cons (l, l0) ->
          (match propagate w (clause_to_SetClause (Cons (l, l0))) is with
           | True ->
             refute_work (add_ICNF i (clause_to_SetClause (Cons (l, l0))) w)
               c o'
           | False -> False)))

(** val make_CNF : clause list -> cNF **)

let rec make_CNF = function
| Nil -> Nought
| Cons (cl, l') -> cNF_add cl (make_CNF l')

(** val refute : clause list -> oracle -> answer **)

let refute c o =
  refute_work empty_ICNF (make_CNF c) o
