{-# OPTIONS_GHC -cpp -XForeignFunctionInterface -XUnliftedFFITypes -XImplicitParams -XScopedTypeVariables -XUnboxedTuples -XTypeSynonymInstances -XStandaloneDeriving -XDeriveDataTypeable -XDeriveFunctor -XDeriveFoldable -XDeriveTraversable -XDeriveGeneric -XFlexibleContexts -XFlexibleInstances -XConstrainedClassMethods -XMultiParamTypeClasses -XFunctionalDependencies -XMagicHash -XPolymorphicComponents -XExistentialQuantification -XUnicodeSyntax -XPostfixOperators -XPatternGuards -XLiberalTypeSynonyms -XRankNTypes -XTypeOperators -XExplicitNamespaces -XRecursiveDo -XParallelListComp -XEmptyDataDecls -XKindSignatures -XGeneralizedNewtypeDeriving #-}
module Checker where

import qualified Prelude
import Prelude (Int,(==),(-),(*),(+),(<))
import qualified Data.Set as Set
deriving instance Prelude.Eq Literal
deriving instance (Prelude.Eq a) => Prelude.Eq (List a)
deriving instance Prelude.Ord Literal
deriving instance (Prelude.Ord a) => Prelude.Ord (List a)

__ :: any
__ = Prelude.error "Logical or arity value used"

and_rect :: (() -> () -> a1) -> a1
and_rect f =
  f __ __

data Bool =
   True
 | False

andb :: Bool -> Bool -> Bool
andb b1 b2 =
  case b1 of {
   True -> b2;
   False -> False}

negb :: Bool -> Bool
negb b =
  case b of {
   True -> False;
   False -> True}

data Option a =
   Some a
 | None

data Prod a b =
   Pair a b

data List a =
   Nil
 | Cons a (List a)

data Comparison =
   Eq
 | Lt
 | Gt

comparison_rect :: a1 -> a1 -> a1 -> Comparison -> a1
comparison_rect f f0 f1 c =
  case c of {
   Eq -> f;
   Lt -> f0;
   Gt -> f1}

comparison_rec :: a1 -> a1 -> a1 -> Comparison -> a1
comparison_rec =
  comparison_rect

compOpp :: Comparison -> Comparison
compOpp r =
  case r of {
   Eq -> Eq;
   Lt -> Gt;
   Gt -> Lt}

id :: a1 -> a1
id x =
  x

type Sig a =
  a
  -- singleton inductive, whose constructor was exist
  
sig_rect :: (a1 -> () -> a2) -> a1 -> a2
sig_rect f s =
  f s __

sig_rec :: (a1 -> () -> a2) -> a1 -> a2
sig_rec =
  sig_rect

data SigT a p =
   ExistT a p

data Sumbool =
   Left
 | Right

sumbool_rect :: (() -> a1) -> (() -> a1) -> Sumbool -> a1
sumbool_rect f f0 s =
  case s of {
   Left -> f __;
   Right -> f0 __}

sumbool_rec :: (() -> a1) -> (() -> a1) -> Sumbool -> a1
sumbool_rec =
  sumbool_rect

data Sumor a =
   Inleft a
 | Inright

positive_rect :: (Int -> a1 -> a1) -> (Int -> a1 -> a1) -> a1 -> Int -> a1
positive_rect f f0 f1 p =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p0 ->
    f p0 (positive_rect f f0 f1 p0))
    (\p0 ->
    f0 p0 (positive_rect f f0 f1 p0))
    (\_ ->
    f1)
    p

positive_rec :: (Int -> a1 -> a1) -> (Int -> a1 -> a1) -> a1 -> Int -> a1
positive_rec =
  positive_rect

data N =
   N0
 | Npos Int

data Z =
   Z0
 | Zpos Int
 | Zneg Int

z_rect :: a1 -> (Int -> a1) -> (Int -> a1) -> Z -> a1
z_rect f f0 f1 z =
  case z of {
   Z0 -> f;
   Zpos x -> f0 x;
   Zneg x -> f1 x}

z_rec :: a1 -> (Int -> a1) -> (Int -> a1) -> Z -> a1
z_rec =
  z_rect

succ :: Int -> Int
succ x =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p -> (\x -> 2*x)
    (succ p))
    (\p -> (\x -> 2*x+1)
    p)
    (\_ -> (\x -> 2*x)
    1)
    x

add :: Int -> Int -> Int
add x y =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x)
      (add_carry p q))
      (\q -> (\x -> 2*x+1)
      (add p q))
      (\_ -> (\x -> 2*x)
      (succ p))
      y)
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x+1)
      (add p q))
      (\q -> (\x -> 2*x)
      (add p q))
      (\_ -> (\x -> 2*x+1)
      p)
      y)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x)
      (succ q))
      (\q -> (\x -> 2*x+1)
      q)
      (\_ -> (\x -> 2*x)
      1)
      y)
    x

add_carry :: Int -> Int -> Int
add_carry x y =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x+1)
      (add_carry p q))
      (\q -> (\x -> 2*x)
      (add_carry p q))
      (\_ -> (\x -> 2*x+1)
      (succ p))
      y)
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x)
      (add_carry p q))
      (\q -> (\x -> 2*x+1)
      (add p q))
      (\_ -> (\x -> 2*x)
      (succ p))
      y)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> (\x -> 2*x+1)
      (succ q))
      (\q -> (\x -> 2*x)
      (succ q))
      (\_ -> (\x -> 2*x+1)
      1)
      y)
    x

pred_double :: Int -> Int
pred_double x =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p -> (\x -> 2*x+1) ((\x -> 2*x)
    p))
    (\p -> (\x -> 2*x+1)
    (pred_double p))
    (\_ ->
    1)
    x

compare_cont :: Comparison -> Int -> Int -> Comparison
compare_cont r x y =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q ->
      compare_cont r p q)
      (\q ->
      compare_cont Gt p q)
      (\_ ->
      Gt)
      y)
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q ->
      compare_cont Lt p q)
      (\q ->
      compare_cont r p q)
      (\_ ->
      Gt)
      y)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      Lt)
      (\_ ->
      Lt)
      (\_ ->
      r)
      y)
    x

eqb :: Int -> Int -> Bool
eqb p q =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q0 ->
      eqb p0 q0)
      (\_ ->
      False)
      (\_ ->
      False)
      q)
    (\p0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      False)
      (\q0 ->
      eqb p0 q0)
      (\_ ->
      False)
      q)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      False)
      (\_ ->
      False)
      (\_ ->
      True)
      q)
    p

nsucc_double :: N -> N
nsucc_double x =
  case x of {
   N0 -> Npos 1;
   Npos p -> Npos ((\x -> 2*x+1) p)}

ndouble :: N -> N
ndouble n =
  case n of {
   N0 -> N0;
   Npos p -> Npos ((\x -> 2*x) p)}

lxor :: Int -> Int -> N
lxor p q =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q0 ->
      ndouble (lxor p0 q0))
      (\q0 ->
      nsucc_double (lxor p0 q0))
      (\_ -> Npos ((\x -> 2*x)
      p0))
      q)
    (\p0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q0 ->
      nsucc_double (lxor p0 q0))
      (\q0 ->
      ndouble (lxor p0 q0))
      (\_ -> Npos ((\x -> 2*x+1)
      p0))
      q)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q0 -> Npos ((\x -> 2*x)
      q0))
      (\q0 -> Npos ((\x -> 2*x+1)
      q0))
      (\_ ->
      N0)
      q)
    p

eq_dec :: Int -> Int -> Sumbool
eq_dec x y =
  positive_rec (\_ h y0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\p0 ->
      sumbool_rec (\_ -> Left) (\_ -> Right) (h p0))
      (\_ ->
      Right)
      (\_ ->
      Right)
      y0) (\_ h y0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      Right)
      (\p0 ->
      sumbool_rec (\_ -> Left) (\_ -> Right) (h p0))
      (\_ ->
      Right)
      y0) (\y0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      Right)
      (\_ ->
      Right)
      (\_ ->
      Left)
      y0) x y

succ_double :: N -> N
succ_double x =
  case x of {
   N0 -> Npos 1;
   Npos p -> Npos ((\x -> 2*x+1) p)}

double :: N -> N
double n =
  case n of {
   N0 -> N0;
   Npos p -> Npos ((\x -> 2*x) p)}

eqb0 :: N -> N -> Bool
eqb0 n m =
  case n of {
   N0 ->
    case m of {
     N0 -> True;
     Npos _ -> False};
   Npos p ->
    case m of {
     N0 -> False;
     Npos q -> eqb p q}}

div2 :: N -> N
div2 n =
  case n of {
   N0 -> N0;
   Npos p0 ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\p -> Npos
      p)
      (\p -> Npos
      p)
      (\_ ->
      N0)
      p0}

even :: N -> Bool
even n =
  case n of {
   N0 -> True;
   Npos p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\_ ->
      False)
      (\_ ->
      True)
      (\_ ->
      False)
      p}

odd :: N -> Bool
odd n =
  negb (even n)

lxor0 :: N -> N -> N
lxor0 n m =
  case n of {
   N0 -> m;
   Npos p ->
    case m of {
     N0 -> n;
     Npos q -> lxor p q}}

double0 :: Z -> Z
double0 x =
  case x of {
   Z0 -> Z0;
   Zpos p -> Zpos ((\x -> 2*x) p);
   Zneg p -> Zneg ((\x -> 2*x) p)}

succ_double0 :: Z -> Z
succ_double0 x =
  case x of {
   Z0 -> Zpos 1;
   Zpos p -> Zpos ((\x -> 2*x+1) p);
   Zneg p -> Zneg (pred_double p)}

pred_double0 :: Z -> Z
pred_double0 x =
  case x of {
   Z0 -> Zneg 1;
   Zpos p -> Zpos (pred_double p);
   Zneg p -> Zneg ((\x -> 2*x+1) p)}

pos_sub :: Int -> Int -> Z
pos_sub x y =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q ->
      double0 (pos_sub p q))
      (\q ->
      succ_double0 (pos_sub p q))
      (\_ -> Zpos ((\x -> 2*x)
      p))
      y)
    (\p ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q ->
      pred_double0 (pos_sub p q))
      (\q ->
      double0 (pos_sub p q))
      (\_ -> Zpos
      (pred_double p))
      y)
    (\_ ->
    (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
      (\q -> Zneg ((\x -> 2*x)
      q))
      (\q -> Zneg
      (pred_double q))
      (\_ ->
      Z0)
      y)
    x

add0 :: Z -> Z -> Z
add0 x y =
  case x of {
   Z0 -> y;
   Zpos x' ->
    case y of {
     Z0 -> x;
     Zpos y' -> Zpos (add x' y');
     Zneg y' -> pos_sub x' y'};
   Zneg x' ->
    case y of {
     Z0 -> x;
     Zpos y' -> pos_sub y' x';
     Zneg y' -> Zneg (add x' y')}}

opp :: Z -> Z
opp x =
  case x of {
   Z0 -> Z0;
   Zpos x0 -> Zneg x0;
   Zneg x0 -> Zpos x0}

sub :: Z -> Z -> Z
sub m n =
  add0 m (opp n)

compare :: Z -> Z -> Comparison
compare x y =
  case x of {
   Z0 ->
    case y of {
     Z0 -> Eq;
     Zpos _ -> Lt;
     Zneg _ -> Gt};
   Zpos x' ->
    case y of {
     Zpos y' ->
      (\x y -> if x == y then Eq else (if x < y then Lt else Gt)) x' y';
     _ -> Gt};
   Zneg x' ->
    case y of {
     Zneg y' ->
      compOpp
        ((\x y -> if x == y then Eq else (if x < y then Lt else Gt)) x' y');
     _ -> Lt}}

max :: Z -> Z -> Z
max n m =
  case compare n m of {
   Lt -> m;
   _ -> n}

eq_dec0 :: Z -> Z -> Sumbool
eq_dec0 x y =
  z_rec (\y0 ->
    case y0 of {
     Z0 -> Left;
     _ -> Right}) (\p y0 ->
    case y0 of {
     Zpos p0 -> sumbool_rec (\_ -> Left) (\_ -> Right) (eq_dec p p0);
     _ -> Right}) (\p y0 ->
    case y0 of {
     Zneg p0 -> sumbool_rec (\_ -> Left) (\_ -> Right) (eq_dec p p0);
     _ -> Right}) x y

compare_Lt_dec :: Comparison -> Sumbool
compare_Lt_dec c =
  comparison_rec Right Left Right c

compare_Gt_dec :: Comparison -> Sumbool
compare_Gt_dec c =
  comparison_rec Right Right Left c

type Ad = N

data Map a =
   M0
 | M1 Ad a
 | M2 (Map a) (Map a)

mapGet :: (Map a1) -> Ad -> Option a1
mapGet m a =
  case m of {
   M0 -> None;
   M1 x y ->
    case eqb0 x a of {
     True -> Some y;
     False -> None};
   M2 m1 m2 ->
    case a of {
     N0 -> mapGet m1 N0;
     Npos p0 ->
      (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
        (\p ->
        mapGet m2 (Npos p))
        (\p ->
        mapGet m1 (Npos p))
        (\_ ->
        mapGet m2 N0)
        p0}}

mapPut1 :: Ad -> a1 -> Ad -> a1 -> Int -> Map a1
mapPut1 a y a' y' p =
  (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
    (\_ ->
    case odd a of {
     True -> M2 (M1 (div2 a') y') (M1 (div2 a) y);
     False -> M2 (M1 (div2 a) y) (M1 (div2 a') y')})
    (\p' ->
    let {m = mapPut1 (div2 a) y (div2 a') y' p'} in
    case odd a of {
     True -> M2 M0 m;
     False -> M2 m M0})
    (\_ ->
    case odd a of {
     True -> M2 (M1 (div2 a') y') (M1 (div2 a) y);
     False -> M2 (M1 (div2 a) y) (M1 (div2 a') y')})
    p

mapPut :: (Map a1) -> Ad -> a1 -> Map a1
mapPut m x x0 =
  case m of {
   M0 -> M1 x x0;
   M1 a y ->
    case lxor0 a x of {
     N0 -> M1 x x0;
     Npos p -> mapPut1 a y x x0 p};
   M2 m1 m2 ->
    case x of {
     N0 -> M2 (mapPut m1 N0 x0) m2;
     Npos p0 ->
      (\fI fO fH n -> if n==1 then fH () else if n `Prelude.mod` 2 == 0 then fO (n `Prelude.div` 2) else fI (n `Prelude.div` 2))
        (\p -> M2 m1
        (mapPut m2 (Npos p) x0))
        (\p -> M2 (mapPut m1 (Npos p) x0)
        m2)
        (\_ -> M2 m1
        (mapPut m2 N0 x0))
        p0}}

makeM2 :: (Map a1) -> (Map a1) -> Map a1
makeM2 m m' =
  case m of {
   M0 ->
    case m' of {
     M0 -> M0;
     M1 a y -> M1 (succ_double a) y;
     M2 _ _ -> M2 m m'};
   M1 a y ->
    case m' of {
     M0 -> M1 (double a) y;
     _ -> M2 m m'};
   M2 _ _ -> M2 m m'}

mapRemove :: (Map a1) -> Ad -> Map a1
mapRemove m x =
  case m of {
   M0 -> M0;
   M1 a _ ->
    case eqb0 a x of {
     True -> M0;
     False -> m};
   M2 m1 m2 ->
    case odd x of {
     True -> makeM2 m1 (mapRemove m2 (div2 x));
     False -> makeM2 (mapRemove m1 (div2 x)) m2}}

data BinaryTree t =
   Nought
 | Node t (BinaryTree t) (BinaryTree t)

binaryTree_rect :: a2 -> (a1 -> (BinaryTree a1) -> a2 -> (BinaryTree 
                   a1) -> a2 -> a2) -> (BinaryTree a1) -> a2
binaryTree_rect f f0 b =
  case b of {
   Nought -> f;
   Node t b0 b1 ->
    f0 t b0 (binaryTree_rect f f0 b0) b1 (binaryTree_rect f f0 b1)}

binaryTree_rec :: a2 -> (a1 -> (BinaryTree a1) -> a2 -> (BinaryTree a1) -> a2
                  -> a2) -> (BinaryTree a1) -> a2
binaryTree_rec =
  binaryTree_rect

height :: (BinaryTree a1) -> Z
height tree =
  case tree of {
   Nought -> Z0;
   Node _ l r -> add0 (Zpos 1) (max (height l) (height r))}

bT_rot_L :: (BinaryTree a1) -> BinaryTree a1
bT_rot_L tree =
  case tree of {
   Nought -> Nought;
   Node n l b ->
    case b of {
     Nought -> Node n l Nought;
     Node n' rL rR -> Node n' (Node n l rL) rR}}

bT_rot_R :: (BinaryTree a1) -> BinaryTree a1
bT_rot_R tree =
  case tree of {
   Nought -> Nought;
   Node n b r ->
    case b of {
     Nought -> Node n Nought r;
     Node n' lL lR -> Node n' lL (Node n lR r)}}

bT_rot_LL :: (BinaryTree a1) -> BinaryTree a1
bT_rot_LL tree =
  case tree of {
   Nought -> Nought;
   Node n l r -> bT_rot_L (Node n l (bT_rot_R r))}

bT_rot_RR :: (BinaryTree a1) -> BinaryTree a1
bT_rot_RR tree =
  case tree of {
   Nought -> Nought;
   Node n l r -> bT_rot_R (Node n (bT_rot_L l) r)}

bT_balance :: (BinaryTree a1) -> BinaryTree a1
bT_balance tree =
  case tree of {
   Nought -> Nought;
   Node n l r ->
    case eq_dec0 (sub (height l) (height r)) (Zpos ((\x -> 2*x) 1)) of {
     Left ->
      case l of {
       Nought -> Node n l r;
       Node _ lL lR ->
        case compare_Gt_dec (compare (height lL) (height lR)) of {
         Left -> bT_rot_R tree;
         Right -> bT_rot_RR tree}};
     Right ->
      case eq_dec0 (sub (height r) (height l)) (Zpos ((\x -> 2*x) 1)) of {
       Left ->
        case r of {
         Nought -> Node n l r;
         Node _ rL rR ->
          case compare_Lt_dec (compare (height rL) (height rR)) of {
           Left -> bT_rot_L tree;
           Right -> bT_rot_LL tree}};
       Right -> tree}}}

bT_add :: (a1 -> a1 -> Comparison) -> a1 -> (BinaryTree a1) -> BinaryTree a1
bT_add t_compare t tree =
  case tree of {
   Nought -> Node t Nought Nought;
   Node t' l r ->
    case t_compare t t' of {
     Eq -> tree;
     Lt -> bT_balance (Node t' (bT_add t_compare t l) r);
     Gt -> bT_balance (Node t' l (bT_add t_compare t r))}}

bT_split :: (BinaryTree a1) -> a1 -> Prod a1 (BinaryTree a1)
bT_split tree default0 =
  case tree of {
   Nought -> Pair default0 Nought;
   Node t l r ->
    case l of {
     Nought -> Pair t r;
     Node _ _ _ ->
      case bT_split l default0 of {
       Pair t' l' -> Pair t' (Node t l' r)}}}

bT_in_dec :: (a1 -> a1 -> Comparison) -> a1 -> (BinaryTree a1) -> Sumbool
bT_in_dec t_compare t tree =
  binaryTree_rec (\_ -> Right) (\t0 _ iHTree1 _ iHTree2 _ ->
    let {z = t_compare t t0} in
    comparison_rec (\_ -> Left) (\_ ->
      sumbool_rec (\_ -> Left) (\_ -> Right) (iHTree1 __)) (\_ ->
      sumbool_rec (\_ -> Left) (\_ -> Right) (iHTree2 __)) z __) tree __

bT_remove :: (a1 -> a1 -> Comparison) -> a1 -> (BinaryTree a1) -> BinaryTree
             a1
bT_remove t_compare t tree =
  case tree of {
   Nought -> Nought;
   Node t' l r ->
    case t_compare t t' of {
     Eq ->
      case r of {
       Nought -> l;
       Node _ _ _ ->
        case bT_split r t of {
         Pair min r' -> Node min l r'}};
     Lt -> Node t' (bT_remove t_compare t l) r;
     Gt -> Node t' l (bT_remove t_compare t r)}}

bT_diff :: (a1 -> a1 -> Comparison) -> (BinaryTree a1) -> (BinaryTree 
           a1) -> BinaryTree a1
bT_diff t_compare tree tree' =
  case tree' of {
   Nought -> tree;
   Node t' l r ->
    bT_remove t_compare t' (bT_diff t_compare (bT_diff t_compare tree l) r)}

data Literal =
   Pos Int
 | Neg Int

negate :: Literal -> Literal
negate l =
  case l of {
   Pos n -> Neg n;
   Neg n -> Pos n}

literal_compare :: Literal -> Literal -> Comparison
literal_compare l l' =
  case l of {
   Pos n ->
    case l' of {
     Pos n' ->
      (\x y -> if x == y then Eq else (if x < y then Lt else Gt)) n n';
     Neg _ -> Gt};
   Neg n ->
    case l' of {
     Pos _ -> Lt;
     Neg n' ->
      (\x y -> if x == y then Eq else (if x < y then Lt else Gt)) n n'}}

type Clause = List Literal

clause_compare :: Clause -> Clause -> Comparison
clause_compare cl cl' =
  case cl of {
   Nil ->
    case cl' of {
     Nil -> Eq;
     Cons _ _ -> Lt};
   Cons l c ->
    case cl' of {
     Nil -> Gt;
     Cons l' c' ->
      case literal_compare l l' of {
       Eq -> clause_compare c c';
       x -> x}}}

type SetClause = BinaryTree Literal

sC_add :: Literal -> (BinaryTree Literal) -> BinaryTree Literal
sC_add =
  bT_add literal_compare

sC_diff :: (BinaryTree Literal) -> (BinaryTree Literal) -> BinaryTree Literal
sC_diff =
  bT_diff literal_compare

clause_to_SetClause :: Clause -> SetClause
clause_to_SetClause cl =
  case cl of {
   Nil -> Nought;
   Cons l cl' -> sC_add l (clause_to_SetClause cl')}

true_SC :: SetClause
true_SC =
  Node (Pos 1) (Node (Neg 1) Nought Nought) Nought

setClause_eq_nil_cons :: SetClause -> Sumor
                         (SigT Literal (SigT SetClause SetClause))
setClause_eq_nil_cons c =
  case c of {
   Nought -> Inright;
   Node l c0 b -> Inleft (ExistT l (ExistT c0 b))}

type CNF = BinaryTree Clause

cNF_add :: Clause -> (BinaryTree Clause) -> BinaryTree Clause
cNF_add =
  bT_add clause_compare

type ICNF = Map SetClause

empty_ICNF :: ICNF
empty_ICNF =
  M0

get_ICNF :: ICNF -> Ad -> SetClause
get_ICNF c i =
  case mapGet c i of {
   Some cl -> cl;
   None -> true_SC}

del_ICNF :: Ad -> ICNF -> ICNF
del_ICNF i c =
  mapRemove c i

add_ICNF :: Ad -> SetClause -> ICNF -> Map SetClause
add_ICNF i cl c =
  mapPut c i cl

propagate :: ICNF -> SetClause -> (List Ad) -> Bool
propagate cs c is =
  case is of {
   Nil -> False;
   Cons i is0 ->
    case setClause_eq_nil_cons (sC_diff (get_ICNF cs i) c) of {
     Inleft h ->
      case h of {
       ExistT l hl ->
        case hl of {
         ExistT c' hc ->
          case setClause_eq_nil_cons c' of {
           Inleft _ -> False;
           Inright ->
            case setClause_eq_nil_cons hc of {
             Inleft _ -> False;
             Inright -> propagate cs (sC_add (negate l) c) is0}}}};
     Inright -> True}}

data Action =
   D (List Ad)
 | O Ad Clause
 | R Ad Clause (List Ad)

type LazyT x = x

data Lazy_list a =
   Lnil
 | Lcons a (LazyT (Lazy_list a))

type Oracle = LazyT (Lazy_list Action)

type Answer = Bool

force :: Oracle -> Oracle
force =
  id

fromVal :: Oracle -> Oracle
fromVal =
  id

refute_work :: ICNF -> CNF -> Oracle -> Answer
refute_work x x0 x2 =
  and_rect (\_ _ ->
    and_rect (\_ _ ->
      and_rect (\_ _ w c _ o0 ->
        let {
         hrec w0 c0 o1 =
           case force o1 of {
            Lnil -> False;
            Lcons a o' ->
             case a of {
              D l ->
               case l of {
                Nil -> sig_rec (\rec_res _ -> rec_res) (hrec w0 c0 o');
                Cons i is ->
                 sig_rec (\rec_res _ -> rec_res)
                   (hrec (del_ICNF i w0) c0 (fromVal (Lcons (D is) o')))};
              O i cl ->
               case bT_in_dec clause_compare cl c0 of {
                Left ->
                 sig_rec (\rec_res _ -> rec_res)
                   (hrec (add_ICNF i (clause_to_SetClause cl) w0) c0 o');
                Right -> False};
              R i cl is ->
               case cl of {
                Nil -> propagate w0 Nought is;
                Cons l l0 ->
                 sig_rec (\rec_res _ ->
                   andb (propagate w0 (clause_to_SetClause (Cons l l0)) is)
                     rec_res)
                   (hrec (add_ICNF i (clause_to_SetClause (Cons l l0)) w0) c0
                     o')}}}}
        in hrec w c o0))) x x0 __ x2

make_CNF :: (List Clause) -> CNF
make_CNF l =
  case l of {
   Nil -> Nought;
   Cons cl l' -> cNF_add cl (make_CNF l')}

refute :: (List Clause) -> Oracle -> Answer
refute c o =
  refute_work empty_ICNF (make_CNF c) o

