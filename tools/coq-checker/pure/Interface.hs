{-# OPTIONS_GHC -cpp -XForeignFunctionInterface -XUnliftedFFITypes -XImplicitParams -XScopedTypeVariables -XUnboxedTuples -XTypeSynonymInstances -XStandaloneDeriving -XDeriveDataTypeable -XDeriveFunctor -XDeriveFoldable -XDeriveTraversable -XDeriveGeneric -XFlexibleContexts -XFlexibleInstances -XConstrainedClassMethods -XMultiParamTypeClasses -XFunctionalDependencies -XMagicHash -XPolymorphicComponents -XExistentialQuantification -XUnicodeSyntax -XPostfixOperators -XPatternGuards -XLiberalTypeSynonyms -XRankNTypes -XTypeOperators -XExplicitNamespaces -XRecursiveDo -XParallelListComp -XEmptyDataDecls -XKindSignatures -XGeneralizedNewtypeDeriving #-}
import qualified Checker
import Prelude
import System.Environment
import System.IO
import Data.List
import qualified Data.Set as Set
import Debug.Trace
import Control.DeepSeq
import Control.Exception

deriving instance Show Checker.Bool 
--deriving instance Show Checker.Nat
deriving instance Show Checker.Action
deriving instance Show a => Show (Checker.List a)
deriving instance Show Checker.N
--deriving instance Show Checker.Positive
deriving instance Show Checker.Literal

data Action = D [Int] | O Int [Int] | R Int [Int] [Int] deriving Read

action_to_Action :: Action -> Checker.Action
--action_to_Action (D is) = Checker.D (list_to_List (map nat_to_Nat is))
--action_to_Action (O i c) = Checker.O (nat_to_Nat i) (clause_to_Clause c)
--action_to_Action (R i c is) = Checker.R (nat_to_Nat i) (clause_to_Clause c) (list_to_List is)
action_to_Action (D is) = Checker.D (list_to_List (map nat_to_N is))
action_to_Action (O i c) = Checker.O (nat_to_N i) (clause_to_Clause c)
action_to_Action (R i c is) = Checker.R (nat_to_N i) (clause_to_Clause c) (list_to_List (map nat_to_N is))

list_to_List :: [a] -> Checker.List a
list_to_List [] = Checker.Nil
list_to_List (x : l) = Checker.Cons x (list_to_List l)

list_to_lazy_List :: [Checker.Action] -> Checker.Lazy_list Checker.Action
list_to_lazy_List [] = Checker.Lnil
list_to_lazy_List (x : l) = Checker.Lcons x (list_to_lazy_List l)

clause_to_Clause :: [Int] -> Checker.Clause
clause_to_Clause = list_to_List . map literal_to_Literal

--literal_to_Literal :: Int -> Checker.Prod Checker.Nat Checker.Bool
--literal_to_Literal x = Checker.Pair (nat_to_Nat x) (bool_to_Bool (x > 0))
literal_to_Literal :: Int -> Checker.Literal
literal_to_Literal x = if x > 0 then Checker.Pos x else Checker.Neg (-x)

nat_to_nat :: Int -> Int
nat_to_nat = n_to_nat . nat_to_N

--nat_to_Nat :: Int -> Checker.Nat
--nat_to_Nat 0 = Checker.O
--nat_to_Nat x | x > 0 = Checker.S (nat_to_Nat (x-1))
--             | otherwise = nat_to_Nat (-x)

n_to_nat :: Checker.N -> Int
n_to_nat Checker.N0 = 0
n_to_nat (Checker.Npos n) = n

nat_to_N :: Int -> Checker.N
nat_to_N 0 = Checker.N0
nat_to_N n = Checker.Npos n

--bool_to_Bool :: Bool -> Checker.Bool
--bool_to_Bool True = Checker.True
--bool_to_Bool False = Checker.False

test :: Checker.List Checker.Clause -> Checker.Oracle -> Checker.Answer
test cnf grup = Checker.refute cnf grup

main = do
  (cnfFile:grupFile:_) <- getArgs
  cnf <- read_cnf cnfFile
  grup <- read_grup grupFile
--  print cnf
--  print grup
  print (test cnf grup)
--  print (length grup)

read_cnf :: String -> IO (Checker.List Checker.Clause)
read_cnf name = do
  f <- openFile name ReadMode
  s <- hGetContents f
  let cnf = parse_cnf s
--  print (length (extract_clauses (lines s)))
--  print (Set.size cnf)
--  print (num_clauses s)
--  return (assert (Set.size cnf == num_clauses s) cnf)
  return cnf

parse_cnf :: String -> Checker.List Checker.Clause
--parse_cnf = Set.fromList . map sort . map drop_zero . (filter (not . null)) . (map (map (read :: String -> Int) . words)) . extract_clauses . lines
parse_cnf = list_to_List . map clause_to_Clause . map sort . map drop_zero . (filter (not . null)) . (map (map (read :: String -> Int) . words)) . extract_clauses . lines

extract_clauses :: [String] -> [String]
extract_clauses [] = []
extract_clauses (('c':line):lines) = extract_clauses lines
extract_clauses (('p':line):lines) = extract_clauses lines
extract_clauses (line:lines) = line:extract_clauses lines

drop_zero :: [Int] -> [Int]
drop_zero [0] = []
drop_zero (x:xs) = x:drop_zero xs

num_clauses :: String -> Int
num_clauses = read . (flip (!!) 3) . words . extract_problem . lines

extract_problem :: [String] -> String
extract_problem (('p':line):lines) = 'p':line
extract_problem (line:lines) = extract_problem lines

read_grup :: String -> IO Checker.Oracle
read_grup name = do
  f <- openFile name ReadMode
  s <- hGetContents f
  return (parse_grup s)

parse_grup :: String -> Checker.Oracle
parse_grup = list_to_lazy_List . map action_to_Action . map read . lines
--(map list_to_action) . (filter (not . null)) . (map (map (read :: String -> Int) . words)) . lines

--list_to_action :: [Int] -> Checker.Action
--list_to_action (0:xs) = Checker.D (drop_zero xs)
--list_to_action (id:xs) = if null zs then Checker.O id (sort ys) else Checker.R id (sort ys) (reverse zs) where (ys,zs) = split_zero xs

split_zero :: [Int] -> ([Int],[Int])
split_zero (0:xs) = ([],drop_zero xs)
split_zero (x:xs) = (x:ys,zs) where (ys,zs) = split_zero xs
