import cclib as cc
import sys
import time

cnf = sys.argv[1]

start_time = time.time()
cc.solve_verify_cube_and_conquer(cnf)
print("Runtime: %s s" % (time.time() - start_time))
