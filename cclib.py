#!/usr/bin/env python
from subprocess import call, Popen, check_output
from functools import partial
from time import strftime
import multiprocessing as mp
import validate as va
import os, sys, ConfigParser, hashlib, sqlite3, resource

#Dictionary configuration
dict_config = {}
Config = ConfigParser.ConfigParser()

#Redirect to DEVNULL
FNULL = open(os.devnull, 'w')

#Global variables
database = False
logfile = False
log = False

#Initialize from configuration
def init():
    if not os.path.isfile("config.ini"):
        exit("Error: Can't find 'config.ini' in current directory") 
    Config.read("config.ini")
    #get settings and paths from config
    for section in Config.sections():
        ConfigSectionMap(section)
    #set general global variables
    global validation
    validation = eval(dict_config.get("validation","False"))
    global overwrite
    overwrite = eval(dict_config.get("overwrite","False"))
    global database
    global logfile
    global log
    log = False
    if logfile:
        log = True
    if database:
        log = True
        database_init()

#Add paths and options from config.ini to dictionary
def ConfigSectionMap(section):
    options = Config.options(section)
    global database
    global logfile
    for opt in options:
        try:
            dict_config[opt] = Config.get(section,opt)
        except:
            exit("Error: In section '%s' option '%s' in 'config.ini'" % (section,opt))
        #check if value is a path
        if opt == "database":
            database = eval(dict_config.get("database","False"))
        if opt == "logfile":
            logfile = eval(dict_config.get("logfile","False"))
        if not section == "general" and not os.path.isfile(Config.get(section,opt)):
            #create logging file if path does not exists
            if section == "logging":
                if opt == "database_path" and database:
                    if not os.path.exists(os.path.dirname(Config.get(section,opt))):
                        os.makedirs(os.path.dirname(Config.get(section,opt)))
                    open(Config.get(section,opt),'w').close()
                    print "\nCreated database '%s'" % Config.get(section,opt)
                elif opt == "logfile_path" and logfile:
                    if not os.path.exists(os.path.dirname(Config.get(section,opt))):
                        os.makedirs(os.path.dirname(Config.get(section,opt)))
                    with open(Config.get(section,opt),'w') as f:
                        f.write("Date, Instance, Executable, Command, Runtime, Memory Peak, Result\n")
                        f.close()
                    print "\nCreated logfile '%s'" % Config.get(section,opt)
            #all others files has to exists
            else:
                exit("Error: Path '%s' in section '%s' in option '%s' does not exist. \nCheck 'config.ini'" % (Config.get(section,opt),section,opt))
        #do not overwrite logfile
        elif opt == "logfile_path" and logfile:
            i = 1
            log_name = Config.get(section,opt)
            while os.path.isfile("%s.%d" % (log_name,i)):
                i += 1
            log_name = "%s.%d" % (log_name,i)
            with open(log_name,'w') as f:
                f.write("Date, Instance, Executable, Command, Runtime, Memory Peak, Result\n")
                f.close()
            dict_config[opt] = log_name
            print "Created logfile '%s'" % log_name

#Database table
def database_init():
    db_dir = dict_config.get("db_path",create_errorhandler_2("db_path"))
    conn = sqlite3.connect(db_dir)
    print "\nOpened database %s successfully" %db_dir
    #create table 'CC' if it does not exists in database
    if not  conn.execute("""SELECT * FROM sqlite_master WHERE name ='CC' and type='table'""").fetchone():
        conn.execute('''CREATE TABLE CC
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    INSTANCE TEXT NOT NULL,
                    INSTANCE_HASH TEXT NOT NULL,
                    EXECUTABLE TEXT NOT NULL,
                    EXECUTABLE_HASH TEXT NOT NULL,
                    COMMAND TEXT NOT NULL,
                    RUNTIME REAL NOT NULL,
                    PEAK_MEMORY INTEGER NOT NULL,
                    RESULT_OK TEXT NOT NULL
                    );''')
        print "Created table CC"
    conn.commit()
    conn.close()

#Generate fresh file or overwrites file
def fresh_file(name):
    if not overwrite and os.path.isfile(name):
        i = 1
        while os.path.isfile("%s.%d" % (name,i)):
            i += 1
        name = "%s.%d" % (name,i)
    return name

def validation_fail_handler(file):
    #force python to exit
    exit("Error: Validation of '%s' failed. Python exited" % file)

#Partitioning a given cnf with march_cc
def partition(cnf,out_cubes=""):
    print "\nPartioning %s with march_cc:" % cnf
    if validation and not va.is_cnf(cnf):
        validation_fail_handler(cnf)
    if not out_cubes:
        out_cubes = cnf+".cubes"
    out_cubes = fresh_file(out_cubes)
    march_cc = dict_config.get("march_cc",create_errorhandler_2("march_cc"))
    command = "%s %s %s" %(march_cc,cnf,out_cubes)
    if log:
        exec_command = 'result = check_output("%s %s",shell=True)' % (command,"")#(command,"| grep '^c print learnt'")
        coms = []
        coms.append(exec_command)
        coms.append("if result.startswith('c print learnt clauses and cubes'):\n\tres = 'Successful'\nelse:\n\tres='Failed'")
        logging(cnf,march_cc,coms,command)
    else:
        call(command,shell=True)
    print "_____________________________________________________\n"
    return out_cubes

#Concatenate original cnf, clauses and cubes
def cat_icnf(cnf,cubes,out_icnf=""):
    if not out_icnf:
        out_icnf = cnf+".icnf"
    out_icnf = fresh_file(out_icnf)
    call("echo 'p inccnf' > "+out_icnf,shell=True)
    call("cat "+cnf+" | grep -v '^[cp]' >> "+out_icnf,shell=True)
    call("cat "+cubes+" >> "+out_icnf,shell=True)
    return out_icnf

#Get p-line from cnf
def get_p_cnf(cnf):
    with open(cnf,'r') as f:
        for line in f:
            if(line.startswith('p')):
                return line

#Increment p-line by number of clauses
def inc_p_cnf(p,k):
    p = p.split()
    p[3] = str(int(p[3])+k)
    return "p cnf "+p[2]+" "+p[3]

#Get amount of variables and clauses of cube file and return as p line
def get_p_cube(neg_cubes):
    with open(neg_cubes, 'r') as f:
        max_int = None
        x = -1
        for i, line in enumerate(f):
            num = line.split()
            x = i
            for n in num:
                if(max_int < abs(int(n))):
                    max_int = abs(int(n))
        return "p cnf "+str(max_int)+" "+str(x+1)

#Generate non incremental cubes 
def gen_cubes(cnf,cubes,out_cubes_name=""):
    if not out_cubes_name:
        out_cubes_name = cubes+".part"
    out_cubes_parts = []
    #temp variable
    out_remA_cubes = cubes+".remA_tmp"
    rem_a(cubes,out_remA_cubes)
    p = get_p_cnf(cnf)
    with open(out_remA_cubes,'r') as f:
        for i, line in enumerate(f):
            cubes_name = fresh_file(out_cubes_name+str(i))
            cubes_name = cubes_name+".cnf"
            out_cubes_parts.append(cubes_name)
            line = line.split()
            line[-1] = ""
            ip = inc_p_cnf(p,len(line)-1)
            line = " 0\n".join(line)
            call("echo "+ip+" > "+cubes_name, shell=True)
            call("cat "+cnf+" | grep -v '^[cp]' >> "+cubes_name, shell=True)
            file = open(cubes_name,'a')
            file.write(line)
            file.close()
    os.remove(out_remA_cubes)
    return out_cubes_parts

#Remove a's from the assumption lines in cubes file
def rem_a(cubes,out_remA_cubes):
    call("cut -d' ' -f2- "+cubes+" > "+out_remA_cubes,shell=True)

#Negation of a CNF in DIMACS format
def neg_cube(remA_cubes,out_neg_cubes):
    file_in = open(remA_cubes,"r")
    file_out = open(out_neg_cubes,"w")
    for line in file_in:
        for n in line.split():
            file_out.write(str(int(n)*-1)+" ")
        file_out.write("\n")
    file_in.close
    file_out.close

#Remove a's, negate a cube file and add p line
def gen_cube_cnf(cubes,out_cubes_cnf=""):
    if not out_cubes_cnf:
        out_cubes_cnf = cubes + ".cnf"
    out_cubes_cnf = fresh_file(out_cubes_cnf)
    out_remA_cubes = out_cubes_cnf+".remA_tmp"
    rem_a(cubes,out_remA_cubes)
    out_neg_cubes = out_cubes_cnf+".neg_tmp"
    neg_cube(out_remA_cubes,out_neg_cubes)
    call("echo "+get_p_cube(out_neg_cubes)+" > "+out_cubes_cnf, shell=True)
    call("cat "+out_neg_cubes+" >> "+out_cubes_cnf, shell=True)
    os.remove(out_remA_cubes)
    os.remove(out_neg_cubes)
    return out_cubes_cnf

#Errorhandler solver,trim,checker dispatch dictionary
def create_errorhandler_1(key_name,dict,dict_name):
    def errorhandler(arg1="",arg2="",arg3=""):
        print "KeyError: No such key '%s' in dictionary '%s'." %(key_name,dict_name)
        print "Choose one of them:"
        for key in dict.iterkeys():
            print key
        exit()
    return errorhandler

#Errorhandler for config dictionary
def create_errorhandler_2(key_name):
    def errorhandler():
        exit("KeyError: No such key '%s' in dictionary. Check config.ini" %name)
    return errorhandler

#Writing to database/logfile
def logging(ins,exe,coms,command):
    rusage_before = resource.getrusage(resource.RUSAGE_CHILDREN)
    result = None
    res = "Error"
    for c in coms:
        exec(c)
    rusage_after = resource.getrusage(resource.RUSAGE_CHILDREN)
    #cputime
    time = (rusage_after.ru_utime + rusage_after.ru_stime) \
            - (rusage_before.ru_utime + rusage_before.ru_stime)
    #memory usage
    peak_mem = rusage_after.ru_maxrss - rusage_before.ru_maxrss
    if database:
        ins_enc = hashlib.md5(ins.encode())
        ins_hash = ins_enc.hexdigest()
        exe_enc = hashlib.md5(exe.encode())
        exe_hash = exe_enc.hexdigest()
        db_dir = dict_config.get("db_path",create_errorhandler_2("db_path"))
        conn = sqlite3.connect(db_dir)
        conn.execute("INSERT INTO CC (INSTANCE,INSTANCE_HASH,EXECUTABLE,EXECUTABLE_HASH,COMMAND,RUNTIME,PEAK_MEMORY,RESULT_OK) \
                VALUES('"+ins+"','"+ins_hash+"','"+exe+"','"+exe_hash+"','"+command+"',"+str(time)+","+str(peak_mem)+",'"+res+"');")
        conn.commit()
        conn.close()
    if logfile:
        date = strftime("%Y-%m-%d %H:%M:%S")
        file_path = dict_config.get("logfile_path",create_errorhandler_2("logfile_path"))
        with open(file_path,'a') as logf:
            logf.write("{}, {}, {}, {}, {}, {}, {}\n".format(date,ins,exe,command,time,peak_mem,res))
            logf.close
    if res is 'Successful':
        return True
    elif res is 'Failed':
        return False
    else:
        exit("Error: In command %s" %exe)
        return False

#Solvers
def lingeling(cnf,out_drup):
   # print "\nSolving %s with lingeling:" % cnf
    if validation and not va.is_cnf(cnf):
        validation_fail_handler(cnf)
    lingeling = dict_config.get("lingeling",create_errorhandler_2("lingeling"))
    if log:
        command = lingeling+" "+cnf+" | tee >(grep -v '^[cos]' > "+out_drup+")"
        exec_command = 'result = check_output("%s %s",shell=True,executable="/bin/bash")' % (command,"| grep '^s'")
        coms = []
        coms.append(exec_command)
        coms.append("if result.startswith('s'):\n\tres = 'Successful'\nelse:\n\tres= 'Failed'")
        logging(cnf,lingeling,coms,command)
    else:
        call(lingeling+" "+cnf+" | grep -v '^[cos]' > "+out_drup,stdout=FNULL,shell=True)
   # print "_____________________________________________________\n"


def lingelingbbc(cnf,out_drup):
   # print "\nSolving %s with lingelingbbc:" % cnf
    if validation and not va.is_cnf(cnf):
        validation_fail_handler(cnf)
    lingeling = dict_config.get("lingelingbbc",create_errorhandler_2("lingelingbbc"))
    if log:
        command = lingeling+" "+cnf+" -t "+out_drup 
        exec_command = 'result = check_output("%s %s",shell=True)' % (command,"| grep '^s'")
        coms = []
        coms.append(exec_command)
        coms.append("if result.startswith('s'):\n\tres = 'Successful'\nelse:\n\tres= 'Failed'")
        logging(cnf,lingeling,coms,command)
    else:
        call(lingeling+" "+cnf+" -t "+out_drup,stdout=FNULL,shell=True)
   # print "_____________________________________________________\n"

def iglucose(icnf,out_drup):
    print "\nSolving %s with iGlucose:" % icnf
    if validation and not va.is_icnf(icnf):
        validation_fail_handler(icnf)
    iglucose = dict_config.get("iglucose",create_errorhandler_2("iglucose"))
    if log:
        command = iglucose+" "+icnf
        exec_command = 'call(["%s","%s","%s","%s","%s","%s"])' % (iglucose,"-incremental",icnf,"-certified","-certified-output="+out_drup,"-verb=0")
        coms = []
        coms.append(exec_command)
        #Set as Failed to avoid Error in logging
        coms.append('res = "Failed"')
        logging(icnf,iglucose,coms,command)
    else:
        call([iglucose,"-incremental",icnf,"-certified","-certified-output="+out_drup,"-verb=0"])
    print "______________________________________________________\n"


#Trimmers
def grit(cnf,drup,out_grit):
    #print "\nValidating clausal proofs of %s. Output GRIT trace:" % drup
    if validation and not va.is_drup(drup):
        validation_fail_handler(drup)
    grit = dict_config.get("grit",create_errorhandler_2("grit"))
    if log:
        command = grit+" "+cnf+" "+drup+" -r "+cnf+".graph_tmp"
        exec_command1 = 'result = check_output("%s %s %s",shell=True)' % ("ulimit -s unlimited;",command,"| grep '^s VERIFIED'")
        exec_command2 = 'call(["tac %s.graph_tmp > %s"],shell=True)' % (cnf,out_grit)
        exec_command3 = 'os.remove("%s.graph_tmp")' % cnf
        coms = []
        coms.append(exec_command1)
        coms.append(exec_command2)
        coms.append(exec_command3)
        coms.append("if result.startswith('s VERIFIED'):\n\tres = 'Successful'\nelse:\n\tres='Failed'")
        logging(cnf,grit,coms,command)
    else:
        call("ulimit -s unlimited; "+grit+" "+cnf+" "+drup+" -r "+cnf+".graph_tmp",stdout=FNULL,shell=True)
        call("tac "+cnf+".graph_tmp > "+out_grit,shell=True)
        os.remove(cnf+".graph_tmp")
   # print "_____________________________________________________\n"


def lrat(cnf,drup,out_lrat):
    #print "\nValidating clausal proofs of %s. Output LRAT trace:" % drup
    if validation and not va.is_drup(drup):
        validation_fail_handler(drup)
    lrat = dict_config.get("lrat",create_errorhandler_2("lrat"))
    if log:
        command = lrat+" "+cnf+" "+drup+" "+"-L "+out_lrat
        exec_command1 = 'result = check_output("%s %s %s", shell=True)' % ("ulimit -s unlimited;",command,"") #"| grep '^s VERIFIED'")
        exec_command2 = 'call(["sort","-n","%s","-o","%s"])' % (out_lrat,out_lrat)
        coms = []
        coms.append(exec_command1)
        coms.append(exec_command2)
        coms.append("if result.startswith('s VERIFIED'):\n\tres = 'Successful'\nelse:\n\tres='Failed'")
        logging(cnf,lrat,coms,command)
    else:
        call("ulimit -s unlimited; "+lrat+" "+cnf+" "+drup+" -L "+out_lrat,stdout=FNULL,shell=True)
        call(["sort","-n",out_lrat,"-o",out_lrat])
   # print "_____________________________________________________\n"



#Checkers
def coq_grit(cnf,grit):
    #print "\nVerify %s in GRIT format with coq-checker:" % grit
    if validation and not va.is_grit(grit):
        validation_fail_handler(grit)
    coq_grit = dict_config.get("coq-grit",create_errorhandler_2("coq-grit"))
    if log:
        command = coq_grit+" "+cnf+" "+grit
        exec_command = 'result = check_output("%s %s",shell=True)' % ("ulimit -s unlimited;",command)
        coms = []
        coms.append(exec_command)
        coms.append("if 'True' in result:\n\tres = 'Successful'\nelse:\n\tres='Failed'")
        return logging(cnf,coq_grit,coms,command)
    else:
        return check_output("ulimit -s unlimited; "+coq_grit+" "+cnf+" "+grit,shell=True)
   # print "_____________________________________________________\n"


def coq_lrat(cnf,lrat):
    #print "\nVerify %s in LRAT format with coq-checker:" % lrat
    if validation and not va.is_lrat(lrat):
        validation_fail_handler(lrat)
    coq_lrat = dict_config.get("coq-lrat",create_errorhandler_2("coq-lrat"))
    if log:
        command = coq_lrat+" "+cnf+" "+lrat
        exec_command = 'result = check_output("%s %s",shell=True)' % ("ulimit -s unlimited;",command)
        coms = []
        coms.append(exec_command)
        coms.append("if 'True' in result:\n\tres = 'Successful'\nelse:\n\tres='Failed'")
        return logging(cnf,coq_lrat,coms,command)
    else:
        print "Verify: "+cnf.rsplit('/', 1)[-1]
        return check_output("ulimit -s unlimited; "+coq_lrat+" "+cnf+" "+lrat,shell=True)
   # print "_____________________________________________________\n"


#Dictionary of solvers
dispatch_solver = {
    'lingeling':lingeling,
    'lingelingbbc':lingelingbbc,
    'iglucose':iglucose
}

#Dictionary trimmers
dispatch_trim = {
    'grit':grit,
    'lrat':lrat
}

#Dictionary of checkers
dispatch_checkers = {
    'coq-grit':coq_grit,
    'coq-lrat':coq_lrat
}

#Call solver
def solve_single_file(file,solver='lingeling',out_drup=""):
    if not out_drup:
        out_drup = file+".drup"
    out_drup = fresh_file(out_drup)
    dispatch_solver.get(solver,create_errorhandler_1(solver,dispatch_solver,"dispatch_solver"))(file,out_drup)
    return out_drup

#Call trimmer
def trim(cnf,drup,format="lrat",out_file=""):
    if not out_file:
        out_file = cnf+"."+format
    out_file = fresh_file(out_file)
    dispatch_trim.get(format,create_errorhandler_1(format,dispatch_trim,"dispatch_trim"))(cnf,drup,out_file)
    return out_file

#Call checker
def check(cnf,format,checker="coq-grit"):
    return dispatch_checkers.get(checker,create_errorhandler_1(checker,dispatch_checkers,"dispatch_checkers"))(cnf,format)

#Multiprocessing
def scatter(f,t):
    return f(*t)

def parallelize(f,args,processes=mp.cpu_count()):
    pool = mp.Pool(processes)
    print "Solving cubes in parallel:"
    try:
        result = pool.map(partial(scatter,f),args)
    except OSError as e:
        print dir(e)
        print e.filename
        raise e
    pool.close()
    pool.join()
    print "\n_____________________________________________________\n"
    return result
  
def solve_verify(cnf,solver="lingelingbbc",format="lrat",checker="coq-lrat"):
    drup = solve_single_file(cnf,solver)
    trace = trim(cnf,drup,format)
    return check(cnf,trace,checker)

#higher-level functions (without init() & partition())
def solve_verify_parallelized(cnf,cubes,solver="lingelingbbc",format="lrat",checker="coq-lrat",processes=mp.cpu_count()):
    parts = gen_cubes(cnf,cubes)
    args = zip(parts,[solver]*len(parts),[format]*len(parts),[checker]*len(parts))
    result = parallelize(solve_verify,args,processes)
    return all(result)

def solve_verify_incremental(cnf,cubes,solver="iglucose",format="lrat",checker="coq-lrat"):
    icnf = cat_icnf(cnf,cubes)
    drup = solve_single_file(icnf,solver)
    trace = trim(cnf,drup,format)
    return check(cnf,trace,checker)

def solve_verify_cube(cubes,solver="lingelingbbc",format="lrat",checker="coq-lrat"):
    cubes_cnf = gen_cube_cnf(cubes)
    drup = solve_single_file(cubes_cnf,solver)
    trace = trim(cubes_cnf,drup,format)
    return check(cubes_cnf,trace,checker)

#prints solved and verfied status and returns it
def print_cube_and_conquer(cubes,sum_cube):
    if cubes and sum_cube:
        print "\nSolved and verified!"
        return True
    elif not (cubes or sum_cube):
        print "\nSolving and verifying the summarized cube and partitioned cubes failed!"
        return False
    elif cubes and not sum_cube:
        print "\nSolving and verifying the partitioned cubes succeded!"
        print "\nSolving and verifying the summarized cube failed!"
        return False
    elif not cubes and sum_cube:
        print "\nSolving and verifying the summarized cube succeded!"
        print "\nSolving and verifying the partitioned cubes failed!"
    return False

#main function to call
def solve_verify_cube_and_conquer(cnf,solver="lingelingbbc",format="lrat",checker="coq-lrat",processes=mp.cpu_count()):
    init()
    cubes = partition(cnf)
    sum_cube = solve_verify_cube(cubes,solver="lingelingbbc",format=format,checker=checker)
    #parallelized
    if solver in ["lingeling","lingelingbbc"]:
        result = solve_verify_parallelized(cnf,cubes,solver=solver,format=format,checker=checker,processes=processes)
        return print_cube_and_conquer(result,sum_cube)
    #incremental
    elif solver == "iglucose":
        result = solve_verify_incremental(cnf,cubes,solver=solver,format=format,checker=checker)
        return print_cube_and_conquer(result,sum_cube)
    #solver not found
    else:
        print "KeyError: The selected solver '%s' is not supported.\nChoose one of them:" % solver
        for key in dispatch_solver.iterkeys():
            print key
        exit()
