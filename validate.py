#!/usr/bin/env python
import os


#is_digit for negative numbers too.
def is_digit(n):
    try:
        int(n)
        return True
    except ValueError:
        return False

def is_cnf(cnf):
    if os.path.exists(cnf) and cnf.lower().endswith('cnf'):
        end = False
        highest_var = 0
        n_clauses = 0
        p = []
        line_number = 1
        with open(cnf,'r') as f:
            try:    
                line = iter(f)
                #Comment section:
                current = next(line)
                while current.isspace():
                    current = next(line)
                    line_number += 1
                while current.startswith('c'):
                    #ignore comments
                    current = next(line)
                    line_number += 1            
                #p line section:
                if not current.startswith('p'):
                    print "Line %s does not start with 'p'" % line_number
                    return False
                p = current.split()
                if not (len(p) == 4 and p[1] == "cnf" and p[2].isdigit() and p[3].isdigit()):
                    print "Line %s is not correct" % line_number
                    return False
                current = next(line)
                line_number += 1
                #Clause section
                while current[0].isdigit() or current[0] == "-":
                    num = current.split()
                    for n in num:
                        if not is_digit(n):
                            print "Line %s: '%s' is not an integer" % (line_number,n)
                            return False
                        temp = abs(int(n))
                        if temp > highest_var:
                            highest_var = temp
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False 
                    n_clauses += 1
                    current = next(line)
                    line_number += 1
                #Empty lines
                while current.isspace():
                    current = next(line)
                    line_number += 1
            except StopIteration:
                end = True
        if not end:
            print "Line %s is not correct" % line_number
            return False
        elif not highest_var == int(p[2]):
            print "Number of variables is not correct"
            return False
        elif not n_clauses == int(p[3]):
            print "Number of clauses is not correct"
            return False
        else:
            print "Syntax is correct"
            return True
    else:
        print "File path '%s' does not exist" % cnf
        return False

def is_icnf(icnf):
    if os.path.exists(icnf) and icnf.lower().endswith(".icnf"):
        end = False
        line_number = 1
        with open(icnf,'r') as f:
            try:
                line = iter(f)
                current = next(line)
                while current.isspace():
                    current = next(line)
                    line_number += 1
                #Comment section:
                while current.startswith('c'):
                    #ignore comments
                    current = next(line)
                    line_number += 1            
                #p line section:
                if not current.startswith('p'):
                    print "Line %s does not start with 'p'" % line_number
                    return False                
                p = current.split()
                if not (len(p) == 2 and p[1] == "inccnf"):
                    print "Line %s is not correct" % line_number
                    return False
                current = next(line)
                line_number += 1
                #Clause section:
                while current[0].isdigit() or current.startswith('a') or current.startswith('-'):
                    num = current.split()
                    if(num[0] == 'a'):
                        for i in xrange(1,len(num)):
                            if not is_digit(num[i]):
                                print "Line %s: '%s' is not an integer" % (line_number,num[i])
                                return False        
                    else:
                        for n in num:
                            if not is_digit(n):
                                print "Line %s: '%s' is not an integer" % (line_number,n)
                                return False
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False
                    current = next(line)
                    line_number += 1
                    while current.isspace():
                        current = next(line)
                    line_number += 1
            except StopIteration:
                end = True
            if not end:
                print "Line %s is not correct" % line_number
                return False
            else:
                print "Syntax is correct"
                return True     
    else:
        print "File path '%s' does not exist" % icnf
        return False
    
def is_cubes(cubes):
    if os.path.exists(cubes):
        with open(cubes,'r') as f:
            line_number = 1
            for line in f:
                if not line.isspace():
                    num = line.split()
                    if not line.startswith('a'):
                        print "Line %s does not start with an 'a'" % line_number
                        return False
                    for n in num[1:len(num)]:
                        if not is_digit(n):
                            print "Line %s: '%s' is not an integer" % (line_number,n)
                            return False
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False
                line_number += 1
        print "Syntax is correct"       
        return True
    else:
        print "File path '%s' does not exist" % cubes
        return False

def is_drup(drup):
    if os.path.exists(drup):
        with open(drup,'r') as f:
            line_number = 1
            for line in f:
                if not line.isspace():
                    num = line.split()
                    if num[0] == 'd':
                        num = num[1:]
                    for n in num:
                        if not is_digit(n):
                            print "Line %s: '%s' is not an integer" % (line_number,n)
                            return False
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False
                line_number += 1
        print "Syntax is correct"
        return True     
    else:
        print "File path '%s' does not exist" % drup
        return False

def is_grit(grit):
    if os.path.exists(grit):
        with open(grit,'r') as f:
            line_number = 1
            for line in f:
                if not line.isspace():
                    num = line.split()
                    for n in num:
                        if not is_digit(n):
                            print "Line %s: '%s' is not an integer" % (line_number,n)
                            return False
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False
                line_number += 1
        print "Syntax is correct"
        return True
    else:
        print "File path '%s' does not exist" % grit
        return False

def is_lrat(lrat):
    if os.path.exists(lrat):
        with open(lrat,'r') as f:
            line_number = 1
            inc_line_number = 0
            for line in f:
                if not line.isspace():
                    num = line.split()
                    if is_digit(num[0]) and int(num[0]) >= inc_line_number:
                        inc_line_number = int(num[0])
                    else:
                        print "Line %s: The first element is not integer or not in increasing order" % line_number
                        return False
                    if is_digit(num[1]) or num[1] == 'd':
                        for n in xrange(2,len(num)):
                            if not is_digit(num[n]):
                                print "Line %s: Not only integers expect 'd'" % line_number
                                return False
                    else:
                        print "Line %s: The second element is wrong" % line_number
                        return False                        
                    if not num[-1] == '0':
                        print "Line %s: Missing a '0' at the end of line" % line_number
                        return False
                line_number += 1
        print "Syntax is correct"
        return True 
    else:
        print "File path '%s' does not exist" % lrat
        return False
