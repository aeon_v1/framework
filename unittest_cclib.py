import unittest
import cclib as cc
import sys, os

cnf = sys.argv[1]


FNULL = open('/dev/null','w')
old = os.dup(sys.stdout.fileno())
os.dup2(FNULL.fileno(),1)

class Test_cclib(unittest.TestCase):
    
    def test_cnf(self):
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf))
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer("no_path") 

    def test_solver(self):
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,solver="lingeling"))
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,solver="lingelingbbc"))
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,solver="iglucose"))
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer(cnf,solver="no_solver")

    def test_format_checker(self):
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,format="grit",checker="coq-grit"))
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,format="lrat",checker="coq-lrat"))
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer(cnf,format="no_format")
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer(cnf,checker="no_checker")

    def test_format_checker_not_suitable(self):
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer(cnf,format="grit",checker="coq-lrat")
        with self.assertRaises(SystemExit):
            cc.solve_verify_cube_and_conquer(cnf,format="lrat",checker="coq-grit")

    def test_processes(self):
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,processes=8))
        self.assertTrue(cc.solve_verify_cube_and_conquer(cnf,processes=32))
        with self.assertRaises(ValueError):
            cc.solve_verify_cube_and_conquer(cnf,processes=0)
        with self.assertRaises(ValueError):
            cc.solve_verify_cube_and_conquer(cnf,processes=-1)

suite = unittest.TestLoader().loadTestsFromTestCase(Test_cclib)
unittest.TextTestRunner(verbosity=2).run(suite)
